// -------------------------------------------------------------
// Blur horizontally
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

#define LOOP_UNROLL

// -------------------------------------------------------------
// uniforms
// -------------------------------------------------------------

uniform vec2 viewSize;
uniform float bloomRadius;

// -------------------------------------------------------------
// samplers
// -------------------------------------------------------------

uniform sampler2D frameBloomImageSampler;

// -------------------------------------------------------------
// varying
// -------------------------------------------------------------

varying vec2 texCoord;

// -------------------------------------------------------------
// fragment shader
// -------------------------------------------------------------

void main(void)
{
         
   // final colour will be stored here
   vec4 colour = vec4( 0.0, 0.0, 0.0, 0.0 );
   vec2 scale = bloomRadius / viewSize;

#ifndef LOOP_UNROLL

   // define pixel offsets (filter size 13)
   vec2 PixelOffsets [13];
        PixelOffsets[0] = vec2( -6, 0 );
        PixelOffsets[1] = vec2( -5, 0 );
        PixelOffsets[2] = vec2( -4, 0 );
        PixelOffsets[3] = vec2( -3, 0 );
        PixelOffsets[4] = vec2( -2, 0 );
        PixelOffsets[5] = vec2( -1, 0 );
        PixelOffsets[6] = vec2(  0, 0 );
        PixelOffsets[7] = vec2(  1, 0 );
        PixelOffsets[8] = vec2(  2, 0 );
        PixelOffsets[9] = vec2(  3, 0);
        PixelOffsets[10] = vec2(  4, 0 );
        PixelOffsets[11] = vec2(  5, 0 );
        PixelOffsets[12] = vec2(  6, 0 );

   // define blur weights (filter size 13)
   float BlurWeights [13];
         BlurWeights[0] = 0.002216;
         BlurWeights[1] = 0.008764;
         BlurWeights[2] = 0.026995;
         BlurWeights[3] = 0.064759;
         BlurWeights[4] = 0.120985;
         BlurWeights[5] = 0.176033;
         BlurWeights[6] = 0.200496;
         BlurWeights[7] = 0.176033;
         BlurWeights[8] = 0.120985;
         BlurWeights[9] = 0.064759;
         BlurWeights[10] = 0.026995;
         BlurWeights[11] = 0.008764;
         BlurWeights[12] = 0.002216;
   
   for (int i = 0; i != 13; i++)
   {
      colour += texture2D( frameBloomImageSampler, texCoord + PixelOffsets[i].xy*scale ) * BlurWeights[i];
   }
   
#else
   // Blur horizontally
   // hard coded the numbers due to perfomance issues
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, -6.0 ) * scale ) * 0.002216;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, -5.0 ) * scale ) * 0.008764;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, -4.0 ) * scale ) * 0.026995;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, -3.0 ) * scale ) * 0.064759;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, -2.0 ) * scale ) * 0.120985;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, -1.0 ) * scale ) * 0.176033;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, 0.0 ) * scale ) * 0.200496;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, 1.0 ) * scale ) * 0.176033;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, 2.0 ) * scale ) * 0.120985;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, 3.0 ) * scale ) * 0.064759;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, 4.0 ) * scale ) * 0.026995;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, 5.0 ) * scale ) * 0.008764;
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, 6.0 ) * scale ) * 0.002216;
   
#endif

   gl_FragColor = colour;
}