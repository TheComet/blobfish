// -------------------------------------------------------------
// Down Sampler
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// ------------------------------------------------------------------
// Uniforms
// ------------------------------------------------------------------

uniform float reduceRatio;
uniform vec2 viewSize;

// ------------------------------------------------------------------
// Samplers
// ------------------------------------------------------------------

uniform sampler2D frameBloomImageSampler;

// ------------------------------------------------------------------
// Varying
// ------------------------------------------------------------------

varying vec2 texCoord;

// ------------------------------------------------------------------
// Fragment Shader
// ------------------------------------------------------------------

void main(void)
{

   //box filter, declare in pixel offsets convert to texel offsets in fragment shader
   /* old code -------------------------------------------
   vec2 DownFilterSamples[9];
        DownFilterSamples[0] = vec2( -1,  -1 );
        DownFilterSamples[1] = vec2( -1,  0  );
        DownFilterSamples[2] = vec2( -1,  1  );
        DownFilterSamples[3] = vec2( 0,   1  );
        DownFilterSamples[4] = vec2( 1,   1  );
        DownFilterSamples[5] = vec2( 1,   0  );
        DownFilterSamples[6] = vec2( 1,   -1 );
        DownFilterSamples[7] = vec2( 0,   -1 );
        DownFilterSamples[8] = vec2( 0,   0  );
         ------------------------------------------------*/

   // final colour will be stored here
   vec4 colour = vec4( 0.0, 0.0, 0.0, 0.0 );
   vec2 scale = reduceRatio/viewSize;
   
   // downsample to smaller image
   // hard coded the numbers due to performance issues
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( -1.0, -1.0 )*scale );
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( -1.0, 0.0 )*scale );
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( -1.0, 1.0 )*scale );
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, 1.0 )*scale );
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 1.0, 1.0 )*scale );
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 1.0, 0.0 )*scale );
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 1.0, -1.0 )*scale );
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, -1.0 )*scale );
   colour += texture2D( frameBloomImageSampler, texCoord + vec2( 0.0, 0.0 )*scale );
   
   /* old code -------------------------------------------
   for (int i = 0; i != 9; i++)
   {
      colour += texture2D( frameBloomImageSampler, texCoord + DownFilterSamples[i].xy*scale );
   } -----------------------------------------------------*/
   colour /= 9.0;

   gl_FragColor = colour;
}