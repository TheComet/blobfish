// -------------------------------------------------------------
// Final pass - combines rendered bloom image with scene
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// -------------------------------------------------------------
// uniforms
// -------------------------------------------------------------

uniform float bloomPower;
uniform float bloomScale;

// -------------------------------------------------------------
// Samplers
// -------------------------------------------------------------

uniform sampler2D frameImageSampler;
uniform sampler2D frameBloomImageProcessedSampler;

// -------------------------------------------------------------
// Varying
// -------------------------------------------------------------

varying vec2 texCoord;

// -------------------------------------------------------------
// Fragmet Shader
// -------------------------------------------------------------

void main(void)
{

   // get both images
   vec3 colour = texture2D( frameImageSampler, texCoord ).rgb;
   vec3 bloomScene = texture2D( frameBloomImageProcessedSampler, texCoord ).rgb;
   
   vec3 colour2 = colour;
   
   float foo = bloomScale;
   
   // combine
   colour += pow( bloomScene, vec3( bloomPower, bloomPower, bloomPower ) ) * bloomScale;
   //colour += pow( bloomScene^bloomPower;
   //colour += bloomScene*bloomScale;
   
   //colour = bloomScene;
   //colour = colour2+bloomScene*bloomScale;
   
   //colour = colour2;
   
   gl_FragColor = vec4( colour, 1.0 );
}