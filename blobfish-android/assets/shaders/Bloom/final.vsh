// -------------------------------------------------------------
// Final pass - combines rendered bloom image with scene
// by Alex Murray
// -------------------------------------------------------------

// -------------------------------------------------------------
// Attributes
// -------------------------------------------------------------

attribute vec4 a_position;

// -------------------------------------------------------------
// Varying
// -------------------------------------------------------------

varying vec2 texCoord;

// -------------------------------------------------------------
// Vertex Shader
// -------------------------------------------------------------

void main(void)
{

   // snap vertices to edge of screen
   vec2 P = sign( a_position.xy );
   
   // snap UV coordinates to edge of screen
   texCoord = P * 0.5 + 0.5;
   
   // align object to screen
   gl_Position = vec4( P, 0.0, 1.0 );
   
}