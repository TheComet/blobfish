// -------------------------------------------------------------
// Wall Shader - Normal, specular and emissive lighting
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// -------------------------------------------------------------
// Samplers
// -------------------------------------------------------------

uniform sampler2D textureSampler;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// emissive properties
uniform float emissivePower;

// -------------------------------------------------------------
// Varying
// -------------------------------------------------------------

varying vec2 texCoord;

// -------------------------------------------------------------
// Fragment Shader
// -------------------------------------------------------------

void main(void)
{

   // compute final colour
   vec3 colour = texture2D( textureSampler, texCoord ).rgb;
   colour *= emissivePower;

   gl_FragColor = vec4( colour, 1.0 );
}