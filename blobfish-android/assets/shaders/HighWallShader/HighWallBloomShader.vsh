// -------------------------------------------------------------
// Wall Shader - Normal, specular and emissive lighting
// by Alex Murray
// -------------------------------------------------------------

// -------------------------------------------------------------
// matrices
// -------------------------------------------------------------

uniform mat4 matViewProjection;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// emissive properties
uniform float emissivePower;

// to pass in the UV coordinates of each texture
uniform vec2 tr200;
uniform vec2 tr211;

// -------------------------------------------------------------
// attributes
// -------------------------------------------------------------

attribute vec4 a_position;
attribute vec2 a_texCoord0;

// -------------------------------------------------------------
// varyings
// -------------------------------------------------------------

varying vec2 texCoord;

// -------------------------------------------------------------
// Vertex Shader
// -------------------------------------------------------------

void main(void)
{

   // computer UV coordinates of each texture region
   texCoord = a_texCoord0 * ( tr211 - tr200 ) + tr200;
   
   // output vertex position
   gl_Position = matViewProjection * a_position;
}
