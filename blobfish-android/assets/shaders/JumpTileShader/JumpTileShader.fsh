// -------------------------------------------------------------
// Tile Shader - Normal, specular and emissive lighting
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// -------------------------------------------------------------
// Samplers
// -------------------------------------------------------------

uniform sampler2D textureSampler;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// to pass in the UV coordinates of each texture
uniform vec2 tr000;
uniform vec2 tr011;

// -------------------------------------------------------------
// Varying
// -------------------------------------------------------------

varying vec2 texCoord0;

// -------------------------------------------------------------
// Fragment Shader
// -------------------------------------------------------------

void main(void)
{
   // get diffuse colour
   vec3 colour = texture2D( textureSampler, texCoord0 ).rgb * 2.0 - 1.0;

   // output colour
   gl_FragColor = vec4((colour * 3.0), 1.0);
}