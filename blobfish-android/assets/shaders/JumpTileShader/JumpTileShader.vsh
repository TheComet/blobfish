// -------------------------------------------------------------
// Tile Shader - Normal, specular and emissive lighting
// by Alex Murray
// -------------------------------------------------------------

// -------------------------------------------------------------
// matrices
// -------------------------------------------------------------

uniform mat4 matViewProjection;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// to pass in the UV coordinates of each texture
uniform vec2 tr000;
uniform vec2 tr011;

// -------------------------------------------------------------
// attributes
// -------------------------------------------------------------

attribute vec4 a_position;
attribute vec2 a_texCoord0;

// -------------------------------------------------------------
// varyings
// -------------------------------------------------------------

varying vec2 texCoord0;

// -------------------------------------------------------------
// Vertex Shader
// -------------------------------------------------------------

void main(void)
{

   // computer UV coordinates of each texture region
   texCoord0 = a_texCoord0 * ( tr011 - tr000 ) + tr000;

   // Output vertex position
   gl_Position = matViewProjection * a_position;
}
