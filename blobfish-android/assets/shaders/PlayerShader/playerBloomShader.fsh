// -------------------------------------------------------------
// UFO BLoom Shader - Renders the UFO only with emissive
//                    for later processing in bloom shader
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// -------------------------------------------------------------
// Samplers
// -------------------------------------------------------------

uniform sampler2D textureSampler;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// emissive properties
uniform vec3 shipColour;
uniform float emissivePower;

// -------------------------------------------------------------
// Varying
// -------------------------------------------------------------

varying vec2 texCoord;

// -------------------------------------------------------------
// Fragment Shader
// -------------------------------------------------------------

void main(void)
{

   // get emissive from emissive map
   float emissive = texture2D( textureSampler, texCoord ).a;
   emissive *= emissivePower;

   // compute final colour
   vec3 colour = emissive*shipColour*emissivePower;

   gl_FragColor = vec4( colour, 1.0 );
}