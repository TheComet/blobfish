// -------------------------------------------------------------
// Tile Bloom Shader - Renders the tile only with emissive
// by Alex Murray
// -------------------------------------------------------------

// -------------------------------------------------------------
// matrices
// -------------------------------------------------------------

uniform mat4 matViewProjection;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// emissive properties
uniform vec3 shipColour;
uniform float emissivePower;

// to pass in the UV coordinates of the texture
uniform vec2 tr200;
uniform vec2 tr211;

// Y angle of UFO
uniform float angleY;

// scale of UFO
uniform vec3 scale;

// -------------------------------------------------------------
// attributes
// -------------------------------------------------------------

attribute vec4 a_position;
attribute vec2 a_texCoord0;

// -------------------------------------------------------------
// varyings
// -------------------------------------------------------------

varying vec2 texCoord;

// -------------------------------------------------------------
// Vertex Shader
// -------------------------------------------------------------

void main(void)
{

   // set up rotation matrix
   float sinY = sin(angleY);
   float cosY = cos(angleY);
   mat4 matRotationY = mat4(
      cosY*scale.x,  0.0,           -sinY*scale.x, 0.0,
      0.0,           scale.y,       0.0,           0.0,
      sinY*scale.z,  0.0,           cosY*scale.z,  0.0,
      0.0,           0.0,           0.0,           1.0
   );
   
   // rotate player
   vec4 newPosition = matRotationY * a_position;
   
   // Texture coordinates for fragment shader
   texCoord = a_texCoord0 * ( tr211 - tr200 ) + tr200;
   
   // output vertex positions
   gl_Position = matViewProjection * newPosition;
}
