// -------------------------------------------------------------
// UFO Shader - Uses diffuse, emissive, and specular lighting
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// -------------------------------------------------------------
// samplers
// -------------------------------------------------------------

uniform sampler2D textureSampler;

// -------------------------------------------------------------
// uniforms
// -------------------------------------------------------------

// camera position
uniform vec4 cameraPosition;

// source of the light
uniform vec3 lightSource;

// ambinet colour
uniform vec3 ambientColour;

// specular colour
uniform vec3 specularColour;

// specular intensity
uniform float specularIntensity;

// shininess of specular lighting
uniform float shininess;

// colour of the ship
uniform vec3 shipColour;

// -------------------------------------------------------------
// varying
// -------------------------------------------------------------

varying vec2 texCoord_diffuse;
varying vec2 texCoord_normal;
varying vec3 tsPosition;
varying vec3 tsCameraPosition;
varying vec3 tsLightSource;

// -------------------------------------------------------------
// vertex shader
// -------------------------------------------------------------

void main(void)
{

   // sample diffuse texture
   vec4 colour = texture2D( textureSampler, texCoord_diffuse );

   // get normal from normal map
   vec4 normalSample = texture2D( textureSampler, texCoord_normal );
   vec3 normal = normalSample.xyz * 2.0 - 1.0;
   
   // get colour changing regions, which are defined by the alpha channel of the normal map
   float shipColourWeight = normalSample.w;
   
   // normalize the other tangent space vectors
   vec3 viewVector = normalize( tsCameraPosition - tsPosition );
   vec3 lightVector = normalize( tsLightSource - tsPosition );
   
   // calculate lighting values
   float nxDir = max( 0.0, dot( normal, lightVector ) );
   vec3 ambient = ambientColour * colour.xyz;
   
   // calculate specular colour
   float specularPower = 0.0;
   if( nxDir != 0.0 )
   {
      vec3 halfVector = normalize( lightVector + viewVector );
      float nxHalf = max( 0.0, dot( normal, halfVector ) );
      specularPower = pow( nxHalf, shininess );
   }
   vec3 specular = specularColour * specularPower * specularIntensity;
   
   // add ship colour
   colour *= (1.0 - shipColourWeight );
   colour += vec4( shipColour * shipColourWeight, 0.0 );
   
   // compute final colour
   colour = vec4( ambient, 1.0 ) + (nxDir * colour * 0.5) + vec4(specular*0.5, 1.0);

   gl_FragColor = colour;
}