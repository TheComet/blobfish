// -------------------------------------------------------------
// UFO Shader - Uses diffuse, emissive, and specular lighting
//              alpha channel of normal map dictates which
//              regions change colour
// by Alex Murray
// -------------------------------------------------------------

// -------------------------------------------------------------
// matrices
// -------------------------------------------------------------

uniform mat4 matViewProjection;

// -------------------------------------------------------------
// uniforms
// -------------------------------------------------------------

// camera position
uniform vec4 cameraPosition;

// source of the light
uniform vec3 lightSource;

// Y angle of UFO
uniform float angleY;

// scale of UFO
uniform vec3 scale;

// to pass in texture coordinates
// tr0 -> diffuse, tr1 -> normal, tr2 -> emissive
uniform vec2 tr000;
uniform vec2 tr011;
uniform vec2 tr100;
uniform vec2 tr111;
uniform vec2 tr200;
uniform vec2 tr211;

// -------------------------------------------------------------
// attributes
// -------------------------------------------------------------

attribute vec4 a_position;
attribute vec3 a_normal;
attribute vec2 a_texCoord0;

// -------------------------------------------------------------
// varying
// -------------------------------------------------------------

varying vec2 texCoord_diffuse;
varying vec2 texCoord_normal;
varying vec3 tsPosition;
varying vec3 tsCameraPosition;
varying vec3 tsLightSource;

// -------------------------------------------------------------
// vertex shader
// -------------------------------------------------------------

void main(void)
{

   // set up rotation matrix
   float sinY = sin(angleY);
   float cosY = cos(angleY);
   mat4 matRotationY = mat4(
      cosY*scale.x,  0.0,           -sinY*scale.x, 0.0,
      0.0,           scale.y,       0.0,           0.0,
      sinY*scale.z,  0.0,           cosY*scale.z,  0.0,
      0.0,           0.0,           0.0,           1.0
   );

   // rotate player
   vec4 newPosition = matRotationY * a_position;
   
   // rotate normal vector
   vec4 newNormal = normalize( matRotationY * vec4( a_normal, 1.0 ) );

   // computer UV coordinates of each texture region
   texCoord_diffuse = a_texCoord0 * ( tr011 - tr000 ) + tr000;
   texCoord_normal = a_texCoord0 * (tr111 - tr100 ) + tr100;
   
   // This is a dirty way of calculating tangent and binormal
   // It assumes the object normal never faces towards 1,0.01,1, which should be pretty damn rare
   // If this is ever the case, the face in question will not calculate its lighting correctly
   vec3 tangent = normalize( cross( vec3( 1.0, 0.01, 1.0 ), newNormal.xyz ) );
   vec3 binormal = normalize( cross( tangent, newNormal.xyz ) );
   mat3 matTBN = mat3( tangent, binormal, newNormal.xyz );
   
   // transform lighting information into tangent space
   tsPosition = vec3( newPosition ) * matTBN;
   tsCameraPosition = vec3( cameraPosition ) * matTBN;
   tsLightSource = lightSource * matTBN;
   
   // output vertex position
   gl_Position = matViewProjection * newPosition;
}