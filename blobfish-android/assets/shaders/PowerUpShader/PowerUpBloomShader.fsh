// -------------------------------------------------------------
// Power Up Shader
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// -------------------------------------------------------------
// samplers
// -------------------------------------------------------------

uniform sampler2D textureSampler;

// -------------------------------------------------------------
// uniforms
// -------------------------------------------------------------

// -------------------------------------------------------------
// varying
// -------------------------------------------------------------

varying vec2 texCoord;

// -------------------------------------------------------------
// vertex shader
// -------------------------------------------------------------

void main(void)
{

   // get diffuse colour
   vec4 colour = texture2D( textureSampler, texCoord );

   // output colour
   gl_FragColor = colour;
}