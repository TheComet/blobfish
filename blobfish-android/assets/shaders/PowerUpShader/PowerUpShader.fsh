// -------------------------------------------------------------
// Power Up Shader
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// -------------------------------------------------------------
// samplers
// -------------------------------------------------------------

uniform sampler2D textureSampler;

// -------------------------------------------------------------
// uniforms
// -------------------------------------------------------------

// -------------------------------------------------------------
// varying
// -------------------------------------------------------------

varying vec2 texCoord;

// -------------------------------------------------------------
// vertex shader
// -------------------------------------------------------------
 
void main(void)
{

   // get diffuse colour
   vec3 colour = texture2D( textureSampler, texCoord ).rgb * 2.0 - 1.0;

   // output colour
   gl_FragColor = vec4((colour * 3.0), 1.0);
}