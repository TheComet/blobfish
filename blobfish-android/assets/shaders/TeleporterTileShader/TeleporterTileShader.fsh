// -------------------------------------------------------------
// Tile Shader - Normal, specular and emissive lighting
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// -------------------------------------------------------------
// Samplers
// -------------------------------------------------------------

uniform sampler2D textureSampler;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// to pass in the UV coordinates of each texture
uniform vec2 tr000;
uniform vec2 tr011;
uniform vec2 tr100;
uniform vec2 tr111;
uniform vec2 tr200;
uniform vec2 tr211;
uniform vec2 tr300;
uniform vec2 tr311;

// -------------------------------------------------------------
// Varying
// -------------------------------------------------------------

varying vec2 texCoord_metal;
varying vec2 texCoord_force;
varying vec2 texCoord_ring;
varying vec2 texCoord_center;

// -------------------------------------------------------------
// Fragment Shader
// -------------------------------------------------------------

void main(void)
{
   // get normal from normal map
   vec3 metal = texture2D( textureSampler, texCoord_metal ).rgb * 2.0 - 1.0;
   vec3 force = texture2D( textureSampler, texCoord_force ).rgb * 2.0 - 1.0;
   vec3 ring = texture2D( textureSampler, texCoord_ring ).rgb * 2.0 - 1.0;
   vec3 center = texture2D( textureSampler, texCoord_center ).rgb * 2.0 - 1.0;

   // compute final colour
   //vec3 colour = vec3((metal * 0.25) + (force * 0.25) + (ring * 0.25) + (center * 0.25));
   vec3 colour = force;
   
   gl_FragColor = vec4( colour, 0.5 );
}