// -------------------------------------------------------------
// Tile Shader - Normal, specular and emissive lighting
// by Alex Murray
// -------------------------------------------------------------

// -------------------------------------------------------------
// matrices
// -------------------------------------------------------------

uniform mat4 matViewProjection;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// to pass in the UV coordinates of each texture
uniform vec2 tr000;
uniform vec2 tr011;
uniform vec2 tr100;
uniform vec2 tr111;
uniform vec2 tr200;
uniform vec2 tr211;
uniform vec2 tr300;
uniform vec2 tr311;

// -------------------------------------------------------------
// attributes
// -------------------------------------------------------------

attribute vec4 a_position;
attribute vec3 a_normal;
attribute vec2 a_texCoord0;

// -------------------------------------------------------------
// varyings
// -------------------------------------------------------------

varying vec2 texCoord_metal;
varying vec2 texCoord_force;
varying vec2 texCoord_ring;
varying vec2 texCoord_center;

// -------------------------------------------------------------
// Vertex Shader
// -------------------------------------------------------------

void main(void)
{

   // computer UV coordinates of each texture region
   texCoord_metal = a_texCoord0 * ( tr011 - tr000 ) + tr000;
   texCoord_force = a_texCoord0 * ( tr111 - tr100 ) + tr100;
   texCoord_ring = a_texCoord0 * ( tr211 - tr200 ) + tr200;
   texCoord_center = a_texCoord0 * ( tr311 - tr300 ) + tr300;
   
   // Output vertex position
   gl_Position = matViewProjection * a_position;
}
