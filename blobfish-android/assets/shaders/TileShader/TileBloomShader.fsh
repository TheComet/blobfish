// -------------------------------------------------------------
// Tile Bloom Shader - Renders the tile only with emissive
//                     for later processing in bloom shader
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// -------------------------------------------------------------
// Samplers
// -------------------------------------------------------------

uniform sampler2D textureSampler;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// emissive properties
uniform vec3 emissiveColour;
uniform float emissivePower;

// to pass in the UV coordinates of the texture
uniform vec2 tr000;
uniform vec2 tr011;

// -------------------------------------------------------------
// Varying
// -------------------------------------------------------------

varying vec2 texCoord;

// -------------------------------------------------------------
// Fragment Shader
// -------------------------------------------------------------

void main(void)
{

   // get emissive from emissive map
   float emissive = texture2D( textureSampler, texCoord ).r;

   // compute final colour
   gl_FragColor = vec4( emissive*emissiveColour*emissivePower, 1.0 );
}