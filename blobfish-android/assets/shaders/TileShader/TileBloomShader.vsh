// -------------------------------------------------------------
// Tile Bloom Shader - Renders the tile only with emissive
// by Alex Murray
// -------------------------------------------------------------

// -------------------------------------------------------------
// matrices
// -------------------------------------------------------------

uniform mat4 matViewProjection;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// emissive properties
uniform vec3 emissiveColour;

// to pass in the UV coordinates of the texture
uniform vec2 tr000;
uniform vec2 tr011;

// -------------------------------------------------------------
// attributes
// -------------------------------------------------------------

attribute vec4 a_position;
attribute vec2 a_texCoord0;

// -------------------------------------------------------------
// varyings
// -------------------------------------------------------------

varying vec2 texCoord;

// -------------------------------------------------------------
// Vertex Shader
// -------------------------------------------------------------

void main(void)
{
   // Texture coordinates for fragment shader
   texCoord = a_texCoord0 * ( tr011 - tr000 ) + tr000;
   
   // output vertex positions
   gl_Position = matViewProjection * a_position;
}
