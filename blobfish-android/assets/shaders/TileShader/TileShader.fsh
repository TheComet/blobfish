// -------------------------------------------------------------
// Tile Shader - Normal, specular and emissive lighting
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// -------------------------------------------------------------
// Defines
// -------------------------------------------------------------

#define AMBIENT_COLOUR vec3( 0.15, 0.15, 0.15 )

// -------------------------------------------------------------
// Samplers
// -------------------------------------------------------------

uniform sampler2D textureSampler;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// camera position
uniform vec4 cameraPos;

// light
uniform vec3 lightSource;

// emissive properties
uniform vec3 emissiveColour;

// emissive Power
uniform float emissivePower;

// shininess
uniform float shininess;

// to pass in the UV coordinates of each texture
uniform vec2 tr000;
uniform vec2 tr011;

// -------------------------------------------------------------
// Varying
// -------------------------------------------------------------

varying vec2 texCoord;
varying vec3 tsPosition;
varying vec3 tsCameraPosition;
varying vec3 tsLightSource;

// -------------------------------------------------------------
// Fragment Shader
// -------------------------------------------------------------

void main(void)
{

   // sample texture
   vec4 sample = texture2D( textureSampler, texCoord );

   // get normal from normal map
   vec3 normal = normalize( sample.rgb - 0.5 );

   // normalize the other tangent space vectors
   vec3 viewVector = normalize( tsCameraPosition - tsPosition );
   vec3 lightVector = normalize( tsLightSource - tsPosition );
   
   // calculate normal weight
   float nxDir = dot( reflect(-viewVector, normal), lightVector );
   
   // Emissive map = diffuse map (we can do this hack because the emissive map is a direct copy of diffuse map)

   gl_FragColor = vec4( (AMBIENT_COLOUR*sample.a) + (shininess*nxDir*sample.a*vec3(0.3,0.3,0.3)) + (sample.a*emissiveColour*0.5*emissivePower), 1.0 );
}