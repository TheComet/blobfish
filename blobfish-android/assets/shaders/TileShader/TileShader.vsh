// -------------------------------------------------------------
// Tile Shader - Normal, specular and emissive lighting
// by Alex Murray
// -------------------------------------------------------------

// -------------------------------------------------------------
// matrices
// -------------------------------------------------------------

uniform mat4 matViewProjection;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// camera position
uniform vec4 cameraPos;

// light
uniform vec3 lightSource;

// shininess
uniform float shininess;

// emissive properties
uniform vec3 emissiveColour;

// emissive Power
uniform float emissivePower;

// to pass in the UV coordinates of each texture
uniform vec2 tr000;
uniform vec2 tr011;

// -------------------------------------------------------------
// attributes
// -------------------------------------------------------------

attribute vec4 a_position;
attribute vec3 a_normal;
attribute vec2 a_texCoord0;

// -------------------------------------------------------------
// varyings
// -------------------------------------------------------------

varying vec2 texCoord;
varying vec3 tsPosition;
varying vec3 tsCameraPosition;
varying vec3 tsLightSource;

// -------------------------------------------------------------
// Vertex Shader
// -------------------------------------------------------------

void main(void)
{
   float foo = emissivePower;
   float bar = shininess;
   
   // computer UV coordinates of texture region
   texCoord = a_texCoord0 * ( tr011 - tr000 ) + tr000;
   
   // This is a dirty way of calculating tangent and binormal
   // It assumes the object normal never faces towards 1,0.01,1, which should be pretty damn rare
   // If this is ever the case, the face in question will not calculate its lighting correctly
   vec3 tangent = normalize( cross( vec3( 1.0, 0.01, 1.0 ), a_normal ) );
   vec3 binormal = normalize( cross( tangent, a_normal ) );
   mat3 matTBN = mat3( tangent, binormal, a_normal );
   
   // transform lighting information into tangent space
   tsPosition = vec3( a_position ) * matTBN;
   tsCameraPosition = vec3( cameraPos ) * matTBN;
   tsLightSource = lightSource * matTBN;

   // Output vertex position
   gl_Position = matViewProjection * a_position;
   
}
