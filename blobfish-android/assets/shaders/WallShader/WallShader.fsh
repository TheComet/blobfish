// -------------------------------------------------------------
// Wall Shader - Normal, specular and emissive lighting
// by Alex Murray
// -------------------------------------------------------------

#ifdef GL_ES
   #define LOWP lowp
   #define MEDP mediump
   #define HIGP highp
   precision lowp float;
#else
   #define LOWP
   #define MEDP
   #define HIGP
#endif

// -------------------------------------------------------------
// Samplers
// -------------------------------------------------------------

uniform sampler2D textureSampler;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// camera position
uniform vec3 cameraPosition;

// light
uniform vec3 lightSource;

// colour of ambient light
uniform vec3 ambientColour;

// colour of diffuse lighting
uniform vec3 diffuseColour;

// colour of specular highlights
uniform vec3 specularColour;

// shininess of the surface
uniform float shininess;

// emissive properties
uniform vec3 emissiveColour;
uniform float emissivePower;

// -------------------------------------------------------------
// Varying
// -------------------------------------------------------------

varying vec2 texCoord_diffuse;
varying vec2 texCoord_normal;
varying vec2 texCoord_emissive;
varying vec3 tsPosition;
varying vec3 tsCameraPosition;
varying vec3 tsLightSource;

// -------------------------------------------------------------
// Fragment Shader
// -------------------------------------------------------------

void main(void)
{

   // get colour of diffuse texture
   vec3 colour = texture2D( textureSampler, texCoord_diffuse ).rgb;

   // get normal from normal map
   vec3 normal = texture2D( textureSampler, texCoord_normal ).rgb * 2.0 - 1.0;
   
   // normalize the other tangent space vectors
   vec3 viewVector = normalize( tsCameraPosition - tsPosition );
   vec3 lightVector = normalize( tsLightSource - tsPosition );
   
   // calculate lighting values
   float nxDir = max( 0.0, dot( normal, lightVector ) );
   
   // calculate specular colour
   float specularPower = 0.0;
   if( nxDir != 0.0 )
   {
      vec3 halfVector = normalize( lightVector + viewVector );
      float nxHalf = max( 0.0, dot( normal, halfVector ) );
      specularPower = pow( nxHalf, shininess );
   }
   vec3 specular = specularColour * specularPower * shininess;
   
   // get emissive from emissive map
   vec3 emissive = texture2D( textureSampler, texCoord_emissive ).rgb;
   emissive *= emissivePower;

   // compute final colour
   colour = (ambientColour*colour) + (diffuseColour*nxDir*colour*0.333) + (specular*0.333) + (emissive*0.333);

   gl_FragColor = vec4( colour, 1.0 );
}