// -------------------------------------------------------------
// Wall Shader - Normal, specular and emissive lighting
// by Alex Murray
// -------------------------------------------------------------

// -------------------------------------------------------------
// matrices
// -------------------------------------------------------------

uniform mat4 matViewProjection;

// -------------------------------------------------------------
// Uniforms
// -------------------------------------------------------------

// camera position
uniform vec4 cameraPosition;

// light
uniform vec3 lightSource;

// colour of ambient light
uniform vec3 ambientColour;

// colour of diffuse lighting
uniform vec3 diffuseColour;

// colour of specular highlights
uniform vec3 specularColour;

// shininess of the surface
uniform float shininess;

// emissive properties
uniform vec3 emissiveColour;
uniform float emissivePower;

// to pass in the UV coordinates of each texture
uniform vec2 tr000;
uniform vec2 tr011;
uniform vec2 tr100;
uniform vec2 tr111;
uniform vec2 tr200;
uniform vec2 tr211;

// -------------------------------------------------------------
// attributes
// -------------------------------------------------------------

attribute vec4 a_position;
attribute vec3 a_normal;
attribute vec2 a_texCoord0;

// -------------------------------------------------------------
// varyings
// -------------------------------------------------------------

varying vec2 texCoord_diffuse;
varying vec2 texCoord_normal;
varying vec2 texCoord_emissive;
varying vec3 tsPosition;
varying vec3 tsCameraPosition;
varying vec3 tsLightSource;

// -------------------------------------------------------------
// Vertex Shader
// -------------------------------------------------------------

void main(void)
{

   // computer UV coordinates of each texture region
   texCoord_diffuse = a_texCoord0 * ( tr011 - tr000 ) + tr000;
   texCoord_normal = a_texCoord0 * ( tr111 - tr100 ) + tr100;
   texCoord_emissive = a_texCoord0 * ( tr211 - tr200 ) + tr200;
   
   // This is a dirty way of calculating tangent and binormal
   // It assumes the object normal never faces towards 1,0.01,1, which should be pretty damn rare
   // If this is ever the case, the face in question will not calculate its lighting correctly
   vec3 tangent = normalize( cross( vec3( 1.0, 0.01, 1.0 ), a_normal ) );
   vec3 binormal = normalize( cross( tangent, a_normal ) );
   mat3 matTBN = mat3( tangent, binormal, a_normal );
   
   // transform lighting information into tangent space
   tsPosition = vec3( a_position ) * matTBN;
   tsCameraPosition = vec3( cameraPosition ) * matTBN;
   tsLightSource = lightSource * matTBN;
   
   // output vertex position
   gl_Position = matViewProjection * a_position;
}
