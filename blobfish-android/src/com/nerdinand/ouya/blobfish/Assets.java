package com.nerdinand.ouya.blobfish;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g3d.loaders.md2.MD2Loader;
import com.badlogic.gdx.graphics.g3d.loaders.wavefront.ObjLoader;
import com.badlogic.gdx.graphics.g3d.materials.Material;
import com.badlogic.gdx.graphics.g3d.model.keyframe.KeyframedAnimation;
import com.badlogic.gdx.graphics.g3d.model.keyframe.KeyframedModel;
import com.badlogic.gdx.graphics.g3d.model.still.StillModel;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public class Assets {
	public static final String TEXTURE_ATLAS_UI = "textures/ui.atlas";
	public static final String TEXTURE_ATLAS_GAME = "textures/game.atlas";

	public static final String FONT_ARIAL_20 = "ui/Arial_20.fnt";

	public static final String SOUND_CHIRP = "sounds/chirp.wav";

	public static final String PIXMAP_MINIMAP_HIGH_WALL = "pixmaps/minimap/wall.png"; // TODO create minimap icon
	public static final String PIXMAP_MINIMAP_WALL = "pixmaps/minimap/wall.png";
	public static final String PIXMAP_MINIMAP_TRIGGER = "pixmaps/minimap/trigger.png";
	public static final String PIXMAP_MINIMAP_TELEPORTER = "pixmaps/minimap/teleporter.png";
	public static final String PIXMAP_MINIMAP_SPAWN = "pixmaps/minimap/spawn.png";
	public static final String PIXMAP_MINIMAP_RESTRICT_SPAWN = "pixmaps/minimap/restrict_spawn.png";
	public static final String PIXMAP_MINIMAP_RESTRICT_POWERUP = "pixmaps/minimap/restrict_powerup.png";
	public static final String PIXMAP_MINIMAP_EMPTY = "pixmaps/minimap/empty.png";
	public static final String PIXMAP_MINIMAP_CONNECT = "pixmaps/minimap/connect.png";
	public static final String PIXMAP_MINIMAP_COLOURTILE = "pixmaps/minimap/colourtile.png";
	public static final String PIXMAP_MINIMAP_JUMP = "pixmaps/minimap/jump.png";

	public static final String PIXMAP_MAP_PREVIEW_PLACEHOLDER = "pixmaps/map_preview_placeholder.png";

	public static final String TEXTURE_CODEGESTALT_LOGO = "logos/CODEGESTALT_logo_white";
	public static final String TEXTURE_CANTERLOCK_LOGO = "logos/canterlock-logo";

	public static final String TEXTURE_POWERUP_ICON_BLACK_HOLE = "powerup/black_hole";
	public static final String TEXTURE_POWERUP_ICON_BUMP = "powerup/bump";
	public static final String TEXTURE_POWERUP_ICON_DRUNK = "powerup/drunk";
	public static final String TEXTURE_POWERUP_ICON_FORCE_FIELD = "powerup/force_field";
	public static final String TEXTURE_POWERUP_ICON_JUMP = "powerup/jump";
	public static final String TEXTURE_POWERUP_ICON_PLAYER_SHOCK_WAVE = "powerup/player_shock_wave";
	public static final String TEXTURE_POWERUP_ICON_SIZE_BOOST = "powerup/size_boost";
	public static final String TEXTURE_POWERUP_ICON_SLOW_DOWN = "powerup/slow_down";
	public static final String TEXTURE_POWERUP_ICON_SPEED_BOOST = "powerup/speed_boost";
	public static final String TEXTURE_POWERUP_ICON_TILE_KILLER = "powerup/tile_killer";
	public static final String TEXTURE_POWERUP_ICON_TILE_SHOCK_WAVE = "powerup/tile_shock_wave";
	public static final String TEXTURE_POWERUP_ICON_TILE_STEALING = "powerup/tile_stealing";


	public static final String TEXTURE_COLOURTILE_DIFFUSE_NORMAL_EMISSIVE = "models/tiles/colourtile/colourtile_diffuse_normal_emissive";

	public static final String TEXTURE_JUMPTILE = "models/tiles/jumptile/jumptile";

	public static final String TEXTURE_PLAYER_DIFFUSE = "models/players/ufo_diffuse";
	public static final String TEXTURE_PLAYER_NORMAL = "models/players/ufo_normal";

	public static final String TEXTURE_WALLTILE_DIFFUSE = "models/tiles/walltile/walltile_diffuse";
	public static final String TEXTURE_WALLTILE_NORMAL = "models/tiles/walltile/walltile_normal";
	public static final String TEXTURE_WALLTILE_EMISSIVE = "models/tiles/walltile/walltile_emissiveMap";

	public static final String TEXTURE_TELEPORTERTILE_METAL = "models/tiles/teleportertile/metal";
	public static final String TEXTURE_TELEPORTERTILE_FORCE = "models/tiles/teleportertile/force";
	public static final String TEXTURE_TELEPORTERTILE_RING = "models/tiles/teleportertile/ring";
	public static final String TEXTURE_TELEPORTERTILE_CENTRE = "models/tiles/teleportertile/centre";

	public static final String TEXTURE_POWERUP_DIFFUSE = "models/powerup/powerup_diffuse";
	public static final String TEXTURE_POWERUP_EMISSIVE = "models/powerup/powerup_emissiveMap";

	public static final String TEXTURE_OUYA_ICON_O = "OUYA_Buttons/OUYA_O";
	public static final String TEXTURE_OUYA_ICON_A = "OUYA_Buttons/OUYA_A";
	public static final String TEXTURE_OUYA_ICON_LS = "OUYA_Buttons/OUYA_LS";
	public static final String TEXTURE_OUYA_ICON_SYSTEM = "OUYA_Buttons/OUYA_SYSTEM";

	private static final String TAG = "Assets";
	public static StillModel playerModel;

	public static ShaderProgram colorShader;
	public static ShaderProgram tileShader;
	public static ShaderProgram powerUpShader;

	public static KeyframedModel teleporterModel;

	public static final AssetManager assetManager = new AssetManager();

	public static final HashMap<String, AtlasRegion> regionMap = new HashMap<String, TextureAtlas.AtlasRegion>();

	public static KeyframedModel powerUpModel;
	public static KeyframedModel blackHoleModel;
	public static KeyframedModel jumpTileModel;
	public static KeyframedAnimation powerUpAnimation;
	public static KeyframedAnimation blackHoleAnimation;
	public static KeyframedAnimation playerAnimation;

	public static StillModel colourTileModel;
	public static StillModel wallTileModel;
	public static StillModel highWallTileModel;
	public static StillModel screenAlignedQuadModel;

	public static ShaderProgram finalBloomShader;
	public static ShaderProgram tileBloomShader;
	public static ShaderProgram wallShader;
	public static ShaderProgram blurHBloomShader;
	public static ShaderProgram blurVBloomShader;
	public static ShaderProgram downSampleBloomShader;
	public static ShaderProgram wallBloomShader;
	public static ShaderProgram playerShader;
	public static ShaderProgram playerBloomShader;
	public static ShaderProgram powerUpBloomShader;
	public static ShaderProgram jumpTileShader;
	public static ShaderProgram teleporterTileShader;
	public static ShaderProgram highWallShader;
	public static ShaderProgram highWallBloomShader;

	public static void loadGameAssets() {
		loadModels();
		loadShaders();
	}

	private static void enqueueSounds() {
		assetManager.load(SOUND_CHIRP, Sound.class);
	}

	private static void loadShaders() {
		colorShader = loadShader("shaders/color.vsh", "shaders/color.fsh");
		tileShader = loadShader("shaders/TileShader/TileShader.vsh", "shaders/TileShader/TileShader.fsh");

		powerUpShader = loadShader("shaders/PowerUpShader/PowerUpShader.vsh", "shaders/PowerUpShader/PowerUpShader.fsh");
		powerUpBloomShader = loadShader("shaders/PowerUpShader/PowerUpBloomShader.vsh", "shaders/PowerUpShader/PowerUpBloomShader.fsh");

		wallShader = loadShader("shaders/WallShader/WallShader.vsh", "shaders/WallShader/WallShader.fsh");
		wallBloomShader = loadShader("shaders/WallShader/WallBloomShader.vsh", "shaders/WallShader/WallBloomShader.fsh");

		highWallShader = loadShader("shaders/HighWallShader/HighWallShader.vsh", "shaders/HighWallShader/HighWallShader.fsh");
		highWallBloomShader = loadShader("shaders/HighWallShader/HighWallBloomShader.vsh", "shaders/HighWallShader/HighWallBloomShader.fsh");

		jumpTileShader = loadShader("shaders/JumpTileShader/JumpTileShader.vsh", "shaders/JumpTileShader/JumpTileShader.fsh");
		teleporterTileShader = loadShader("shaders/TeleporterTileShader/TeleporterTileShader.vsh", "shaders/TeleporterTileShader/TeleporterTileShader.fsh");

		tileBloomShader = loadShader("shaders/TileShader/TileBloomShader.vsh", "shaders/TileShader/TileBloomShader.fsh");
		blurHBloomShader = loadShader("shaders/Bloom/blurH.vsh", "shaders/Bloom/blurH.fsh");
		blurVBloomShader = loadShader("shaders/Bloom/blurV.vsh", "shaders/Bloom/blurV.fsh");
		downSampleBloomShader = loadShader("shaders/Bloom/downSample.vsh", "shaders/Bloom/downSample.fsh");
		finalBloomShader = loadShader("shaders/Bloom/final.vsh", "shaders/Bloom/final.fsh");

		playerShader = loadShader("shaders/PlayerShader/playerShader.vsh", "shaders/PlayerShader/playerShader.fsh");
		playerBloomShader = loadShader("shaders/PlayerShader/playerBloomShader.vsh", "shaders/PlayerShader/playerBloomShader.fsh");
	}

	private static ShaderProgram loadShader(String vertexShader, String fragmentShader) {
		FileHandle vertexShaderFile = Gdx.files.internal(vertexShader);
		FileHandle fragmentShaderFile = Gdx.files.internal(fragmentShader);
		ShaderProgram shader = new ShaderProgram(vertexShaderFile, fragmentShaderFile);

		if (shader.isCompiled()) {
			Gdx.app.log(TAG, "Shader compiled. vsh: " + vertexShader + " fsh: " + fragmentShader + " Log: ");
			Gdx.app.log(TAG, shader.getLog());
		} else {
			throw new IllegalArgumentException("couldn't compile shader " + vertexShader + ", " + fragmentShader + ": " + shader.getLog());
		}

		return shader;
	}

	private static void loadModels() {
		// object loaders
		ObjLoader objLoader = new ObjLoader();
		MD2Loader md2Loader = new MD2Loader();
		Mesh mesh;

		wallTileModel = objLoader.loadObj(Gdx.files.internal("models/tiles/walltile/walltile.obj"));
		wallTileModel.setMaterial(new Material());

		highWallTileModel = objLoader.loadObj(Gdx.files.internal("models/tiles/highwalltile/highwalltile.obj"));
		highWallTileModel.setMaterial(new Material());

		teleporterModel = md2Loader.load(Gdx.files.internal("models/tiles/teleportertile/teleportertile.md2"), 0.1f);
		teleporterModel.setMaterial(new Material());

		// playerModel = md2Loader.load(Gdx.files.internal("models/players/ufo.md2"), 0.1f);
		// playerModel.setMaterial(new Material());
		// playerAnimation = playerModel.getAnimation("all");

		playerModel = objLoader.loadObj(Gdx.files.internal("models/players/ufo.obj"));
		playerModel.setMaterial(new Material());

		powerUpModel = md2Loader.load(Gdx.files.internal("models/powerup/power_up.md2"), 0.1f);
		powerUpModel.setMaterial(new Material());
		powerUpAnimation = powerUpModel.getAnimation("all");

		jumpTileModel = md2Loader.load(Gdx.files.internal("models/tiles/jumptile/jumptile.md2"), 0.1f);
		jumpTileModel.setMaterial(new Material());

		colourTileModel = objLoader.loadObj(Gdx.files.internal("models/tiles/colourtile/colourtile.obj"));
		colourTileModel.setMaterial(new Material());

		screenAlignedQuadModel = objLoader.loadObj(Gdx.files.internal("shaders/Bloom/screenAlignedQuad.obj"));
		screenAlignedQuadModel.setMaterial(new Material());

		blackHoleModel = md2Loader.load(Gdx.files.internal("models/blackhole/blackhole.md2"), 0.1f);
		blackHoleModel.setMaterial(new Material());
		blackHoleAnimation = blackHoleModel.getAnimation("all");
	}

	public static void enqueueUIAssets() {
		assetManager.load(FONT_ARIAL_20, BitmapFont.class);

		assetManager.load(TEXTURE_ATLAS_UI, TextureAtlas.class);

		assetManager.load(PIXMAP_MINIMAP_HIGH_WALL, Pixmap.class);
		assetManager.load(PIXMAP_MINIMAP_WALL, Pixmap.class);
		assetManager.load(PIXMAP_MINIMAP_TRIGGER, Pixmap.class);
		assetManager.load(PIXMAP_MINIMAP_TELEPORTER, Pixmap.class);
		assetManager.load(PIXMAP_MINIMAP_SPAWN, Pixmap.class);
		assetManager.load(PIXMAP_MINIMAP_RESTRICT_SPAWN, Pixmap.class);
		assetManager.load(PIXMAP_MINIMAP_RESTRICT_POWERUP, Pixmap.class);
		assetManager.load(PIXMAP_MINIMAP_EMPTY, Pixmap.class);
		assetManager.load(PIXMAP_MINIMAP_CONNECT, Pixmap.class);
		assetManager.load(PIXMAP_MINIMAP_COLOURTILE, Pixmap.class);
		assetManager.load(PIXMAP_MINIMAP_JUMP, Pixmap.class);
	}

	public static void dispose() {
		disposeModels();
		disposeShaders();

		regionMap.clear();

		assetManager.dispose();
	}

	private static void disposeShaders() {
		if (colorShader != null)
			colorShader.dispose();
		if (tileShader != null)
			tileShader.dispose();
		if (powerUpShader != null)
			powerUpShader.dispose();
		if (playerShader != null)
			playerShader.dispose();
		if (playerBloomShader != null)
			playerBloomShader.dispose();
		if (jumpTileShader != null)
			jumpTileShader.dispose();
	}

	private static void disposeModels() {
		if (playerModel != null)
			playerModel.dispose();
		if (colourTileModel != null)
			colourTileModel.dispose();
		if (wallTileModel != null)
			wallTileModel.dispose();
		if (teleporterModel != null)
			teleporterModel.dispose();
		if (powerUpModel != null)
			powerUpModel.dispose();
		if (blackHoleModel != null)
			blackHoleModel.dispose();
	}

	public static void enqueueGameAssets() {
		enqueueSounds();

		assetManager.load(TEXTURE_ATLAS_GAME, TextureAtlas.class);
	}

	public static AtlasRegion getRegion(String atlas, String path) {
		AtlasRegion region;

		if ((region = regionMap.get(path)) == null) {
			region = assetManager.get(atlas, TextureAtlas.class).findRegion(path);
			regionMap.put(path, region);
		}

		return region;
	}
}
