package com.nerdinand.ouya.blobfish;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.nerdinand.ouya.blobfish.input.FunctionButtonsListener;
import com.nerdinand.ouya.blobfish.input.InputManager;
import com.nerdinand.ouya.blobfish.screen.BlobfishScreen;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;

public class Blobfish extends Game implements FunctionButtonsListener {
	private static final String TAG = "Blobfish";

	private FPSLogger fps;

	private static InputManager inputManager;

	public static InputManager getInputManager() {
		return inputManager;
	}

	@Override
	public void create() {
		inputManager = new InputManager();

		Assets.enqueueUIAssets();

		Assets.assetManager.finishLoading();

		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		fps = new FPSLogger();

		setScreen(ScreenStateMachine.getStartScreen());
		
		inputManager.addListener(this);

	}

	@Override
	public void render() {
		BlobfishScreen currentScreen = getScreen();

		// update the screen
		currentScreen.render(Gdx.graphics.getDeltaTime());

		if (currentScreen.isDone()) {
//			currentScreen.dispose();

			setScreen(ScreenStateMachine.getNextScreen());
		}

		fps.log();
	}

	@Override
	public BlobfishScreen getScreen() {
		return (BlobfishScreen) super.getScreen();
	}

	@Override
	public void dispose() {
		Assets.dispose();
		super.dispose();
	}

	@Override
	public void functionButtonDown(LocalPlayer player, FunctionButtonType functionType) {
		switch (functionType) {
		case CANCEL:
			if (getScreen().backAllowed()){
				BlobfishScreen previousScreen = ScreenStateMachine.getPreviousScreen();
				previousScreen.setDone(false);
				
				setScreen(previousScreen);
			}
			break;
			
		case HOME:
			BlobfishScreen pauseScreen = ScreenStateMachine.getPauseScreen();
			setScreen(pauseScreen);
			break;

		default:
			break;
		}
	}

	@Override
	public void functionButtonUp(LocalPlayer player, FunctionButtonType functionType) {
		// TODO Auto-generated method stub
		
	}
}
