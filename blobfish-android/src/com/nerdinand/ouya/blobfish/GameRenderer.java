package com.nerdinand.ouya.blobfish;

import java.util.HashMap;
import java.util.Map.Entry;

import bloom.Bloom;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.model.keyframe.KeyframedModel;
import com.badlogic.gdx.graphics.g3d.model.still.StillModel;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.ColourTile;
import com.nerdinand.ouya.blobfish.map.tile.Tile.TileType;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.Simulation;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUp.PowerUpType;

public class GameRenderer implements Disposable {
	private static final String TAG = "GameRenderer";

	private static final Vector3 RELATIVE_LIGHT_POSITION = new Vector3(20.0f, 20.0f, 20.0f);
	private Vector3 lightAbsolut = new Vector3();
	private static final Vector3 DIFFUSE_COLOUR = new Vector3(1.0f, 1.0f, 1.0f);
	private static final Vector3 EMISSIVE_COLOUR = new Vector3(0f, 0f, 0f);
	private static final Vector3 SPECULAR_COLOUR = new Vector3(1f, 1f, 1f);
	private static final Vector3 AMBIENT_COLOUR = new Vector3(0.15f, 0.15f, 0.15f);
	private static final float EMISSIVE_POWER = 0.6f;
	private static final float BLOOM_INTENSITY = 15.0f;

	private static final boolean ENABLE_BLOOM = true;

	private static final double FULL_ROTATION = Math.PI * 2;
	private static final double PLAYER_ROTATION_SPEED_PER_SECOND = 2 * FULL_ROTATION;

	/** view and transform matrix for text rendering and transforming 3D objects **/
	private final Matrix4 transform = new Matrix4();

	public PerspectiveCamera camera;

	private TextureManager colourTileTextureManager;
	private TextureManager wallTileTextureManager;
	private TextureManager playerTextureManager;
	private TextureManager powerUpTextureManager;
	private TextureManager jumpTileTextureManager;
	private TextureManager teleporterTileTextureManager;

	private SpriteBatch hudBatch = new SpriteBatch();

	private Matrix4 viewMatrix = new Matrix4();
	private Bloom bloom;

	private BitmapFont font;

	private double currentPlayerAngle;

	private float powerUpAnimationTime;

	public GameRenderer() {
		if (!Gdx.graphics.isGL20Available()) {
			throw new RuntimeException("OpenGL 2.0 not available!");
		}
		colourTileTextureManager = new TextureManager();
		colourTileTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_COLOURTILE_DIFFUSE_NORMAL_EMISSIVE));

		wallTileTextureManager = new TextureManager();
		wallTileTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_WALLTILE_DIFFUSE));
		wallTileTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_WALLTILE_NORMAL));
		wallTileTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_WALLTILE_EMISSIVE));

		playerTextureManager = new TextureManager();
		playerTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_PLAYER_DIFFUSE));
		playerTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_PLAYER_NORMAL));
		playerTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_PLAYER_NORMAL));

		powerUpTextureManager = new TextureManager();
		powerUpTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_POWERUP_DIFFUSE));
		powerUpTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_POWERUP_EMISSIVE));

		jumpTileTextureManager = new TextureManager();
		jumpTileTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_JUMPTILE));

		teleporterTileTextureManager = new TextureManager();
		teleporterTileTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_TELEPORTERTILE_METAL));
		teleporterTileTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_TELEPORTERTILE_FORCE));
		teleporterTileTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_TELEPORTERTILE_RING));
		teleporterTileTextureManager.addTextureRegion(Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, Assets.TEXTURE_TELEPORTERTILE_CENTRE));

		bloom = new Bloom();
		bloom.setBlending(true);
		bloom.setBloomIntensity(BLOOM_INTENSITY);

		font = Assets.assetManager.get(Assets.FONT_ARIAL_20);
	}

	public void render(Simulation simulation, float deltaTime) {
		GLCommon gl = Gdx.gl;

		updatePlayerAngle(deltaTime);

		clearThatShit(gl);

		final Array<LocalPlayer> localPlayerList = simulation.getLocalPlayerList();

		gl.glEnable(GL10.GL_CULL_FACE);
		gl.glEnable(GL10.GL_DEPTH_TEST);

		renderPlayers(localPlayerList, simulation.getPlayerList(), false);
		renderTileField(localPlayerList, simulation.getMap().getTileField(), false);
		renderPowerUps(localPlayerList, simulation.getPowerUpManager().getMapPowerUps(), false);

		if (ENABLE_BLOOM) {
			bloom.capture();
			renderPlayers(localPlayerList, simulation.getPlayerList(), true);
			renderTileField(localPlayerList, simulation.getMap().getTileField(), true);
			renderPowerUps(localPlayerList, simulation.getPowerUpManager().getMapPowerUps(), true);
			bloom.render();
		}

		gl.glDisable(GL10.GL_CULL_FACE);
		gl.glDisable(GL10.GL_DEPTH_TEST);

		hudBatch.begin();
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		for (LocalPlayer player : localPlayerList) {
			viewMatrix.setToOrtho2D(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			hudBatch.setProjectionMatrix(viewMatrix);

			renderHUD(hudBatch, simulation, player);
		}
		renderDebug(hudBatch);
		hudBatch.end();

		// renderDebug(simulation);
	}

	private void updatePlayerAngle(final float deltaTime) {
		currentPlayerAngle += deltaTime * PLAYER_ROTATION_SPEED_PER_SECOND;

		if (currentPlayerAngle >= FULL_ROTATION) {
			currentPlayerAngle = 0;
		}
		
		powerUpAnimationTime += deltaTime;
		if (powerUpAnimationTime >= Assets.powerUpAnimation.totalDuration) {
			powerUpAnimationTime = 0;
		}

	}

	private void clearThatShit(GLCommon gl) {
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		gl.glEnable(GL10.GL_DEPTH_TEST);
		gl.glDisable(GL10.GL_CULL_FACE);
	}

	private void setProjectionAndCamera(LocalPlayer player) {
		Rectangle screen = player.getScreen();

		Gdx.gl.glViewport((int) screen.x, (int) screen.y, (int) screen.width, (int) screen.height);

		setCamera(player.getCamera());
		getCamera().update();

		if (player.getPosition() != null) {
			lightAbsolut = (getCamera().position.cpy().sub(player.getPosition().cpy())).mul(2.0f);
			lightAbsolut.y += 80.0f;
		}
	}

	private void renderPlayers(Array<LocalPlayer> localPlayers, Array<? extends Player> players, boolean renderBloom) {
		playerTextureManager.bindTextures();

		for (LocalPlayer localPlayer : localPlayers) {
			setProjectionAndCamera(localPlayer);

			ShaderProgram shader;
			if (renderBloom) {
				shader = Assets.playerBloomShader;
			} else {
				shader = Assets.playerShader;
			}

			shader.begin();

			if (renderBloom) {
				playerTextureManager.setTextureCoordinates(shader, 2);

				shader.setUniformf("emissivePower", (float) EMISSIVE_POWER * 1.5f);
			} else {
				playerTextureManager.setTextureCoordinates(shader);

				shader.setUniformf("shininess", 80.0f);
				shader.setUniformf("cameraPosition", getCamera().position);
				shader.setUniformf("lightSource", getCamera().position.cpy().add(RELATIVE_LIGHT_POSITION));
				shader.setUniformf("ambientColour", AMBIENT_COLOUR);
				shader.setUniformf("specularColour", SPECULAR_COLOUR);
				shader.setUniformf("specularIntensity", 0.2f);
			}

			shader.setUniformf("angleY", (float) currentPlayerAngle);

			for (Player player : players) {
				Vector3 position = player.getPosition();

				if (position != null) {
					transform.set(getCamera().combined);
					transform.translate(position);
				}

				shader.setUniformMatrix("matViewProjection", transform);

				Color color = player.getColor();
				shader.setUniformf("shipColour", new Vector3(color.r, color.g, color.b));

				final StillModel playerModel = Assets.playerModel;

				boolean hasTriggeredSizeBoostPowerUp = (player.hasTriggeredPowerup() && (player.getTriggeredPowerUp().getPowerUpType() == PowerUpType.SIZE_BOOST_POWER_UP));
				if (hasTriggeredSizeBoostPowerUp) {
					shader.setUniformf("scale", new Vector3(3, 3, 3));
				} else {
					shader.setUniformf("scale", new Vector3(1, 1, 1));
				}

				playerModel.render(shader);
			}
			shader.end();
		}
	}

	private void renderTileField(Array<LocalPlayer> localPlayerList, TileField tileField, boolean renderBloom) {
		for (LocalPlayer localPlayer : localPlayerList) {
			setProjectionAndCamera(localPlayer);

			HashMap<TileType, Array<VisibleTile>> visibleTilesMap = tileField.getVisibleTiles(localPlayer);

			colourTileTextureManager.bindTextures();
			wallTileTextureManager.bindTextures();
			jumpTileTextureManager.bindTextures();
			teleporterTileTextureManager.bindTextures();

			for (Entry<TileType, Array<VisibleTile>> entry : visibleTilesMap.entrySet()) {
				renderTiles(entry.getKey(), entry.getValue(), renderBloom);
			}
		}
	}

	private void renderTiles(TileType type, Array<VisibleTile> tiles, boolean renderBloom) {
		switch (type) {
		case COLOUR_TILE:
			renderColourTiles(tiles, renderBloom);

			break;
		case JUMP_TILE:
			renderJumpTiles(tiles, renderBloom);

			break;
		case TELEPORTER_TILE:
			renderTeleporterTiles(tiles, renderBloom);

			break;
		case WALL_TILE:
			renderWallTiles(tiles, renderBloom);

		case HIGH_WALL_TILE:
			renderHighWallTiles(tiles, renderBloom);
			
			break;
		default:
			break;
		}
	}

	private void renderColourTiles(Array<VisibleTile> tiles, boolean renderBloom) {
		ShaderProgram shader;

		if (renderBloom) {
			shader = Assets.tileBloomShader;
		} else {
			shader = Assets.tileShader;
		}

		shader.begin();

		colourTileTextureManager.setTextureCoordinates(shader);

		shader.setUniformf("cameraPos", getCamera().position);
		shader.setUniformf("lightSource", lightAbsolut);

		for (VisibleTile tile : tiles) {
			Vector3 position = tile.getPosition();
			transform.set(getCamera().combined);
			transform.translate(position);

			shader.setUniformMatrix("matViewProjection", transform);
			shader.setUniformf("emissiveColour", EMISSIVE_COLOUR);
			shader.setUniformf("emissivePower", EMISSIVE_POWER);
			shader.setUniformf("shininess", 1.2f);

			if (tile instanceof ColourTile) {
				ColourTile colourTile = (ColourTile) tile;
				Player owner = colourTile.getOwner();

				if (owner != null) {
					Color color = owner.getColor();
					shader.setUniformf("emissiveColour", new Vector3(color.r, color.g, color.b));
				}
				if (colourTile.isLocked()) {
					shader.setUniformf("emissivePower", 1.0f);
					Assets.colourTileModel.render(shader);
				} else {
					Assets.colourTileModel.render(shader);
				}
			}
		}
		shader.end();
	}

	private void renderJumpTiles(Array<VisibleTile> tiles, boolean renderBloom) {

		ShaderProgram shader;
		if (renderBloom) {
			// TODO implement proper bloom shader
			shader = Assets.jumpTileShader;
		} else {
			shader = Assets.jumpTileShader;
		}

		shader.begin();

		jumpTileTextureManager.setTextureCoordinates(shader);

		for (VisibleTile tile : tiles) {
			Vector3 position = tile.getPosition();
			transform.set(getCamera().combined);
			transform.translate(position);
			shader.setUniformMatrix("matViewProjection", transform);
			Assets.jumpTileModel.render(shader);
		}

		shader.end();
	}

	private void renderTeleporterTiles(Array<VisibleTile> tiles, boolean renderBloom) {
		ShaderProgram shader;

		if (renderBloom) {
			// TODO implement proper bloom shader
			shader = Assets.teleporterTileShader;
		} else {
			shader = Assets.teleporterTileShader;
		}

		shader.begin();
		teleporterTileTextureManager.setTextureCoordinates(shader);
		for (VisibleTile tile : tiles) {
			if (tile.getTileType() == TileType.TELEPORTER_TILE) {
				Vector3 position = tile.getPosition();
				transform.set(getCamera().combined);
				transform.translate(position);
				shader.setUniformMatrix("matViewProjection", transform);
				Assets.teleporterModel.render(shader);
			}
		}
		shader.end();
	}

	private void renderWallTiles(Array<VisibleTile> tiles, boolean renderBloom) {
		ShaderProgram shader;

		if (renderBloom) {
			shader = Assets.wallBloomShader;
		} else {
			shader = Assets.wallShader;
		}

		shader.begin();
		wallTileTextureManager.setTextureCoordinates(shader);
		shader.setUniformf("shininess", 20.0f);
		shader.setUniformf("emissivePower", 1.2f);

		shader.setUniformf("cameraPosition", getCamera().position);
		shader.setUniformf("lightSource", lightAbsolut);

		shader.setUniformf("ambientColour", AMBIENT_COLOUR);
		shader.setUniformf("specularColour", SPECULAR_COLOUR);
		shader.setUniformf("diffuseColour", DIFFUSE_COLOUR);

		for (VisibleTile tile : tiles) {
			Vector3 position = tile.getPosition();
			transform.set(getCamera().combined);
			transform.translate(position);
			shader.setUniformMatrix("matViewProjection", transform);

			Assets.wallTileModel.render(shader);
		}
		shader.end();

	}
	
	private void renderHighWallTiles(Array<VisibleTile> tiles, boolean renderBloom) {
		ShaderProgram shader;

		if (renderBloom) {
			shader = Assets.highWallBloomShader;
		} else {
			shader = Assets.highWallShader;
		}

		shader.begin();
		wallTileTextureManager.setTextureCoordinates(shader);
		shader.setUniformf("shininess", 20.0f);
		shader.setUniformf("emissivePower", 1.2f);

		shader.setUniformf("cameraPosition", getCamera().position);
		shader.setUniformf("lightSource", lightAbsolut);

		shader.setUniformf("ambientColour", AMBIENT_COLOUR);
		shader.setUniformf("specularColour", SPECULAR_COLOUR);
		shader.setUniformf("diffuseColour", DIFFUSE_COLOUR);

		for (VisibleTile tile : tiles) {
			Vector3 position = tile.getPosition();
			transform.set(getCamera().combined);
			transform.translate(position);
			shader.setUniformMatrix("matViewProjection", transform);

			Assets.highWallTileModel.render(shader);
		}
		shader.end();

	}

	private void renderPowerUps(Array<LocalPlayer> localPlayerList, Array<PowerUp> powerUps, boolean renderBloom) {
		powerUpTextureManager.bindTextures();

		for (LocalPlayer localPlayer : localPlayerList) {
			setProjectionAndCamera(localPlayer);

			ShaderProgram shader;
			if (renderBloom) {
				shader = Assets.powerUpBloomShader;
			} else {
				shader = Assets.powerUpShader;
			}

			shader.begin();

			if (renderBloom) {
				powerUpTextureManager.setTextureCoordinates(shader, 1);
			} else {
				powerUpTextureManager.setTextureCoordinates(shader, 0);
			}

			for (PowerUp powerUp : powerUps) {
				Vector3 position = powerUp.getSpawnTile().getPosition();
				transform.set(getCamera().combined);
				transform.translate(position);
				shader.setUniformMatrix("matViewProjection", transform);

				final KeyframedModel powerUpModel = Assets.powerUpModel;
				powerUpModel.setAnimation(Assets.powerUpAnimation.name, powerUpAnimationTime, false);
				
				powerUpModel.render(shader);
			}
			shader.end();
		}
	}

	private void renderHUD(SpriteBatch batch, Simulation simulation, LocalPlayer player) {
		Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0); // SpriteBatch expects this, better make it happy!

		int width = (int) player.getScreen().getWidth();
		int height = (int) player.getScreen().getHeight();

		int startX = (int) player.getScreen().x;
		int startY = (int) player.getScreen().y;

		int secondsLeft = simulation.getSecondsLeft();
		font.draw(batch, "" + secondsLeft, startX + 20, startY + height - 20);

		if (player.hasPowerUp()) {

			PowerUp powerUp = player.getPowerUp();
			TextureRegion textureRegion = powerUp.getIconTextureRegion();

			float x = startX + width - textureRegion.getRegionWidth();
			float y = startY + height - textureRegion.getRegionHeight();

			batch.draw(textureRegion, x, y);
		}
	}

	private void renderDebug(SpriteBatch spriteBatch) {
		StringBuffer string = new StringBuffer();
		string.append("FPS: " + Gdx.graphics.getFramesPerSecond() + "\n");
		font.drawMultiLine(spriteBatch, string, 10, 300);
	}

	public void resume() {
		bloom.resume();
	}

	@Override
	public void dispose() {
		// TODO actually dispose some shit
	}

	public PerspectiveCamera getCamera() {
		return camera;
	}

	public void setCamera(PerspectiveCamera camera) {
		this.camera = camera;
	}

	public void hide() {
		// apparently we need to set the active texture back to 0, otherwise scene2d (or SpriteBatch) is confused and fucks up our textures, thanks for not documenting shit, libgdx
		Gdx.gl20.glActiveTexture(GL20.GL_TEXTURE0);
		Gdx.gl20.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}
}
