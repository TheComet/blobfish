package com.nerdinand.ouya.blobfish;

import tv.ouya.console.api.OuyaFacade;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.surfaceview.RatioResolutionStrategy;
import com.nerdinand.ouya.blobfish.util.PurchaseManager;

public class MainActivity extends AndroidApplication {

	private OuyaFacade ouyaFacade = OuyaFacade.getInstance();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// TODO uncomment this for purchases
//		if (ouyaFacade.isRunningOnOUYAHardware()) {
//			PurchaseManager.init(ouyaFacade, this);
//		}

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useGL20 = true;
		config.useAccelerometer = false;
		config.useCompass = false;
		config.resolutionStrategy = new RatioResolutionStrategy(16, 9);

		initialize(new Blobfish(), config);
	}

	@Override
	protected void onDestroy() {
		ouyaFacade.shutdown();
		super.onDestroy();
	}
}