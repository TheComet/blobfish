package com.nerdinand.ouya.blobfish;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class TextureManager {
	static int nextID = 3;

	private int texID;

	Array<TextureRegion> textureRegions = new Array<TextureRegion>();

	public TextureManager() {
		this(nextID++);
	}
	
	public TextureManager(int texID){
		this.texID = texID;
	}

	void addTextureRegion(TextureRegion textureRegion) {
		textureRegions.add(textureRegion);
	}

	void bindTextures() {
		Gdx.gl20.glActiveTexture(GL20.GL_TEXTURE0+texID);
		textureRegions.first().getTexture().bind();
	}

	void setTextureCoordinates(ShaderProgram shader) {
		for (int i = 0; i < textureRegions.size; i++) {
			setTextureCoordinates(shader, i);
		}
	}

	void setTextureCoordinates(ShaderProgram shader, int index) {
		shader.setUniformi("textureSampler", texID);
		TextureRegion textureRegion = textureRegions.get(index);

		Vector2 tr00 = new Vector2(textureRegion.getU(), textureRegion.getV());
		Vector2 tr11 = new Vector2(textureRegion.getU2(), textureRegion.getV2());

		shader.setUniformf("tr" + index + "00", tr00);
		shader.setUniformf("tr" + index + "11", tr11);
	}
}
