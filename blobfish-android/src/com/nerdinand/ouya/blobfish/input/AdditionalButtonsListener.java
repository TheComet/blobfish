package com.nerdinand.ouya.blobfish.input;

import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;

public interface AdditionalButtonsListener extends InputManagerListener {
	enum AdditionalButtonType{
		R1, R2, R3, 
		L1, L2, L3
	}
	
	void additionalButtonDown(LocalPlayer player, AdditionalButtonType buttonType);
	void additionalButtonUp(LocalPlayer player, AdditionalButtonType buttonType);
}
