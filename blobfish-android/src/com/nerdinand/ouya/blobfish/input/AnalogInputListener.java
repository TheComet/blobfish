package com.nerdinand.ouya.blobfish.input;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.math.Vector2;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;

public interface AnalogInputListener extends InputManagerListener {
	enum ControllerAxis {
		AXIS_LEFT,
		AXIS_RIGHT,
		AXIS_LEFT_TRIGGER,
		AXIS_RIGHT_TRIGGER
	}
	
	void controllerAxisMoved(LocalPlayer player, Controller controller, ControllerAxis axis, Vector2 value);
}
