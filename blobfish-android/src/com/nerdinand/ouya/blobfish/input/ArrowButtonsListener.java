package com.nerdinand.ouya.blobfish.input;

import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;

public interface ArrowButtonsListener extends InputManagerListener {
	enum ArrowButtonDirection {
		UP,
		DOWN,
		LEFT,
		RIGHT
	}
	
	void arrowButtonDown(LocalPlayer player, ArrowButtonDirection direction);
	void arrowButtonUp(LocalPlayer player, ArrowButtonDirection direction);
}
