package com.nerdinand.ouya.blobfish.input;

import com.badlogic.gdx.controllers.Controller;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;

public interface ControllerConnectionListener extends InputManagerListener {
	public void controllerConnected(Controller controller, LocalPlayer player);
	public void controllerDisconnected(Controller controller, LocalPlayer player);
}
