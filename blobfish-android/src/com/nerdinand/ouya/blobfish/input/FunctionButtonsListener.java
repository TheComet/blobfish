package com.nerdinand.ouya.blobfish.input;

import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;

public interface FunctionButtonsListener extends InputManagerListener {
	/*
	 OUYA Color Default Function 
	 O green select 
	 U blue options (Android's menu function) 
	 Y yellow - 
	 A red back/cancel
	 */
	
	enum FunctionButtonType {
		SELECT, // O
		OPTIONS, // U
		ADDITIONAL, // Y
		CANCEL, // A
		HOME
	}
	
	public void functionButtonDown(LocalPlayer player, FunctionButtonType functionType);
	public void functionButtonUp(LocalPlayer player, FunctionButtonType functionType);
}
