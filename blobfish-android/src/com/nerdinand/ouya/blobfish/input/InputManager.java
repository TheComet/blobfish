package com.nerdinand.ouya.blobfish.input;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.controllers.mappings.Ouya;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.input.AdditionalButtonsListener.AdditionalButtonType;
import com.nerdinand.ouya.blobfish.input.AnalogInputListener.ControllerAxis;
import com.nerdinand.ouya.blobfish.input.ArrowButtonsListener.ArrowButtonDirection;
import com.nerdinand.ouya.blobfish.input.FunctionButtonsListener.FunctionButtonType;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;

@SuppressLint("UseSparseArrays")
public class InputManager implements ControllerListener, InputProcessor {
	private static final String TAG = "InputManager";

	private static final double AXIS_ARROW_KEY_THRESHOLD = 0.5;

	enum Platform {
		Desktop, Android, OUYA
	}

	public static final Map<Integer, ArrowButtonDirection> keyBoardDirectionMap;
	public static final Map<Integer, FunctionButtonType> keyBoardFunctionTypeMap;
	public static final Map<Integer, AdditionalButtonType> keyBoardButtonTypeMap;
	private static final int OUYA_BUTTON_HOME = 82;

	private final Platform platform;

	private Array<FunctionButtonsListener> functionButtonsListeners = new Array<FunctionButtonsListener>();
	private Array<AnalogInputListener> analogInputListeners = new Array<AnalogInputListener>();
	private Array<ControllerConnectionListener> controllerConnectionListeners = new Array<ControllerConnectionListener>();
	private Array<ArrowButtonsListener> arrowButtonsListeners = new Array<ArrowButtonsListener>();
	private Array<AdditionalButtonsListener> additionalButtonsListeners = new Array<AdditionalButtonsListener>();

	private HashMap<Controller, LocalPlayer> controllerPlayerMap = new HashMap<Controller, LocalPlayer>();
	private HashMap<Controller, ArrowButtonDirection> controllerArrowButtonDirectionMap = new HashMap<Controller, ArrowButtonDirection>();

	public Vector2 arrowDirection = new Vector2();

	static {
		keyBoardDirectionMap = new HashMap<Integer, ArrowButtonDirection>();
		keyBoardDirectionMap.put(Keys.UP, ArrowButtonDirection.UP);
		keyBoardDirectionMap.put(Keys.DOWN, ArrowButtonDirection.DOWN);
		keyBoardDirectionMap.put(Keys.LEFT, ArrowButtonDirection.LEFT);
		keyBoardDirectionMap.put(Keys.RIGHT, ArrowButtonDirection.RIGHT);

		keyBoardFunctionTypeMap = new HashMap<Integer, FunctionButtonType>();
		keyBoardFunctionTypeMap.put(Keys.ESCAPE, FunctionButtonType.HOME);
		keyBoardFunctionTypeMap.put(Keys.BACKSPACE, FunctionButtonType.CANCEL);

		keyBoardFunctionTypeMap.put(Keys.ENTER, FunctionButtonType.SELECT);
		keyBoardFunctionTypeMap.put(Keys.SPACE, FunctionButtonType.SELECT);

		keyBoardFunctionTypeMap.put(Keys.Y, FunctionButtonType.ADDITIONAL);

		keyBoardFunctionTypeMap.put(Keys.X, FunctionButtonType.OPTIONS);

		keyBoardButtonTypeMap = new HashMap<Integer, AdditionalButtonType>();
		keyBoardButtonTypeMap.put(Keys.SHIFT_LEFT, AdditionalButtonType.L1);
		keyBoardButtonTypeMap.put(Keys.SHIFT_RIGHT, AdditionalButtonType.R1);
		keyBoardButtonTypeMap.put(Keys.CONTROL_LEFT, AdditionalButtonType.L2);
		keyBoardButtonTypeMap.put(Keys.CONTROL_RIGHT, AdditionalButtonType.R2); // Macbook
																				// Pro
																				// doesn't
																				// have
																				// control
																				// right
		keyBoardButtonTypeMap.put(Keys.ALT_LEFT, AdditionalButtonType.L3);
		keyBoardButtonTypeMap.put(Keys.ALT_RIGHT, AdditionalButtonType.R3);
	}

	public InputManager() {
		ApplicationType type = Gdx.app.getType();

		if (type == ApplicationType.Android) {
			if (Ouya.runningOnOuya) {
				this.platform = Platform.OUYA;
				Controllers.addListener(this);

				for (Controller controller : Controllers.getControllers()) {
					addController(controller);
				}

			} else {
				this.platform = Platform.Android;
				// TODO generic Android behaviour
			}
		} else if (type == ApplicationType.Desktop) {
			this.platform = Platform.Desktop;
			Gdx.input.setInputProcessor(this);
			LocalPlayer keyboardPlayer = new LocalPlayer();
			controllerPlayerMap.put(null, keyboardPlayer);
			this.addListener(keyboardPlayer);

		} else {
			throw new RuntimeException("Unsupported platform: " + type.name());
		}
	}

	private LocalPlayer addController(Controller controller) {
		LocalPlayer player = new LocalPlayer();

		this.addListener(player);

		controllerPlayerMap.put(controller, player);
		controllerArrowButtonDirectionMap.put(controller, null);

		return player;
	}

	public void addListener(InputManagerListener listener) {
		Gdx.app.log(TAG, "Adding listener: " + listener);

		if (listener instanceof FunctionButtonsListener) {
			FunctionButtonsListener functionButtonsListener = (FunctionButtonsListener) listener;
			if (functionButtonsListeners.contains(functionButtonsListener, true)) {
				throw new IllegalArgumentException("listener can only be added once");
			}

			functionButtonsListeners.add(functionButtonsListener);
		}

		if (listener instanceof AnalogInputListener) {
			AnalogInputListener analogInputListener = (AnalogInputListener) listener;

			if (analogInputListeners.contains(analogInputListener, true)) {
				throw new IllegalArgumentException("listener can only be added once");
			}

			analogInputListeners.add(analogInputListener);
		}

		if (listener instanceof ControllerConnectionListener) {
			ControllerConnectionListener controllerConnectionListener = (ControllerConnectionListener) listener;

			if (controllerConnectionListeners.contains(controllerConnectionListener, true)) {
				throw new IllegalArgumentException("listener can only be added once");
			}

			controllerConnectionListeners.add(controllerConnectionListener);
		}

		if (listener instanceof ArrowButtonsListener) {
			ArrowButtonsListener arrowButtonsListener = (ArrowButtonsListener) listener;

			if (arrowButtonsListeners.contains(arrowButtonsListener, true)) {
				throw new IllegalArgumentException("listener can only be added once");
			}

			arrowButtonsListeners.add(arrowButtonsListener);
		}

		if (listener instanceof AdditionalButtonsListener) {
			AdditionalButtonsListener additionalButtonsListener = (AdditionalButtonsListener) listener;

			if (additionalButtonsListeners.contains(additionalButtonsListener, true)) {
				throw new IllegalArgumentException("listener can only be added once");
			}

			additionalButtonsListeners.add(additionalButtonsListener);
		}
	}

	public void removeListener(InputManagerListener listener) {
		Gdx.app.log(TAG, "Removing listener: " + listener);

		if (listener instanceof FunctionButtonsListener) {
			functionButtonsListeners.removeValue((FunctionButtonsListener) listener, true);
		}

		if (listener instanceof AnalogInputListener) {
			analogInputListeners.removeValue((AnalogInputListener) listener, true);
		}

		if (listener instanceof ControllerConnectionListener) {
			controllerConnectionListeners.removeValue((ControllerConnectionListener) listener, true);
		}

		if (listener instanceof ArrowButtonsListener) {
			arrowButtonsListeners.removeValue((ArrowButtonsListener) listener, true);
		}

		if (listener instanceof AdditionalButtonsListener) {
			additionalButtonsListeners.removeValue((AdditionalButtonsListener) listener, true);
		}
	}

	/**
	 * OUYA
	 */

	@Override
	public boolean buttonDown(Controller controller, int keyCode) {
		LocalPlayer player = createOrGetPlayer(controller);

		Gdx.app.debug(TAG, "buttonDown: " + player);

		ArrowButtonDirection direction;
		FunctionButtonType functionType;
		AdditionalButtonType buttonType;

		if ((direction = getOuyaArrowKey(keyCode)) != null) {
			for (ArrowButtonsListener listener : arrowButtonsListeners) {
				listener.arrowButtonDown(player, direction);
			}

			return true;

		} else if ((functionType = getOuyaFunctionKey(keyCode)) != null) {
			for (FunctionButtonsListener listener : functionButtonsListeners) {
				listener.functionButtonDown(player, functionType);
			}

			return true;

		} else if ((buttonType = getOuyaAdditionalKey(keyCode)) != null) {
			for (AdditionalButtonsListener listener : additionalButtonsListeners) {
				listener.additionalButtonDown(player, buttonType);
			}

			return true;

		} else {
			return false;
		}

	}

	/**
	 * OUYA
	 */

	@Override
	public boolean buttonUp(Controller controller, int keyCode) {
		LocalPlayer player = createOrGetPlayer(controller);

		ArrowButtonDirection direction;
		FunctionButtonType functionType;
		AdditionalButtonType buttonType;

		if ((direction = getOuyaArrowKey(keyCode)) != null) {
			for (ArrowButtonsListener listener : arrowButtonsListeners) {
				listener.arrowButtonUp(player, direction);
			}

			return true;

		} else if ((functionType = getOuyaFunctionKey(keyCode)) != null) {
			for (FunctionButtonsListener listener : functionButtonsListeners) {
				listener.functionButtonUp(player, functionType);
			}

			return true;

		} else if ((buttonType = getOuyaAdditionalKey(keyCode)) != null) {
			for (AdditionalButtonsListener listener : additionalButtonsListeners) {
				listener.additionalButtonUp(player, buttonType);
			}

			return true;

		} else {
			return false;
		}
	}

	private LocalPlayer createOrGetPlayer(Controller controller) {
		LocalPlayer player;

		if (controllerRegistered(controller)) {
			player = controllerPlayerMap.get(controller);
		} else {
			player = addController(controller);

			Gdx.app.log(TAG, "new Controller: " + controller.getName() + " new Player: " + player);
		}

		return player;
	}

	/**
	 * OUYA
	 */

	@Override
	public void connected(Controller controller) {
		LocalPlayer player;

		player = createOrGetPlayer(controller);

		Gdx.app.log(TAG, "Controller connected: " + controller);

		notifyControllerConnected(controller, player);
	}

	private void notifyControllerConnected(Controller controller, LocalPlayer player) {
		for (ControllerConnectionListener listener : controllerConnectionListeners) {
			listener.controllerConnected(controller, player);
		}
	}

	/**
	 * OUYA
	 */

	@Override
	public void disconnected(Controller controller) {
		LocalPlayer player = controllerPlayerMap.remove(controller);
		controllerArrowButtonDirectionMap.remove(controller);

		this.removeListener(player);

		Gdx.app.log(TAG, "Controller disconnected: " + controller);

		for (ControllerConnectionListener listener : controllerConnectionListeners) {
			listener.controllerDisconnected(controller, player);
		}
	}

	private boolean controllerRegistered(Controller newController) {
		return controllerPlayerMap.containsKey(newController);
	}

	@Override
	public boolean povMoved(Controller controller, int povCode, PovDirection value) {
		createOrGetPlayer(controller);
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
		createOrGetPlayer(controller);

		throw new RuntimeException("not implemented yet");
	}

	@Override
	public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
		createOrGetPlayer(controller);

		throw new RuntimeException("not implemented yet");
	}

	@Override
	public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
		createOrGetPlayer(controller);

		throw new RuntimeException("not implemented yet");
	}

	/**
	 * OUYA
	 */

	@Override
	public boolean axisMoved(Controller controller, int axisCode, float value) {
		LocalPlayer player = createOrGetPlayer(controller);

		ControllerAxis axis = getOuyaAxis(axisCode);
		Vector2 vector = new Vector2();

		if (axis != null) {

			switch (axis) {
			case AXIS_LEFT_TRIGGER:
				vector.set(value, value);
				break;

			case AXIS_RIGHT_TRIGGER:
				vector.set(value, value);
				break;

			case AXIS_LEFT:
				vector.set(controller.getAxis(Ouya.AXIS_LEFT_X), controller.getAxis(Ouya.AXIS_LEFT_Y));
				break;

			case AXIS_RIGHT:
				vector.set(controller.getAxis(Ouya.AXIS_RIGHT_X), controller.getAxis(Ouya.AXIS_RIGHT_Y));
				break;

			default:
				break;
			}

			for (AnalogInputListener listener : analogInputListeners) {
				listener.controllerAxisMoved(player, controller, axis, vector);
			}

			if (arrowButtonsListeners.size > 0 && axis == ControllerAxis.AXIS_LEFT) {
				ArrowButtonDirection oldDirection = controllerArrowButtonDirectionMap.get(controller);

				if (vector.len() > AXIS_ARROW_KEY_THRESHOLD) {

					ArrowButtonDirection direction = getDirection(vector);

					if (direction != oldDirection) {
						controllerArrowButtonDirectionMap.put(controller, direction);
						for (ArrowButtonsListener listener : arrowButtonsListeners) {
							listener.arrowButtonDown(player, direction);
						}
					}

				} else {
					if (oldDirection != null) {
						controllerArrowButtonDirectionMap.put(controller, null);
						for (ArrowButtonsListener listener : arrowButtonsListeners) {
							listener.arrowButtonUp(player, oldDirection);
						}
					}
				}
			}
		}

		return true;
	}

	private ArrowButtonDirection getDirection(Vector2 vector) {
		boolean vertical = Math.abs(vector.y) > Math.abs(vector.x);
		boolean positive = vertical ? (vector.y > 0) : (vector.x > 0);

		if (vertical) {
			if (positive) {
				return ArrowButtonDirection.DOWN;
			} else {
				return ArrowButtonDirection.UP;
			}
		} else {
			if (positive) {
				return ArrowButtonDirection.RIGHT;
			} else {
				return ArrowButtonDirection.LEFT;
			}
		}
	}

	private ControllerAxis getOuyaAxis(int axisCode) {
		ControllerAxis controllerAxis = null;

		if (axisCode == Ouya.AXIS_LEFT_TRIGGER) {
			controllerAxis = ControllerAxis.AXIS_LEFT_TRIGGER;
		} else if (axisCode == Ouya.AXIS_RIGHT_TRIGGER) {
			controllerAxis = ControllerAxis.AXIS_RIGHT_TRIGGER;
		} else if (axisCode == Ouya.AXIS_LEFT_X) {
			controllerAxis = ControllerAxis.AXIS_LEFT;
		} else if (axisCode == Ouya.AXIS_LEFT_Y) {
			controllerAxis = ControllerAxis.AXIS_LEFT;
		} else if (axisCode == Ouya.AXIS_RIGHT_X) {
			controllerAxis = ControllerAxis.AXIS_RIGHT;
		} else if (axisCode == Ouya.AXIS_RIGHT_Y) {
			controllerAxis = ControllerAxis.AXIS_RIGHT;
		}

		return controllerAxis;
	}

	private ArrowButtonDirection getKeyboardArrowKey(int keyCode) {
		ArrowButtonDirection direction = null;

		direction = keyBoardDirectionMap.get(keyCode);

		return direction;
	}

	private FunctionButtonType getKeyboardFunctionKey(int keyCode) {
		FunctionButtonType functionType = null;

		functionType = keyBoardFunctionTypeMap.get(keyCode);

		return functionType;
	}

	private AdditionalButtonType getKeyboardAdditionalKey(int keyCode) {
		AdditionalButtonType buttonType = null;

		buttonType = keyBoardButtonTypeMap.get(keyCode);

		return buttonType;
	}

	private ArrowButtonDirection getOuyaArrowKey(int keyCode) {
		ArrowButtonDirection direction = null;

		if (keyCode == Ouya.BUTTON_DPAD_UP) {
			direction = ArrowButtonDirection.UP;
		} else if (keyCode == Ouya.BUTTON_DPAD_DOWN) {
			direction = ArrowButtonDirection.DOWN;
		} else if (keyCode == Ouya.BUTTON_DPAD_LEFT) {
			direction = ArrowButtonDirection.LEFT;
		} else if (keyCode == Ouya.BUTTON_DPAD_RIGHT) {
			direction = ArrowButtonDirection.RIGHT;
		}

		return direction;
	}

	private FunctionButtonType getOuyaFunctionKey(int keyCode) {
		FunctionButtonType functionType = null;

		if (keyCode == Ouya.BUTTON_O) {
			functionType = FunctionButtonType.SELECT;
		} else if (keyCode == Ouya.BUTTON_U) {
			functionType = FunctionButtonType.OPTIONS;
		} else if (keyCode == Ouya.BUTTON_Y) {
			functionType = FunctionButtonType.ADDITIONAL;
		} else if (keyCode == Ouya.BUTTON_A) {
			functionType = FunctionButtonType.CANCEL;
		} else if (keyCode == OUYA_BUTTON_HOME) {
			functionType = FunctionButtonType.HOME;
		}

		return functionType;
	}

	private AdditionalButtonType getOuyaAdditionalKey(int keyCode) {
		AdditionalButtonType buttonType = null;

		if (keyCode == Ouya.BUTTON_L1) {
			buttonType = AdditionalButtonType.L1;
		} else if (keyCode == Ouya.BUTTON_L2) {
			buttonType = AdditionalButtonType.L2;
		} else if (keyCode == Ouya.BUTTON_L3) {
			buttonType = AdditionalButtonType.L3;
		} else if (keyCode == Ouya.BUTTON_R1) {
			buttonType = AdditionalButtonType.R1;
		} else if (keyCode == Ouya.BUTTON_R2) {
			buttonType = AdditionalButtonType.R2;
		} else if (keyCode == Ouya.BUTTON_R3) {
			buttonType = AdditionalButtonType.R3;
		}

		return buttonType;
	}

	/**
	 * keyboard
	 */

	@Override
	public boolean keyDown(int keyCode) {
		ArrowButtonDirection direction;
		FunctionButtonType functionType;
		AdditionalButtonType buttonType;

		if ((functionType = getKeyboardFunctionKey(keyCode)) != null) {
			for (FunctionButtonsListener listener : functionButtonsListeners) {
				listener.functionButtonDown(getKeyboardPlayer(), functionType);
			}

			return true;

		} else if ((direction = getKeyboardArrowKey(keyCode)) != null) {
			Vector2 vector = directionToVector(direction);

			arrowDirection.add(vector);

			for (ArrowButtonsListener listener : arrowButtonsListeners) {
				listener.arrowButtonDown(getKeyboardPlayer(), direction);
			}

			for (AnalogInputListener listener : analogInputListeners) {

				listener.controllerAxisMoved(getKeyboardPlayer(), null, ControllerAxis.AXIS_LEFT, arrowDirection);
			}

			return true;

		} else if ((buttonType = getKeyboardAdditionalKey(keyCode)) != null) {
			for (AdditionalButtonsListener listener : additionalButtonsListeners) {
				listener.additionalButtonDown(getKeyboardPlayer(), buttonType);
			}

			return true;

		} else {
			return false;
		}
	}

	private LocalPlayer getKeyboardPlayer() {
		return controllerPlayerMap.get(null);
	}

	private Vector2 directionToVector(ArrowButtonDirection direction) {
		switch (direction) {
		case UP:
			return new Vector2(0, -1);

		case DOWN:
			return new Vector2(0, 1);

		case LEFT:
			return new Vector2(-1, 0);

		case RIGHT:
			return new Vector2(1, 0);
		}

		return null;
	}

	/**
	 * keyboard
	 */

	@Override
	public boolean keyUp(int keyCode) {
		ArrowButtonDirection direction;
		FunctionButtonType functionType;
		AdditionalButtonType buttonType;

		if ((functionType = getKeyboardFunctionKey(keyCode)) != null) {
			for (FunctionButtonsListener listener : functionButtonsListeners) {
				listener.functionButtonUp(getKeyboardPlayer(), functionType);
			}

			return true;

		} else if ((direction = getKeyboardArrowKey(keyCode)) != null) {
			Vector2 vector = directionToVector(direction);

			arrowDirection.sub(vector);

			for (ArrowButtonsListener listener : arrowButtonsListeners) {
				listener.arrowButtonUp(getKeyboardPlayer(), direction);
			}

			for (AnalogInputListener listener : analogInputListeners) {

				listener.controllerAxisMoved(getKeyboardPlayer(), null, ControllerAxis.AXIS_LEFT, arrowDirection);
			}

			return true;

		} else if ((buttonType = getKeyboardAdditionalKey(keyCode)) != null) {
			for (AdditionalButtonsListener listener : additionalButtonsListeners) {
				listener.additionalButtonUp(getKeyboardPlayer(), buttonType);
			}

			return true;

		} else {
			return false;
		}
	}

	/**
	 * keyboard
	 */

	@Override
	public boolean keyTyped(char arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	public Platform getPlatform() {
		return platform;
	}

	public Collection<LocalPlayer> getLocalPlayerList() {
		ArrayList<LocalPlayer> localPlayerList = new ArrayList<LocalPlayer>(controllerPlayerMap.values());
		// localPlayerList.add(0, new LocalPlayer()); // to test split screen
		return localPlayerList;
	}
}
