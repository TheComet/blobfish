package com.nerdinand.ouya.blobfish.libgdx_extension;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.input.ArrowButtonsListener;
import com.nerdinand.ouya.blobfish.input.FunctionButtonsListener;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;

public class InputManagerStage extends Stage implements ArrowButtonsListener, FunctionButtonsListener {

	private static final String TAG = "InputManagerStage";
	private Actor selectedActor;

	private Array<Actor> unrolledActors = new Array<Actor>();
	private SelectionListener selectionListener;

	public InputManagerStage() {
		super();
	}

	public InputManagerStage(float width, float height, boolean keepAspectRatio) {
		super(width, height, keepAspectRatio);
	}

	public void selectActor(Actor selectedActor) {
		Gdx.app.debug(TAG, "setSelectedActor: " + selectedActor);

		if (this.selectedActor != null) {
			this.selectedActor.setColor(Color.WHITE);
		}

		if (this.selectedActor != selectedActor) {
			// selection actually changed

			if (selectionListener != null) {
				selectionListener.selectionChanged(this.selectedActor, selectedActor);
			}

			this.selectedActor = selectedActor;
			this.selectedActor.setColor(Color.RED);
		}
	}

	public Actor getSelectedActor() {
		return selectedActor;
	}

	public void setSelectionListener(SelectionListener listener) {
		this.selectionListener = listener;
	}

	@Override
	public void functionButtonDown(LocalPlayer player, FunctionButtonType functionType) {
		if (functionType == FunctionButtonType.SELECT) {
			Actor actor = getSelectedActor();

			if (actor != null) {
				sendClick(actor);
			}
		}
	}

	private void sendClick(Actor actor) {
		InputEvent inputEvent = new InputEvent();

		inputEvent.setType(InputEvent.Type.touchDown);
		actor.fire(inputEvent);
	}

	@Override
	public void functionButtonUp(LocalPlayer player, FunctionButtonType functionType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void arrowButtonDown(LocalPlayer player, ArrowButtonDirection direction) {
		int index = unrolledActors.indexOf(selectedActor, true);

		Actor nextActor = null;

		switch (direction) {
		case UP:
			nextActor = getActor(index - 1);
			break;

		case DOWN:
			nextActor = getActor(index + 1);
			break;

		case LEFT:
			if (selectedActor instanceof Slider) {
				changeSliderValue((Slider) selectedActor, -((Slider) selectedActor).getStepSize());
			}

			break;
		case RIGHT:
			if (selectedActor instanceof Slider) {
				changeSliderValue((Slider) selectedActor, +((Slider) selectedActor).getStepSize());
			}

			break;
		}

		if (nextActor != null) {
			selectActor(nextActor);
		}
	}

	private void changeSliderValue(Slider slider, float amount) {
		final float oldValue = slider.getValue();
		float newValue = oldValue + amount;
		
		if (newValue > slider.getMaxValue() || newValue < slider.getMinValue()){
			newValue = oldValue;
		}
		
		slider.setValue(newValue);
	}

	private Actor getActor(int index) {
		if (index < 0 || index >= unrolledActors.size) {
			return null;
		} else {
			return unrolledActors.get(index);
		}
	}

	@Override
	public void addActor(Actor actor) {
		super.addActor(actor);

		if (actor instanceof Group) {
			addElements((Group) actor);
		}
	}

	private void addElements(Actor actor) {
		if (checkSelectableElement(actor)) {
			Gdx.app.log(TAG, "adding Element: " + actor);
			unrolledActors.add(actor);
		}

		if (actor instanceof Group) {
			for (Actor subActor : ((Group) actor).getChildren()) {
				addElements(subActor);
			}
		}
	}

	private boolean checkSelectableElement(Actor actor) {
		return (actor instanceof Button || actor instanceof CheckBox || actor instanceof Slider);
	}

	@Override
	public void arrowButtonUp(LocalPlayer player, ArrowButtonDirection direction) {
	}

	public interface SelectionListener {
		public void selectionChanged(Actor oldSelection, Actor newSelection);
	}
}
