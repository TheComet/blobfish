package com.nerdinand.ouya.blobfish.map;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.Assets;
import com.nerdinand.ouya.blobfish.map.formats.JSONMap;
import com.nerdinand.ouya.blobfish.map.tile.ColourTile;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.Tile.TileType;

public abstract class Map {
	private static final double DEFAULT_POWERUP_SPAWN_FACTOR = 0.06;

	private int version;
	private TileField tileField;

	private int playerCount;
	private MiniMap miniMap;
	private Float powerUpsPerMinute = null;
	private String path;

	public Map(String path) {
		this.path = path;
	}

	public Map() {
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}

	public void setPowerUpsPerMinute(float powerUpsPerMinute) {
		this.powerUpsPerMinute = powerUpsPerMinute;
	}

	public float getPowerUpsPerMinute() {
		if (powerUpsPerMinute == null) {
			this.powerUpsPerMinute = calculatePowerUpsPerMinute();
		}
		return powerUpsPerMinute;
	}

	private Float calculatePowerUpsPerMinute() {
		int numColourTiles = 0;

		for (Array<Tile> tileArray : getTileField().getTiles()) {
			for (Tile tile : tileArray) {
				if (tile.getTileType() == TileType.COLOUR_TILE && ((ColourTile) tile).canSpawnPowerUp()) {
					numColourTiles++;
				}
			}
		}

		float powerUpCount = (float) (numColourTiles * DEFAULT_POWERUP_SPAWN_FACTOR);

		return powerUpCount;
	}

	public void setPlayerCount(int playerCount) {
		this.playerCount = playerCount;
	}

	public int getPlayerCount() {
		return playerCount;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getVersion() {
		return version;
	}

	public void setTileField(TileField tileField) {
		this.tileField = tileField;
	}

	public TileField getTileField() {
		return tileField;
	}

	public Pixmap getPreview() {
		FileHandle previewPath = Gdx.files.internal(getPath() + ".png");

		if (previewPath.exists()) {
			return new Pixmap(previewPath);
		} else {
			if (this instanceof JSONMap) {
				return ((JSONMap) this).getMiniMap().getPixmap();
			} else {
				return Assets.assetManager.get(Assets.PIXMAP_MAP_PREVIEW_PLACEHOLDER, Pixmap.class);
			}
		}
	}

	public abstract Map loadMap() throws IOException, MapFormatException;

	public abstract void deserialize() throws MapFormatException;

}