package com.nerdinand.ouya.blobfish.map;

public class MapFormatException extends Exception {

	public MapFormatException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4052992969904816825L;

}
