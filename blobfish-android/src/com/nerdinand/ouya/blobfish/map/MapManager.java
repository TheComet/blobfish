package com.nerdinand.ouya.blobfish.map;

import java.io.IOException;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.formats.JSONMap;

public class MapManager {

	private static Array<MapPack> mapPackList = new Array<MapPack>();
	
	static {
		MapPack pack;
		String base;
		
		try {
			// TODO set proper ids to be able to sell map packs
			pack = new MapPack("new maps", "TODO: SET ID!");
			base = "maps/new maps/";
			
			pack.add((JSONMap) (new JSONMap(base+"test.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Arena.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Tribute to RoD.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Kingdom of Eight.r34")).loadMap());
			
			mapPackList.add(pack);
			
			
			
			pack = new MapPack("pwnies", "TODO: SET ID!");
			base = "maps/pwnies/";
			
			pack.add((JSONMap) (new JSONMap(base+"apples.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"dresses.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"magic.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"party.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"stayoutofmyshed.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"swag.r34")).loadMap());
			
			mapPackList.add(pack);
			
			
			pack = new MapPack("classic maps I", "TODO: SET ID!");
			base = "maps/classic maps I/";

			pack.add((JSONMap) (new JSONMap(base+"Adam.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Diamond.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Dungeon.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Goomba.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Isles.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Kingdom of Four.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Le Pain.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Light Speed.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Lost Ruins.r34")).loadMap());
			
			mapPackList.add(pack);
			
			
			pack = new MapPack("classic maps II", "TODO: SET ID!");
			base = "maps/classic maps II/";
			
			pack.add((JSONMap) (new JSONMap(base+"Plains.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Random.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"reto.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Round and Round.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"skywalk.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"snake.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Split Up.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"Tile Hopping.r34")).loadMap());
			pack.add((JSONMap) (new JSONMap(base+"TURD.r34")).loadMap());
			
			mapPackList.add(pack);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MapFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Array<MapPack> getMapPackList() {
		return mapPackList;
	}
}
