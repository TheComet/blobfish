package com.nerdinand.ouya.blobfish.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.Assets;
import com.nerdinand.ouya.blobfish.map.formats.JSONMap;

public class MapPack {
	private String name;
	private String id;
	
	private Array<JSONMap> maps = new Array<JSONMap>();
	
	public MapPack(String name, String id) {
		super();
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public TextureRegion getPreview() {
		FileHandle previewPath = Gdx.files.internal(getName() + ".png");

		if (previewPath.exists()) {
			return new TextureRegion(new Texture(previewPath));
		} else {
			return Assets.getRegion(Assets.TEXTURE_ATLAS_UI, Assets.PIXMAP_MAP_PREVIEW_PLACEHOLDER);
		}
	}
	
	public void add(JSONMap map){
		maps.add(map);
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public Array<JSONMap> getMaps() {
		return maps;
	}
}
