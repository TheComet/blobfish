package com.nerdinand.ouya.blobfish.map;

import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.formats.JSONMap;
import com.nerdinand.ouya.blobfish.map.tile.TeleporterTile;
import com.nerdinand.ouya.blobfish.map.tile.Tile;

public class MiniMap {

	private static final int MINIMAP_TILE_WIDTH = 20;
	private static final int MINIMAP_TILE_HEIGHT = 20;

	private static final int CENTER_WIDTH = MINIMAP_TILE_WIDTH / 2;
	private static final int CENTER_HEIGHT = MINIMAP_TILE_HEIGHT / 2;
	private static final String TAG = "MiniMap";

	private JSONMap map;
	private Pixmap standardMap;

	public MiniMap(JSONMap map) {
		this.map = map;

		generatePixmap();
	}

	private void generatePixmap() {
		JSONMap jsonMap = getMap();
		
		int height = jsonMap.getHeight();
		int width = jsonMap.getWidth();

		standardMap = new Pixmap(width * MINIMAP_TILE_HEIGHT, height * MINIMAP_TILE_WIDTH, Pixmap.Format.RGB565);

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				char tileCharacter = jsonMap.map[j].charAt(i);

				final Pixmap icon = JSONMap.miniMapIconFor(tileCharacter);
				standardMap.drawPixmap(icon, i * MINIMAP_TILE_HEIGHT, j * MINIMAP_TILE_WIDTH);
			}
		}
	}

	public Pixmap getTeleporterMap() {
		Pixmap teleporterMap = getMiniMapCopy();
		teleporterMap.setColor(Color.RED);

		Array<TeleporterTile> teleporterTiles = getMap().getTileField().getTeleporterTiles();
		for (TeleporterTile teleporterTile : teleporterTiles) {
			Vector2 source = teleporterTile.getTileCoordinates();
			Vector2 destination = teleporterTile.getDestinationTile().getTileCoordinates();

			teleporterMap.drawLine(getTileX(source), getTileY(source), getTileX(destination), getTileY(destination));
		}

		return teleporterMap;
	}

	private int getTileY(Vector2 source) {
		return (int) source.y * MINIMAP_TILE_WIDTH + CENTER_WIDTH;
	}

	private int getTileX(Vector2 source) {
		return (int) source.x * MINIMAP_TILE_HEIGHT + CENTER_HEIGHT;
	}

	private Pixmap getMiniMapCopy() {
		Pixmap copy = new Pixmap(standardMap.getWidth(), standardMap.getHeight(), Pixmap.Format.RGB565);
		copy.drawPixmap(standardMap, 0, 0);

		return copy;
	}

	public JSONMap getMap() {
		return map;
	}
	
	public Pixmap getPixmap() {
		return standardMap;
	}

	public Pixmap getPathMap(List<Tile> path) {
		Pixmap pathMap = getMiniMapCopy();
		
		if (path != null) {
			pathMap.setColor(Color.GREEN);

			for (int i = 0; i < path.size() - 1; i++) {
				Tile tile = path.get(i);
				Tile tile2 = path.get(i + 1);
				Vector2 source = tile.getTileCoordinates();
				Vector2 destination = tile2.getTileCoordinates();

				pathMap.drawLine(getTileX(source), getTileY(source), getTileX(destination), getTileY(destination));
			}
		}

		return pathMap;
	}
}
