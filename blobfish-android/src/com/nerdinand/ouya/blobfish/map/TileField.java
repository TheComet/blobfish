package com.nerdinand.ouya.blobfish.map;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.tile.EmptyTile;
import com.nerdinand.ouya.blobfish.map.tile.TeleporterTile;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.Tile.TileType;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;
import com.nerdinand.ouya.blobfish.simulation.player.Player;

public class TileField {

	private static final String TAG = "TileField";
	private List<Tile> neighbourTileList;

	private Array<Array<Tile>> tiles;

	private Array<VisibleTile> spawnTiles = new Array<VisibleTile>();
	private Array<VisibleTile> powerUpTiles = new Array<VisibleTile>();

	private final int height;
	private final int width;

	private Array<TeleporterTile> teleporterTiles = new Array<TeleporterTile>();

	public TileField(Array<Array<Tile>> tiles) throws MapFormatException {
		this.tiles = tiles;

		width = tiles.size;
		height = tiles.first().size;

		initialize();
	}

	public List<Tile> killTiles(Tile currentTile) throws MapFormatException {
		this.neighbourTileList = getNeighbourTiles(currentTile, false);
		for (Tile neighbourTile : neighbourTileList) {
			if (!(neighbourTile.getTileType() == TileType.EMPTY_TILE || neighbourTile == null)) {
				Vector2 tileCord = neighbourTile.getTileCoordinates();
				if (!neighbourTile.isSpawnPoint()) {
					tiles.get((int) tileCord.x).set((int) tileCord.y, new EmptyTile(tileCord));
				}
			}
		}

		initialize();

		return neighbourTileList;
	}

	public void revive(List<Tile> killedTiles) {
		for (Tile tile : killedTiles) {
			Vector2 tileCord = tile.getTileCoordinates();
			tiles.get((int) tileCord.x).set((int) tileCord.y, tile);
		}
	}

	private void initialize() throws MapFormatException {
		spawnTiles.clear();
		teleporterTiles.clear();
		powerUpTiles.clear();

		for (Array<Tile> column : tiles) {
			for (Tile tile : column) {
				if (tile != null) {
					if (tile.isSpawnPoint()) {
						spawnTiles.add((VisibleTile) tile);
					}

					if (tile.getTileType() == TileType.TELEPORTER_TILE) {
						TeleporterTile teleporterTile = (TeleporterTile) tile;
						teleporterTiles.add(teleporterTile);

						Vector2 destination = teleporterTile.getDestination();

						if (destination == null) {
							throw new MapFormatException(teleporterTile.toString() + " has no destination tile!");
						}

						Tile destinationTile = getTile(destination);
						teleporterTile.setDestinationTile(destinationTile);
					}

					if (tile.canSpawnPowerUp()) {
						powerUpTiles.add((VisibleTile) tile);
					}
				}
			}
		}
	}

	private Tile getTile(Vector2 tileCoordinates) {
		return getTile(tileCoordinates.x, tileCoordinates.y);
	}

	public Tile getTile(int x, int y) {
		if (x < 0 || y < 0 || x >= width || y >= height) {
			return new EmptyTile(new Vector2(x, y));
		}
		return tiles.get(x).get(y);
	}

	public HashMap<TileType, Array<VisibleTile>> getVisibleTiles(LocalPlayer player) {
		HashMap<TileType, Array<VisibleTile>> tileMap = new HashMap<TileType, Array<VisibleTile>>();
		
		// TODO find out, why this can even be null here
		if (player.getPosition() != null) {
			
			Vector2 positionOnMap = worldCoordinatesToMap(player.getPosition());

			for (TileType type : TileType.values()) {
				tileMap.put(type, new Array<VisibleTile>());
			}

			float x = positionOnMap.x;
			float y = positionOnMap.y;

			float startX = x - player.getTilesCullingLeft();
			float endX = x + player.getTilesCullingRight();
			float startY = y - player.getTilesCullingTop();
			float endY = y + player.getTilesCullingBottom();

			// Gdx.app.debug(TAG, "from: " + startX + ", " + startY + " to " + endX
			// + ", " + endY);

			for (float i = startX; i < endX; i++) {
				for (float j = startY; j < endY; j++) {
					Tile tile = getTile(i, j);

					if (tile != null) {
						if (tile.doesRender()) {
							tileMap.get(tile.getTileType()).add((VisibleTile) tile);
						}
					}
				}
			}
		}

		return tileMap;
	}

	private Tile getTile(float x, float y) {
		return getTile(MathUtils.floorPositive(x + 0.5f), MathUtils.floorPositive(y + 0.5f));
	}

	public Array<Array<Tile>> getTiles() {
		return tiles;
	}

	public Tile getTileAt(Vector3 origPosition) {
		return getTile(worldCoordinatesToMap(origPosition));
	}

	public static Vector2 worldCoordinatesToMap(Vector3 vector3) {
		Vector3 position = vector3.cpy();
		position.div(Tile.X_DIMENSION, 1, Tile.Y_DIMENSION);

		// x=z; y=-x
		return new Vector2(position.z + 0.5f, -position.x + 0.5f);
		// return new Vector2(position.x, -position.z);
	}

	public static Vector3 mapCoordinatesToWorld(Vector2 vector2) {
		Vector2 position = vector2.cpy();
		position.x -= 0.5f;
		position.y -= 0.5f;
		position.mul(Tile.X_DIMENSION, Tile.Y_DIMENSION);

		// z=x; x=-y
		return new Vector3(-position.y, 0, position.x);
		// return new Vector3(position.x, 0, -position.y);
	}

	public Tile getSpawnTile() {
		return spawnTiles.random();
	}

	public VisibleTile getPowerUpTile() {
		return powerUpTiles.random();
	}

	public Array<TeleporterTile> getTeleporterTiles() {
		return teleporterTiles;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public int estimateDistance(Tile tile, Tile to) {
		// TODO implement euclidian distance or something like that, in case
		// stuff gets slow! With distance=0, A* will find the optimal solution
		// but
		// not in optimal time
		return 0;
	}

	public List<Tile> getAdjacentTiles(Tile currentTile) {
		List<Tile> adjacent = getNeighbourTiles(currentTile, true);

		if (currentTile.getTileType() == TileType.TELEPORTER_TILE) {
			adjacent.add(((TeleporterTile) currentTile).getDestinationTile());
		}

		Collections.shuffle(adjacent);

		return adjacent;
	}

	public List<Tile> getPossibleTilesforIntersection(Player player) {
		List<Tile> tileList = new ArrayList<Tile>(2);

		Vector3 posX = (player.getPosition().cpy().add((player.getVelocity().cpy()).nor().x * 10f, 0.0f, 0.0f));
		Vector3 posY = (player.getPosition().cpy().add(0.0f, 0.0f, (player.getVelocity().cpy()).nor().z * 10f));

		Tile tileX = getTileAt(posX);
		Tile tileY = getTileAt(posY);
		Tile tilePos = getTileAt(player.getPosition());
		
		if (tilePos != null) {
			tileList.add(tilePos);
		}
		if (tileX != null && tileX != tilePos && ((tileX.getTileType() == TileType.WALL_TILE && !(tilePos.getTileType() == TileType.WALL_TILE)) || (tileX.getTileType() == TileType.HIGH_WALL_TILE && !(tilePos.getTileType() == TileType.HIGH_WALL_TILE)))) {
			tileList.add(tileX);
		}
		if (tileY != null && tileY != tilePos && ((tileY.getTileType() == TileType.WALL_TILE && !(tilePos.getTileType() == TileType.WALL_TILE)) || (tileY.getTileType() == TileType.HIGH_WALL_TILE && !(tilePos.getTileType() == TileType.HIGH_WALL_TILE)))) {
			tileList.add(tileY);
		}
		return tileList;
	}

	/**
	 * Returns the neighbouring tiles of currentTile
	 * 
	 * @param currentTile
	 *            the tile to get the neighbours of
	 * @param hasToBeWalkable
	 *            whether the neighbour tiles have to be walkable
	 * @return a list of tiles which are neighbours of currentTile
	 */
	public List<Tile> getNeighbourTiles(Tile currentTile, boolean hasToBeWalkable) {
		List<Tile> adjacent = new ArrayList<Tile>(8);

		if (currentTile == null) {
			return adjacent;
		}

		Vector2 tileCoordinates = currentTile.getTileCoordinates();
		for (int x = (int) (tileCoordinates.x - 1); x <= tileCoordinates.x + 1; x++) {
			for (int y = (int) (tileCoordinates.y - 1); y <= tileCoordinates.y + 1; y++) {
				Tile tile = getTile(x, y);
				if (tile != null) {
					if (hasToBeWalkable) {
						if (tile.isWalkable()) {
							adjacent.add(tile);
						}
					} else if (!hasToBeWalkable) {
						adjacent.add(tile);
					}
				}
			}
		}

		adjacent.remove(currentTile);

		return adjacent;
	}

}
