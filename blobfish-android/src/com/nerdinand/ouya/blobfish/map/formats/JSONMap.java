package com.nerdinand.ouya.blobfish.map.formats;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.nerdinand.ouya.blobfish.Assets;
import com.nerdinand.ouya.blobfish.map.Map;
import com.nerdinand.ouya.blobfish.map.MapFormatException;
import com.nerdinand.ouya.blobfish.map.MiniMap;
import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.ColourTile;
import com.nerdinand.ouya.blobfish.map.tile.EmptyTile;
import com.nerdinand.ouya.blobfish.map.tile.HighWallTile;
import com.nerdinand.ouya.blobfish.map.tile.JumpTile;
import com.nerdinand.ouya.blobfish.map.tile.TeleporterTile;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.Tile.TileType;
import com.nerdinand.ouya.blobfish.map.tile.WallTile;

public class JSONMap extends Map {
	private static final String TAG = "JSONMap";

	String name;
	String creator;
	int playerCount;
	String date;
	String description;

	public String[] map;

	Teleporter[] teleporters;

	private MiniMap miniMap;

	public JSONMap() {
	}

	public JSONMap(String path) {
		super(path);
	}

	public String getCreator() {
		return creator;
	}
	
	public String getDate() {
		return date;
	}
	
	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public JSONMap(OldMap oldMap) {
		setTileField(oldMap.getTileField());

		setPowerUpsPerMinute(oldMap.getPowerUpsPerMinute());
		this.name = oldMap.getPath();
		this.description = "Converted from " + oldMap.getPath() + " to new map format.";

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		this.date = format.format(new Date());

		this.creator = "JSONMap class";
		this.playerCount = oldMap.getPlayerCount();
	}

	public MiniMap getMiniMap() {
		if (miniMap == null) {
			miniMap = new MiniMap(this);
		}
		return miniMap;
	}

	private static JSONMap loadMap(JSONMap map) throws MapFormatException {
		Json json = new Json();
		String path = map.getPath();
		map = json.fromJson(JSONMap.class, Gdx.files.internal(path));
		map.setPath(path);

		return map;
	}

	@Override
	public void deserialize() throws MapFormatException {
		int height = getHeight();
		int width = getWidth();

		Gdx.app.debug(TAG, "map dimensions: height: " + height + ", width: " + width);

		checkArrayDimensions(this.map, height, width);

		Array<Array<Tile>> tiles = new Array<Array<Tile>>(width);

		for (int x = 0; x < width; x++) {
			tiles.add(new Array<Tile>(height));
			for (int y = 0; y < height; y++) {
				Tile newTile = newTile(this.map[y].charAt(x), new Vector2(x, y));
				tiles.get(x).add(newTile);
			}
		}

		for (Teleporter teleporter : teleporters) {
			Tile tile = tiles.get(teleporter.source.x).get(teleporter.source.y);
			if (tile.getTileType() == TileType.TELEPORTER_TILE) {
				((TeleporterTile) tile).setDestination(new Vector2(teleporter.destination.x, teleporter.destination.y));
			}
		}

		TileField tileField = new TileField(tiles);
		this.setTileField(tileField);
	}

	public int getWidth() {
		return this.map[0].length();
	}

	public int getHeight() {
		return this.map.length;
	}

	private void checkArrayDimensions(String[] map2, int height, int width) throws MapFormatException {
		if (map2.length != height) {
			throw new MapFormatException("Map should be " + height + " high but is " + map2.length);
		}

		int i = 0;

		for (String string : map2) {
			if (string.length() != width) {
				throw new MapFormatException("Line " + i + " of map should be " + width + " wide but is " + string.length());
			}
			i++;
		}
	}

	public static Pixmap miniMapIconFor(char character) {
		final String iconName;

		switch (character) {
		case '#':
			iconName = Assets.PIXMAP_MINIMAP_HIGH_WALL;
			break;
			
		case ' ':
			iconName = Assets.PIXMAP_MINIMAP_EMPTY;
			break;

		case 'R':
			iconName = Assets.PIXMAP_MINIMAP_RESTRICT_POWERUP;
			break;

		case 'W':
			iconName = Assets.PIXMAP_MINIMAP_WALL;
			break;
			
		case 'T':
			iconName = Assets.PIXMAP_MINIMAP_TELEPORTER;
			break;
			
		case 'C':
			iconName = Assets.PIXMAP_MINIMAP_COLOURTILE;
			break;
			
		case '*':
			iconName = Assets.PIXMAP_MINIMAP_JUMP;
			break;
			
		case 'S':
			iconName = Assets.PIXMAP_MINIMAP_SPAWN;
			break;
			
		default:
			iconName = Assets.PIXMAP_MINIMAP_EMPTY;
			break;
		}
		
		return Assets.assetManager.get(iconName, Pixmap.class);
	}

	private Tile newTile(char character, Vector2 tileCoordinates) {
		switch (character) {
		case '#':
			return new HighWallTile(tileCoordinates);

		case ' ':
			return new EmptyTile(tileCoordinates);

		case 'R':
			return new ColourTile(tileCoordinates, false, false);

		case 'W':
			return new WallTile(tileCoordinates);

		case 'T':
			return new TeleporterTile(tileCoordinates);

		case 'C':
			return new ColourTile(tileCoordinates);

		case '*':
			return new JumpTile(tileCoordinates);

		case 'S':
			return new ColourTile(tileCoordinates, true);

		default:
			// TODO proper error handling

			break;
		}

		return new EmptyTile(tileCoordinates);
	}

	public void saveMap(String filename) {
		serialize();
		TileField field = getTileField();
		setTileField(null);
		Json json = new Json();
		json.toJson(this, Gdx.files.external(filename));
		setTileField(field);
	}

	private void serialize() {
		// TODO implement serialization (if necessary)

		map = new String[getTileField().getHeight()];

		Arrays.fill(map, "");

		for (Array<Tile> tileArray : getTileField().getTiles()) {
			int i = 0;

			for (Tile tile : tileArray) {
				map[i++] += getSerializationForTile(tile);
			}
		}

		Array<Teleporter> teleporters = new Array<Teleporter>();

		for (TeleporterTile teleporterTile : getTileField().getTeleporterTiles()) {
			Position source = new Position();
			Vector2 tileCoordinates = teleporterTile.getTileCoordinates();
			source.x = (int) tileCoordinates.x;
			source.y = (int) tileCoordinates.y;

			Position destination = new Position();
			tileCoordinates = teleporterTile.getDestination();
			destination.x = (int) tileCoordinates.x;
			destination.y = (int) tileCoordinates.y;

			Teleporter teleporter = new Teleporter();
			teleporter.source = source;
			teleporter.destination = destination;

			teleporters.add(teleporter);
		}

		this.teleporters = teleporters.toArray(Teleporter.class);
	}

	private char getSerializationForTile(Tile tile) {
		switch (tile.getTileType()) {
		case COLOUR_TILE:
			ColourTile colourTile = (ColourTile) tile;
			if (colourTile.isSpawnPoint()) {
				return 'S';
			} else {
				if (colourTile.canSpawnPowerUp()) {
					return 'C';
				} else {
					return 'R';
				}
			}

		case EMPTY_TILE:
			return ' ';

		case NULL_TILE:
			return ' ';

		case JUMP_TILE:
			return '*';

		case TELEPORTER_TILE:
			return 'T';

		case WALL_TILE:
			return 'W';

		case HIGH_WALL_TILE:
			return '#';

		}

		return ' '; // when in doubt, return a hole
	}

	@Override
	public Map loadMap() throws IOException, MapFormatException {
		return loadMap(this);
	}
}
