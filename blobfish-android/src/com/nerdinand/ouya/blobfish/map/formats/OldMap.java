package com.nerdinand.ouya.blobfish.map.formats;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.Map;
import com.nerdinand.ouya.blobfish.map.MapFormatException;
import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.ColourTile;
import com.nerdinand.ouya.blobfish.map.tile.EmptyTile;
import com.nerdinand.ouya.blobfish.map.tile.TeleporterTile;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.WallTile;

public class OldMap extends Map {

	private static final String TAG = "Map";

	private int width;
	private int height;

	private int pngSize;

	int[][] tilesBytes;
	int[][] teleporterX;
	int[][] teleporterY;

	int[][] flags;

	private String startupScript;
	
	enum TileType {
		TileEmpty(0), TileFloor(1), TileTeleport(2), TileWall(3), TileSpawn(4), TileConnect(5), TileRestrictSpawn(6), TileRestrictPowerupSpawn(7), TileTrigger(8), TileScript(9);

		private int index;

		TileType(int index) {
			this.index = index;
		}
		
		public static TileType getByIndex(int index){
			for (TileType type : values()) {
				if (type.index == index){
					return type;
				}
			}
			
			return TileEmpty;
		}
	}
	
	public OldMap(String path) {
		super(path);
	}

	@Override
	public Map loadMap() throws IOException, MapFormatException{
		return loadOldFormat(this);
	}
	
	private static OldMap loadOldFormat(OldMap map) throws IOException, MapFormatException {
		String mapPath = map.getPath();
		Gdx.app.debug(TAG, "Reading map from " + mapPath + " in old Format...");

		FileHandle handle = Gdx.files.internal(mapPath);
		InputStream inputStream = handle.read();

		map.setVersion(inputStream.read());

		Gdx.app.debug(TAG, "Map version: " + map.getVersion());

		byte[] bytes = new byte[4]; // long type in DarkBasic is 4 bytes

		inputStream.read(bytes);
		map.setPlayerCount(bytesToIntLE(bytes));

		Gdx.app.debug(TAG, "Player count: " + map.getPlayerCount());

		inputStream.read(bytes);
		map.width = bytesToIntLE(bytes);

		inputStream.read(bytes);
		map.height = bytesToIntLE(bytes);

		Gdx.app.debug(TAG, "Map size: " + map.width + ", " + map.height);

		inputStream.read(bytes);
		map.pngSize = bytesToIntLE(bytes);

		Gdx.app.debug(TAG, "PNG size: " + map.pngSize);

		bytes = new byte[map.pngSize];
		inputStream.read(bytes);
		// TODO: do something with bytes (minimap-png)

		map.tilesBytes = new int[map.width][map.height];
		map.teleporterX = new int[map.width][map.height];
		map.teleporterY = new int[map.width][map.height];
		map.flags = new int[map.width][map.height];

		bytes = new byte[4];

		for (int i = 0; i < map.width; i++) {
			for (int j = 0; j < map.height; j++) {
				map.tilesBytes[i][j] = inputStream.read();

				inputStream.read(bytes);
				map.teleporterX[i][j] = bytesToIntLE(bytes);

				inputStream.read(bytes);
				map.teleporterY[i][j] = bytesToIntLE(bytes);

				map.flags[i][j] = inputStream.read();
			}
		}

		int startupScriptExists = inputStream.read();

		if (startupScriptExists == 1) {
			bytes = new byte[4];

			inputStream.read(bytes);
			int z = bytesToIntLE(bytes);

			inputStream.read(bytes);
			int px = bytesToIntLE(bytes);

			inputStream.read(bytes);
			int py = bytesToIntLE(bytes);

			Gdx.app.debug(TAG, "Startup script: z: " + z + " px: " + px + " py: " + py);

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				map.startupScript += line + "\n";
				Gdx.app.debug(TAG, line);
			}
		}

		map.deserialize();

		return map;
	}

	private static int bytesToIntLE(byte[] bytes) {
		int value = 0;

		for (int i = 0; i < bytes.length; i++) {
			value += ((long) bytes[i] & 0xffL) << (8 * i);
		}
		return value;
	}

	public void deserialize() throws MapFormatException {
		int width = this.tilesBytes.length;
		int height = this.tilesBytes[0].length;

		Array<Array<Tile>> tiles = new Array<Array<Tile>>(width);

		for (int i = 0; i < width; i++) {
			int[] row = this.tilesBytes[i];
			Array<Tile> tilesRow = new Array<Tile>(height);

			for (int j = 0; j < height; j++) {
				int tileInt = row[j];

				Vector2 tileCoordinates = new Vector2(i, j);

				Tile newTile = newTile(tileInt, tileCoordinates);
				tilesRow.add(newTile);
			}

			tiles.add(tilesRow);
		}

		TileField tileField = new TileField(tiles);
		this.setTileField(tileField);
	}

	private Tile newTile(int tileInt, Vector2 tileCoordinates) {
		TileType tileType = TileType.getByIndex(tileInt);

		switch (tileType) {
		case TileEmpty:
			return new EmptyTile(tileCoordinates);

		case TileFloor:
			return new ColourTile(tileCoordinates);

		case TileSpawn:
			return new ColourTile(tileCoordinates, true);

		case TileWall:
			return new WallTile(tileCoordinates);

		case TileTeleport:
			int x = (int) tileCoordinates.x;
			int y = (int) tileCoordinates.y;
			return new TeleporterTile(tileCoordinates, new Vector2(teleporterX[x][y] - 1, teleporterY[x][y] - 1));

		default:
			return new EmptyTile(tileCoordinates); // when in doubt, better
													// return an
			// EmptyTile (hole)
		}
	}
}
