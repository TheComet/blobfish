package com.nerdinand.ouya.blobfish.map.tile;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.nerdinand.ouya.blobfish.Assets;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine;
import com.nerdinand.ouya.blobfish.simulation.player.Player;

public class ColourTile extends VisibleTile {

	private static final String TAG = "ColourTile";

	private Player owner;

	private boolean isSpawnPoint = false;
	private boolean canSpawnPowerUp = true;

	private boolean locked;

	private float currentLockTime;
	private float lockTime;
	
	public ColourTile(Vector2 tileCoordinates) {
		super(tileCoordinates);
		
		this.lockTime = ScreenStateMachine.getGameSettings().getLockTime();
	}

	public ColourTile(Vector2 tileCoordinates, boolean isSpawnPoint) {
		this(tileCoordinates);

		this.isSpawnPoint = isSpawnPoint;
	}

	public ColourTile(Vector2 tileCoordinates, boolean isSpawnPoint, boolean canSpawnPowerUp) {
		this(tileCoordinates, isSpawnPoint);

		this.canSpawnPowerUp = canSpawnPowerUp;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public Player getOwner() {
		return owner;
	}

	@Override
	public boolean canSpawnPowerUp() {
		return canSpawnPowerUp;
	}

	@Override
	public boolean isSpawnPoint() {
		return isSpawnPoint;
	}

	@Override
	public Pixmap getMiniMapIcon() {
		String iconName;

		if (isSpawnPoint()) {
			iconName = Assets.PIXMAP_MINIMAP_SPAWN;
		} else if (!canSpawnPowerUp()) {
			iconName = Assets.PIXMAP_MINIMAP_RESTRICT_POWERUP;
		} else {
			iconName = Assets.PIXMAP_MINIMAP_COLOURTILE;
		}
		
		return Assets.assetManager.get(iconName, Pixmap.class);
	}

	@Override
	public boolean isWalkable() {
		return true;
	}

	public boolean isLocked() {
		return locked;
	}
	
	public void setLocked() {
		this.locked = true;
	}

	@Override
	public void update(float deltaTime) {
		if (isLocked()){
			currentLockTime += deltaTime;
			
			if (currentLockTime > lockTime){
				currentLockTime = 0;
				locked = false;
			}
		}
	}
	
	@Override
	public TileType getTileType() {
		return TileType.COLOUR_TILE;
	}
}
