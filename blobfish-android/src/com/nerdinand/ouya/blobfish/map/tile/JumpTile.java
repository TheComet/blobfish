package com.nerdinand.ouya.blobfish.map.tile;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.nerdinand.ouya.blobfish.Assets;

public class JumpTile extends VisibleTile {

	public JumpTile(Vector2 tileCoordinates) {
		super(tileCoordinates);
	}
	
	@Override
	public boolean isSpawnPoint() {
		return false;
	}

	@Override
	public boolean canSpawnPowerUp() {
		return false;
	}

	@Override
	public boolean isWalkable() {
		return true;
	}

	@Override
	public Pixmap getMiniMapIcon() {
		return Assets.assetManager.get(Assets.PIXMAP_MINIMAP_JUMP, Pixmap.class);
	}

	@Override
	public TileType getTileType() {
		return TileType.JUMP_TILE;
	}

}
