package com.nerdinand.ouya.blobfish.map.tile;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.nerdinand.ouya.blobfish.Assets;

public class NullTile extends Tile {

	public NullTile(Vector2 tileCoordinates) {
		super(tileCoordinates);
	}

	@Override
	public boolean isSpawnPoint() {
		return false;
	}

	@Override
	public boolean canSpawnPowerUp() {
		return false;
	}

	@Override
	public boolean doesRender() {
		return false;
	}

	@Override
	public Pixmap getMiniMapIcon() {
		return Assets.assetManager.get(Assets.PIXMAP_MINIMAP_EMPTY, Pixmap.class);
	}

	@Override
	public boolean isWalkable() {
		return true;
	}

	@Override
	public void update(float deltaTime) {
	}

	@Override
	public Vector3 getDimension() {
		return Vector3.Zero;
	}

	@Override
	public TileType getTileType() {
		return TileType.NULL_TILE;
	}
}
