package com.nerdinand.ouya.blobfish.map.tile;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.nerdinand.ouya.blobfish.Assets;

public class TeleporterTile extends VisibleTile {

	private Tile destinationTile;
	private Vector2 destination;

	public TeleporterTile(Vector2 tileCoordinates, Vector2 destination) {
		this(tileCoordinates);

		this.destination = destination;
	}

	public TeleporterTile(Vector2 tileCoordinates) {
		super(tileCoordinates);
	}

	@Override
	public boolean isSpawnPoint() {
		return false;
	}

	@Override
	public boolean canSpawnPowerUp() {
		return false;
	}
	
	public void setDestination(Vector2 destination) {
		this.destination = destination;
	}

	public Vector2 getDestination() {
		if (destinationTile != null) {
			return destinationTile.getTileCoordinates();
		}

		return destination;
	}

	public Tile getDestinationTile() {
		return destinationTile;
	}

	public void setDestinationTile(Tile destinationTile) {
		this.destinationTile = destinationTile;
	}

	@Override
	public Pixmap getMiniMapIcon() {
		return Assets.assetManager.get(Assets.PIXMAP_MINIMAP_TELEPORTER, Pixmap.class);
	}

	@Override
	public boolean isWalkable() {
		return true;
	}
	
	@Override
	public TileType getTileType() {
		return TileType.TELEPORTER_TILE;
	}
}
