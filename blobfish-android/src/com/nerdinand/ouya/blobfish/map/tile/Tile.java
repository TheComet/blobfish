package com.nerdinand.ouya.blobfish.map.tile;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.nerdinand.ouya.blobfish.map.TileField;

public abstract class Tile {
	private static final String TAG = "Tile";
	public static float X_DIMENSION = 10f;
	public static float Y_DIMENSION = 10f;
	
	protected static final Vector3 DIMENSION = new Vector3(-X_DIMENSION, 0, Y_DIMENSION);

	public enum TileType {
		COLOUR_TILE,
		EMPTY_TILE,
		NULL_TILE,
		TELEPORTER_TILE,
		WALL_TILE, 
		JUMP_TILE, 
		HIGH_WALL_TILE
	}
	
	private BoundingBox boundingBox;

	private Vector3 position;
	private Vector2 tileCoordinates;

	public Tile(Vector2 tileCoordinates) {
		setTileCoordinates(tileCoordinates);
	}

	public abstract boolean isSpawnPoint();

	public abstract boolean canSpawnPowerUp();

	public abstract boolean isWalkable();

	public abstract boolean doesRender();

	public abstract Vector3 getDimension();
	
	public abstract TileType getTileType();
	
	private void setPosition(Vector3 position) {
		this.position = position;
	}

	public void setTileCoordinates(Vector2 tileCoordinates) {
		this.tileCoordinates = tileCoordinates;

		setPosition(TileField.mapCoordinatesToWorld(tileCoordinates));
	}

	public Vector2 getTileCoordinates() {
		return tileCoordinates;
	}

	public Vector3 getPosition() {
		return position;
	}

	public void setBoundingBox(BoundingBox boundingBox) {
		this.boundingBox = boundingBox;
	}

	public BoundingBox getBoundingBox() {
		return boundingBox;
	}

	public abstract Pixmap getMiniMapIcon();

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " at " + getTileCoordinates();
	}

	/**
	 * Checks whether to is directly adjacent diagonal to this tile
	 * 
	 * @param to
	 * @return
	 */
	public boolean diagonalTo(Tile to) {
		return (Math.abs((int) to.getTileCoordinates().x - this.getTileCoordinates().x) == 1 && Math.abs((int) to.getTileCoordinates().y
				- this.getTileCoordinates().y) == 1);
	}

	public double getgCost(Tile to) {
		if(this.diagonalTo(to)) {
			return (double) 100;
		} else if (this.getTileType() == TileType.TELEPORTER_TILE) {
			return (double) 15;
		} else {
			return (double) 10;
		}
	}

	public Vector3 getCenter() {
		return getPosition();
	}
	
	public abstract void update(float deltaTime);
}