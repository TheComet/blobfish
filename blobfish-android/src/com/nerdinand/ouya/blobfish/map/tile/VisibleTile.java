package com.nerdinand.ouya.blobfish.map.tile;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUp;

public abstract class VisibleTile extends Tile {

	private static final Vector3 DIMENSION = new Vector3(-X_DIMENSION, 0, Y_DIMENSION);

	private PowerUp powerUp;

	public VisibleTile(Vector2 tileCoordinates) {
		super(tileCoordinates);

		Vector3 min = TileField.mapCoordinatesToWorld(tileCoordinates);
		
		Vector3 dimension2 = getDimension().cpy();

		min.sub(dimension2.cpy().div(2)); 
		Vector3 max = min.cpy().add(dimension2);

		BoundingBox boundingBox2 = new BoundingBox(min, max);
		setBoundingBox(boundingBox2);
		
	}

	public void setPowerUp(PowerUp powerUp) {
		this.powerUp = powerUp;
	}

	public PowerUp getPowerUp() {
		return powerUp;
	}

	@Override
	public boolean doesRender() {
		return true;
	}

	public boolean hasPowerUp() {
		return getPowerUp() != null;
	}

	@Override
	public void update(float deltaTime) {
	}

	@Override
	public Vector3 getDimension() {
		return DIMENSION;
	}
}
