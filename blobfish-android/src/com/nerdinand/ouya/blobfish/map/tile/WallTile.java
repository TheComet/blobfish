package com.nerdinand.ouya.blobfish.map.tile;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.nerdinand.ouya.blobfish.Assets;

public class WallTile extends VisibleTile {

	private static final float HEIGHT = 20;
	
	private static Vector3 dimension;

	public WallTile(Vector2 tileCoordinates) {
		super(tileCoordinates);
	}

	@Override
	public boolean isSpawnPoint() {
		return false;
	}

	@Override
	public boolean canSpawnPowerUp() {
		return false;
	}

	@Override
	public Pixmap getMiniMapIcon() {
		return Assets.assetManager.get(Assets.PIXMAP_MINIMAP_WALL, Pixmap.class);
	}

	@Override
	public boolean isWalkable() {
		return false;
	}
	
	@Override
	public Vector3 getDimension() {
		if (dimension == null){
			dimension = super.getDimension().cpy().add(0, HEIGHT, 0);
		}
		
		return dimension;
	}
	
	@Override
	public TileType getTileType() {
		return TileType.WALL_TILE;
	}
}
