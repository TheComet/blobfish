package com.nerdinand.ouya.blobfish.screen;

import com.badlogic.gdx.Screen;

public abstract class BlobfishScreen implements Screen {	
	public abstract ScreenStateMachine.ScreenType getScreenType();
	
	public abstract boolean isDone();
	
	public abstract boolean backAllowed();
	
	public abstract void setDone(boolean done);
}
