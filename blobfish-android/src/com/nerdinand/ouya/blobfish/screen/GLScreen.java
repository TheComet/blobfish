package com.nerdinand.ouya.blobfish.screen;


public abstract class GLScreen extends BlobfishScreen {
	
	@Override
	public void dispose() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void render(float deltaTime) {
		update(deltaTime);
		draw(deltaTime);
	}

	public abstract void draw(float deltaTime);

	public abstract void update(float deltaTime);
	
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void resume() {
	}

	@Override
	public void show() {
	}

}
