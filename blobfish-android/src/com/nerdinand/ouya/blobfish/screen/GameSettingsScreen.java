package com.nerdinand.ouya.blobfish.screen;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.nerdinand.ouya.blobfish.libgdx_extension.InputManagerStage.SelectionListener;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine.ScreenType;
import com.nerdinand.ouya.blobfish.simulation.GameSettings;

public class GameSettingsScreen extends UIScreen implements SelectionListener {

	private static final int DURATION_STEP = 30;
	private static final int MAX_DURATION = 300;
	private static final int MIN_DURATION = 30;
	private static final float MIN_LOCK_TIME = 0;
	private static final float MAX_LOCK_TIME = 20;
	private static final float LOCK_TIME_STEP = 1;
	
	private GameSettings gameSettings;
	private TextButton startGameButton;
	private CheckBox powerUpCheckBox;
	private boolean done = false;
	private Slider gameDurationSlider;
	private Label durationLabel;
	private Slider lockTimeSlider;
	private Label lockTimeLabel;

	public GameSettingsScreen(GameSettings gameSettings) {
		this.gameSettings = gameSettings;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		Table table = new Table(getSkin());

		Label mapLabel = new Label("Enable powerups", getSkin());
		table.add(mapLabel).pad(10);

		powerUpCheckBox = new CheckBox(null, getSkin());
		table.add(powerUpCheckBox).pad(10);
		table.row();

		powerUpCheckBox.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				powerUpCheckBox.toggle();

				gameSettings.setPowerUpsEnabled(powerUpCheckBox.isChecked());

				return true;
			}
		});
		
		powerUpCheckBox.setChecked(gameSettings.isPowerUpsEnabled());
		
		gameDurationSlider = new Slider(MIN_DURATION, MAX_DURATION, DURATION_STEP, false, getSkin());
		durationLabel = new Label(null, getSkin());

		gameDurationSlider.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				durationLabel.setText("Game duration: "+gameDurationSlider.getValue());
				gameSettings.setDuration(gameDurationSlider.getValue());
			}
		});
		
		gameDurationSlider.setValue(gameSettings.getDuration());
		
		table.add(durationLabel).pad(10);
		table.add(gameDurationSlider).pad(10);
		table.row();
		
		lockTimeSlider = new Slider(MIN_LOCK_TIME, MAX_LOCK_TIME, LOCK_TIME_STEP, false, getSkin());
		lockTimeLabel = new Label(null, getSkin());
		lockTimeSlider.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				lockTimeLabel.setText("Tile lock time: "+lockTimeSlider.getValue());
				gameSettings.setLockTime(lockTimeSlider.getValue());
			}
		});
		
		lockTimeSlider.setValue(gameSettings.getLockTime());
		
		table.add(lockTimeLabel).pad(10);
		table.add(lockTimeSlider).pad(10);
		table.row();
		
		startGameButton = new TextButton("Start game", getSkin());
		table.add(startGameButton);

		stage.selectActor(startGameButton);

		startGameButton.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				setAction(ACTION_START_GAME);
				
				setDone(true);
				
				return true;
			}
		});

		float layoutWidth = BUTTON_WIDTH * 2;
		float layoutHeight = BUTTON_HEIGHT * 3;

		final float startX = (width - layoutWidth) / 2;
		float currentY = height - 50;

		table.setX(startX);
		table.setY(currentY -= layoutHeight);
		table.setWidth(layoutWidth);
		table.setHeight(layoutHeight);
		stage.addActor(table);

		stage.setSelectionListener(this);

	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.GAME_SETTINGS;
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public boolean backAllowed() {
		return true;
	}

	@Override
	public void setDone(boolean done) {
		this.done = done;
	}

	@Override
	public void selectionChanged(Actor oldSelection, Actor newSelection) {
		// TODO Auto-generated method stub

	}

}
