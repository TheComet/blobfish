package com.nerdinand.ouya.blobfish.screen;

import java.util.HashMap;
import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.nerdinand.ouya.blobfish.screen.types.CreditsScreen;
import com.nerdinand.ouya.blobfish.screen.types.GameOverScreen;
import com.nerdinand.ouya.blobfish.screen.types.GameScreen;
import com.nerdinand.ouya.blobfish.screen.types.LoadingScreen;
import com.nerdinand.ouya.blobfish.screen.types.LogoScreen;
import com.nerdinand.ouya.blobfish.screen.types.MapPackSelectionScreen;
import com.nerdinand.ouya.blobfish.screen.types.MapSelectionScreen;
import com.nerdinand.ouya.blobfish.screen.types.PauseScreen;
import com.nerdinand.ouya.blobfish.screen.types.PlayerSelectionScreen;
import com.nerdinand.ouya.blobfish.screen.types.TitleScreen;
import com.nerdinand.ouya.blobfish.simulation.GameSettings;
import com.nerdinand.ouya.blobfish.simulation.Simulation;

public class ScreenStateMachine {
	private static LinkedList<ScreenType> screenHistory = new LinkedList<ScreenType>();
	
	private static GameSettings gameSettings = new GameSettings();
	
	private static HashMap<ScreenType, BlobfishScreen> screenSingletons = new HashMap<ScreenStateMachine.ScreenType, BlobfishScreen>();
	
	public enum ScreenType {
		LOGO, TITLE, CREDITS, PLAYER_SELECTION, MAP_PACK_SELECTION, MAP_SELECTION, LOADING, GAME, GAME_OVER, PAUSE, GAME_SETTINGS
	}

	public static BlobfishScreen getNextScreen() {
		BlobfishScreen currentScreen = screenSingletons.get(screenHistory.getFirst());
		ScreenType screenType = currentScreen.getScreenType();
		Simulation simulation;

		switch (screenType) {
		case LOGO:
			return changeScreenTo(new TitleScreen());
			
		case TITLE:
			String action = ((UIScreen) currentScreen).getAction();

			if (action.equals(TitleScreen.ACTION_START_GAME)) {
				return changeScreenTo(new PlayerSelectionScreen(getGameSettings()));
			} else if (action.equals(TitleScreen.ACTION_CREDITS)) {
				return changeScreenTo(new CreditsScreen());
			} else if (action.equals(TitleScreen.ACTION_EXIT)) {
				Gdx.app.exit();
			}
			break;

		case PLAYER_SELECTION:
			return changeScreenTo(new MapPackSelectionScreen(getGameSettings()));
		
		case MAP_PACK_SELECTION:
			return changeScreenTo(new MapSelectionScreen(getGameSettings()));
			
		case MAP_SELECTION:
			return changeScreenTo(new GameSettingsScreen(getGameSettings()));

		case GAME_SETTINGS:
			return changeScreenTo(new LoadingScreen(getGameSettings()));
			
		case LOADING:
			return changeScreenTo(new GameScreen(getGameSettings()));
			
		case GAME:
			simulation = ((GameScreen) currentScreen).getSimulation();
			return changeScreenTo(new GameOverScreen(simulation.getWinner()));
			
		case GAME_OVER:
			return changeScreenTo(new TitleScreen());
			
		case CREDITS:
			return changeScreenTo(new TitleScreen());
			
		case PAUSE:
			action = ((UIScreen) currentScreen).getAction();
			
			if (action.equals(PauseScreen.ACTION_CONTINUE)) {
				return getPreviousScreen();
				
			} else if (action.equals(PauseScreen.ACTION_EXIT)) {
				if (currentScreen.getScreenType() == ScreenType.GAME){
					simulation = ((GameScreen) currentScreen).getSimulation();
					simulation.stopGame();
				}
				return changeScreenTo(new TitleScreen());
			}
			
			break;
		default:
			break;
		}

		return currentScreen;
	}

	public static GameSettings getGameSettings() {
		return gameSettings;
	}
	
	public static Screen getStartScreen() {
		return changeScreenTo(new LogoScreen());
	}

	public static BlobfishScreen getPreviousScreen() {
		screenHistory.removeFirst();
		return screenSingletons.get(screenHistory.getFirst());
	}

	public static BlobfishScreen getPauseScreen() {
		return changeScreenTo(new PauseScreen());
	}
	
	private static BlobfishScreen changeScreenTo(BlobfishScreen screen){
		ScreenType screenType = screen.getScreenType();
		screenHistory.addFirst(screenType);
		screenSingletons.put(screenType, screen);
		
		return screen;
	}

}
