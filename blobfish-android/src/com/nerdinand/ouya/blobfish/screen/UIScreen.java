package com.nerdinand.ouya.blobfish.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.nerdinand.ouya.blobfish.Blobfish;
import com.nerdinand.ouya.blobfish.input.InputManagerListener;
import com.nerdinand.ouya.blobfish.libgdx_extension.InputManagerStage;

public abstract class UIScreen extends BlobfishScreen implements InputManagerListener {
	public static final float BUTTON_WIDTH = 300f;
	public static final float BUTTON_HEIGHT = 50f;
	public static final float BUTTON_SPACING = 40f;
	public static final String ACTION_START_GAME = "startGame";
	public static final String ACTION_CREDITS = "credits";
	public static final String ACTION_EXIT = "exit";
	
	private static final String TAG = "AbstractScreen";
	private BitmapFont font;
	private SpriteBatch batch;
	private Skin skin;
	protected InputManagerStage stage;
	private String action;

	public UIScreen() {
		this.stage = new InputManagerStage(0, 0, true);
	}
	
	protected String getName() {
		return getClass().getSimpleName();
	}

	public BitmapFont getFont() {
		if (font == null) {
			font = new BitmapFont();
		}
		return font;
	}

	public SpriteBatch getBatch() {
		if (batch == null) {
			batch = new SpriteBatch();
		}
		return batch;
	}

	protected Skin getSkin() {
		if (skin == null) {
			skin = new Skin(Gdx.files.internal("ui/skin.json"));
		}
		return skin;
	}

	// Screen implementation

	@Override
	public void show() {
		Blobfish.getInputManager().addListener(this);
		Blobfish.getInputManager().addListener(getStage());
		Gdx.app.log(TAG, "Showing screen: " + getName());
	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log(TAG, "Resizing screen: " + getName() + " to: " + width + " x " + height);

		// resize and clear the stage
		stage.setViewport(width, height, true);
		stage.clear();
	}

	@Override
	public void render(float delta) {
		// (1) process the game logic

		// update the actors
		stage.act(delta);

		// (2) draw the result

		// clear the screen with the given RGB color (black)
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		drawBackground(delta);
		
		// draw the actors
		stage.draw();
	}

	protected void drawBackground(float delta) {
	}
	
	protected void setAction(String action){
		this.action = action;
	}
	
	public String getAction() {
		return action;
	}

	@Override
	public void hide() {
		Blobfish.getInputManager().removeListener(getStage());
		Blobfish.getInputManager().removeListener(this);
		Gdx.app.log(TAG, "Hiding screen: " + getName());
	}

	@Override
	public void pause() {
		Gdx.app.log(TAG, "Pausing screen: " + getName());
	}

	@Override
	public void resume() {
		Gdx.app.log(TAG, "Resuming screen: " + getName());
	}

	@Override
	public void dispose() {
		Blobfish.getInputManager().removeListener(this);
		
		Gdx.app.log(TAG, "Disposing screen: " + getName());
		stage.dispose();
		if (font != null)
			font.dispose();
		if (batch != null)
			batch.dispose();
		if (skin != null)
			skin.dispose();
	}
	
	public InputManagerStage getStage() {
		return stage;
	}

}
