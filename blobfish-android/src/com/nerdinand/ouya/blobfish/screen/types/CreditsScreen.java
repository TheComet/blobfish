package com.nerdinand.ouya.blobfish.screen.types;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.nerdinand.ouya.blobfish.input.FunctionButtonsListener;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine.ScreenType;
import com.nerdinand.ouya.blobfish.screen.UIScreen;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;

public class CreditsScreen extends UIScreen implements FunctionButtonsListener {

	private boolean done = false;

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		Label label = new Label("Project Manager\n  Ferdinand Niedermann\nLead Design \n  Alex Murray\nProgramming \n  Ferdinand Niedermann, Alex Murray\nArt \n  Alex Murray\nProgramming Assistance\n  Lukas Walter\nProducer \n  Ferdinand Niedermann\n\n� 2013 Alex Murray, Ferdinand Niedermann\n\nmade with libgdx", getSkin());

		label.setX((width - label.getWidth()) / 2);
		label.setY(height - label.getHeight()-50);
		stage.addActor(label);
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.CREDITS;
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public void functionButtonDown(LocalPlayer player, FunctionButtonType functionType) {
		done = true;
	}

	@Override
	public void functionButtonUp(LocalPlayer player, FunctionButtonType functionType) {
	}

	@Override
	public boolean backAllowed() {
		return true;
	}

	@Override
	public void setDone(boolean done) {
		this.done = done;
	}

}
