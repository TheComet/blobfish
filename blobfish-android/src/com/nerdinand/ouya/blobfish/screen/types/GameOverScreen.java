package com.nerdinand.ouya.blobfish.screen.types;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.nerdinand.ouya.blobfish.input.FunctionButtonsListener;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine.ScreenType;
import com.nerdinand.ouya.blobfish.screen.UIScreen;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;
import com.nerdinand.ouya.blobfish.simulation.player.Player;

public class GameOverScreen extends UIScreen implements FunctionButtonsListener {

	private Player winner;

	private boolean done = false;

	public GameOverScreen(Player winner) {
		this.winner = winner;
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		final float buttonX = (width - BUTTON_WIDTH) / 2;
		float currentY = 280f;

		Label welcomeLabel = new Label("Winner: " + winner.toString() +" Score: "+winner.getScore(), getSkin());
		welcomeLabel.setX(buttonX);
		welcomeLabel.setY(currentY + 100);
		stage.addActor(welcomeLabel);
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.GAME_OVER;
	}

	@Override
	public boolean backAllowed() {
		return false;
	}

	@Override
	public void setDone(boolean done) {
		this.done = done;
	}

	@Override
	public void functionButtonDown(LocalPlayer player, FunctionButtonType functionType) {
		done = true;
	}

	@Override
	public void functionButtonUp(LocalPlayer player, FunctionButtonType functionType) {
	}
}
