package com.nerdinand.ouya.blobfish.screen.types;

import com.nerdinand.ouya.blobfish.Assets;
import com.nerdinand.ouya.blobfish.GameRenderer;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.screen.GLScreen;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine.ScreenType;
import com.nerdinand.ouya.blobfish.simulation.GameSettings;
import com.nerdinand.ouya.blobfish.simulation.Simulation;
import com.nerdinand.ouya.blobfish.simulation.SimulationListener;
import com.nerdinand.ouya.blobfish.simulation.player.Player;

public class GameScreen extends GLScreen implements SimulationListener {

	private static final String TAG = "GameScreen";
	private Simulation simulation;

	private boolean done = false;
	private GameRenderer renderer;

	public GameScreen(GameSettings gameSettings) {
		Assets.loadGameAssets();

		simulation = new Simulation(gameSettings);
		simulation.setListener(this);

		renderer = new GameRenderer();

		// JSONMap jsonMap = new JSONMap((OldMap) map);
		// jsonMap.saveMap(map.getPath()+".map");

		// for (Map map2 : MapManager.getMapList()) {
		// Map loadedMap = null;
		//
		// try {
		// loadedMap = map2.loadMap();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (MapFormatException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// MiniMap miniMap = new MiniMap(loadedMap);
		// Pixmap pixmap = miniMap.getPixmap();
		//
		// PixmapIO.writePNG(Gdx.files.external(loadedMap.getPath() + ".png"), pixmap);
		// }
	}

	public Simulation getSimulation() {
		return simulation;
	}

	@Override
	public void draw(float deltaTime) {
		renderer.render(simulation, deltaTime);
	}

	@Override
	public void update(float deltaTime) {
		simulation.update(deltaTime);
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public void dispose() {
		renderer.dispose();
	}

	@Override
	public void hide() {
		super.hide();
		
		renderer.hide();
	}
	
//	private void testLua() {
//		Gdx.app.debug(TAG, "Running lua...");
//		// String script = Gdx.files.internal("helloworld.lua").readString();
//		try {
//			String script = "print 'Hello World!'";
//			InputStream input = new ByteArrayInputStream(script.getBytes());
//			Prototype prototype = LuaC.compile(input, "script");
//			LuaValue globals = JsePlatform.standardGlobals();
//			LuaClosure closure = new LuaClosure(prototype, globals);
//			closure.call();
//
//			script = "function something(argument)\n" + "test_string = 'Hello World!'\n" + "print(test_string)\n" + "print(argument)\n" + "end";
//
//			input = new ByteArrayInputStream(script.getBytes());
//			prototype = LuaC.compile(input, "script");
//			globals = JsePlatform.standardGlobals();
//			closure = new LuaClosure(prototype, globals);
//			closure.invokemethod("something", CoerceJavaToLua.coerce("Foo"));
//
//			// closure.invokemethod("something", LuaJHelper.argumentsToLua(new
//			// Object[]{"Foo"}));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

	@Override
	public void onGameOver() {
		setDone(true);
	}

	@Override
	public void tileOwned(Tile tile, Player player) {
		for (Player localPlayer : simulation.getLocalPlayerList()) {
			// SoundPlayer.playSound(Assets.assetManager.get(Assets.SOUND_CHIRP, Sound.class), localPlayer.getPosition(), player.getPosition());
		}
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.GAME;
	}

	@Override
	public void powerUpGet(Tile tile, Player player) {
		// TODO do something useful here (e.g. play a sound)
	}

	@Override
	public boolean backAllowed() {
		return false;
	}

	@Override
	public void setDone(boolean done) {
		this.done = done;
	}
	
	@Override
	public void resume() {
		renderer.resume();
	}

}
