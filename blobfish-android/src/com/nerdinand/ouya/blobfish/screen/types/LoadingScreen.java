package com.nerdinand.ouya.blobfish.screen.types;

import java.io.IOException;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.nerdinand.ouya.blobfish.Assets;
import com.nerdinand.ouya.blobfish.map.Map;
import com.nerdinand.ouya.blobfish.map.MapFormatException;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine.ScreenType;
import com.nerdinand.ouya.blobfish.screen.UIScreen;
import com.nerdinand.ouya.blobfish.simulation.GameSettings;

public class LoadingScreen extends UIScreen {

	private GameSettings gameSettings;

	public LoadingScreen(GameSettings gameSettings) {
		Assets.enqueueGameAssets();

		this.gameSettings = gameSettings;
	}

	public Map getSelectedMap() {
		return gameSettings.getMap();
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.LOADING;
	}

	@Override
	public boolean isDone() {
		boolean finished = Assets.assetManager.update();
		if (!finished) {
			return false;
		}

		try {
			getSelectedMap().deserialize();

		} catch (MapFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		float layoutWidth = BUTTON_WIDTH * 2;
		float layoutHeight = BUTTON_HEIGHT * 3 * 4;
		final float startX = (width - layoutWidth) / 2;
		float currentY = height - 50;
		
		TextureRegion iconLS = Assets.getRegion(Assets.TEXTURE_ATLAS_UI, Assets.TEXTURE_OUYA_ICON_LS);
		TextureRegion iconO = Assets.getRegion(Assets.TEXTURE_ATLAS_UI, Assets.TEXTURE_OUYA_ICON_O);
		TextureRegion iconSYSTEM = Assets.getRegion(Assets.TEXTURE_ATLAS_UI, Assets.TEXTURE_OUYA_ICON_SYSTEM);

		Table table = new Table(getSkin());

		table.add(new Image(iconLS)).pad(10);
		Label iconLSLabel = new Label("move", getSkin());
		table.add(iconLSLabel);
		table.row();
		table.add(new Image(iconO)).pad(10);
		Label iconOLabel = new Label("trigger teleporters or powerups", getSkin());
		table.add(iconOLabel);
		table.row();
		table.add(new Image(iconSYSTEM)).pad(10);
		Label iconSYSTEMLabel = new Label("pause", getSkin());
		table.add(iconSYSTEMLabel);

		table.setX(startX);
		table.setY(currentY -= layoutHeight);
		table.setWidth(layoutWidth);
		table.setHeight(layoutHeight);
		stage.addActor(table);

		Label placeHolderLabel = new Label("Loading...", getSkin());
		placeHolderLabel.setX((width - placeHolderLabel.getWidth()) / 2);
		placeHolderLabel.setY(currentY);
		stage.addActor(placeHolderLabel);
	}

	@Override
	public boolean backAllowed() {
		return false;
	}

	@Override
	public void setDone(boolean done) {
	}

}