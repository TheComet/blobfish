package com.nerdinand.ouya.blobfish.screen.types;

import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.nerdinand.ouya.blobfish.Assets;
import com.nerdinand.ouya.blobfish.input.FunctionButtonsListener;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine.ScreenType;
import com.nerdinand.ouya.blobfish.screen.UIScreen;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;

public class LogoScreen extends UIScreen implements FunctionButtonsListener {

	private float durationLeft = 3;

	private boolean done = false;

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		AtlasRegion canterlockLogo = Assets.getRegion(Assets.TEXTURE_ATLAS_UI, Assets.TEXTURE_CANTERLOCK_LOGO);
		AtlasRegion codegestaltLogo = Assets.getRegion(Assets.TEXTURE_ATLAS_UI, Assets.TEXTURE_CODEGESTALT_LOGO);
		
		float currentY = (height - (canterlockLogo.getRegionHeight() + codegestaltLogo.getRegionHeight())) / 2;

		Image codegestaltImage = new Image(codegestaltLogo);
		codegestaltImage.setX((width - codegestaltImage.getWidth()) / 2);
		codegestaltImage.setY(currentY);
		stage.addActor(codegestaltImage);

		currentY += codegestaltImage.getHeight() + 40;

		Image canterlockImage = new Image(canterlockLogo);
		canterlockImage.setX((width - canterlockImage.getWidth()) / 2);
		canterlockImage.setY(currentY);
		stage.addActor(canterlockImage);
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.LOGO;
	}

	@Override
	public boolean isDone() {
		return done || (durationLeft <= 0);
	}

	@Override
	public void functionButtonDown(LocalPlayer player, FunctionButtonType functionType) {
		switch (functionType) {
		case SELECT:
			setDone(true);

			break;

		default:
			break;
		}

	}

	@Override
	public void functionButtonUp(LocalPlayer player, FunctionButtonType functionType) {
	}

	@Override
	public void render(float deltaTime) {
		super.render(deltaTime);

		durationLeft -= deltaTime;
	}

	@Override
	public boolean backAllowed() {
		return false;
	}

	@Override
	public void setDone(boolean done) {
		this.done = done;
	}
}