package com.nerdinand.ouya.blobfish.screen.types;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.libgdx_extension.InputManagerStage.SelectionListener;
import com.nerdinand.ouya.blobfish.map.MapManager;
import com.nerdinand.ouya.blobfish.map.MapPack;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine.ScreenType;
import com.nerdinand.ouya.blobfish.screen.UIScreen;
import com.nerdinand.ouya.blobfish.simulation.GameSettings;

public class MapPackSelectionScreen extends UIScreen implements SelectionListener {
	private boolean done;

	private HashMap<TextButton, MapPack> textButtonMap = new HashMap<TextButton, MapPack>();

	private TextureRegion previewTexture;

	private GameSettings gameSettings;

	private static final int PREVIEW_WIDTH = 500;
	private static final int PREVIEW_HEIGHT = 500;

	public MapPackSelectionScreen(GameSettings gameSettings){
		this.gameSettings = gameSettings;
	}
	
	@Override
	public ScreenType getScreenType() {
		return ScreenType.MAP_PACK_SELECTION;
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		Table table = new Table(getSkin());

		Array<MapPack> mapPackList = MapManager.getMapPackList();
		Array<TextButton> chooseMapButtons = new Array<TextButton>();

		for (final MapPack mapPack : mapPackList) {
			Label mapLabel = new Label(mapPack.getName(), getSkin());
			table.add(mapLabel).pad(10);

			TextButton chooseMapButton = new TextButton("Choose map pack", getSkin());
			table.add(chooseMapButton).pad(10);
			table.row();

			textButtonMap.put(chooseMapButton, mapPack);

			chooseMapButtons.add(chooseMapButton);

			chooseMapButton.addListener(new InputListener() {
				public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
					gameSettings.setMapPack(mapPack);

					setDone(true);

					return true;

				}
			});
		}

		float layoutWidth = BUTTON_WIDTH * 2;
		float layoutHeight = BUTTON_HEIGHT * mapPackList.size;

		// final float startX = (width - layoutWidth) / 2;
		final float startX = width - layoutWidth;
		float currentY = height - 50;

		table.setX(startX);
		table.setY(currentY -= layoutHeight);
		table.setWidth(layoutWidth);
		table.setHeight(layoutHeight);
		stage.addActor(table);

		stage.setSelectionListener(this);

		stage.selectActor(chooseMapButtons.first());
	}

	@Override
	protected void drawBackground(float delta) {
		if (previewTexture != null) {
			SpriteBatch spriteBatch = stage.getSpriteBatch();
			spriteBatch.begin();

			spriteBatch.setColor(Color.WHITE); // otherwise, the preview picture might be red, because of the selected button that was drawn last

			TextureRegion textureRegion = new TextureRegion(previewTexture);
			spriteBatch.draw(textureRegion, 50, (Gdx.graphics.getHeight() - PREVIEW_HEIGHT) / 2, PREVIEW_WIDTH, PREVIEW_HEIGHT);

			spriteBatch.end();
		}
	}

	@Override
	public void selectionChanged(Actor oldSelection, Actor newSelection) {
		MapPack mapPack = textButtonMap.get(newSelection);
		previewTexture = mapPack.getPreview();
	}

	@Override
	public boolean backAllowed() {
		return true;
	}

	@Override
	public void setDone(boolean done) {
		this.done = done;
	}
}
