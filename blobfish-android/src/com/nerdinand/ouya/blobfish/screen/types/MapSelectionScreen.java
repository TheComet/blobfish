package com.nerdinand.ouya.blobfish.screen.types;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.libgdx_extension.InputManagerStage.SelectionListener;
import com.nerdinand.ouya.blobfish.map.formats.JSONMap;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine.ScreenType;
import com.nerdinand.ouya.blobfish.screen.UIScreen;
import com.nerdinand.ouya.blobfish.simulation.GameSettings;

public class MapSelectionScreen extends UIScreen implements SelectionListener {
	private boolean done;

	private HashMap<TextButton, JSONMap> textButtonMap = new HashMap<TextButton, JSONMap>();

	private Texture previewTexture;

	private GameSettings gameSettings;

	private JSONMap map;

	private static final int PREVIEW_WIDTH = 300;
	private static final int PREVIEW_HEIGHT = 300;

	public MapSelectionScreen(GameSettings gameSettings) {
		this.gameSettings = gameSettings;
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.MAP_SELECTION;
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		Table table = new Table(getSkin());

		Array<JSONMap> mapList = gameSettings.getMapPack().getMaps();
		Array<TextButton> chooseMapButtons = new Array<TextButton>();

		for (final JSONMap map : mapList) {
			Label mapLabel = new Label(map.getName(), getSkin());
			table.add(mapLabel).pad(10);

			TextButton chooseMapButton = new TextButton("Choose map", getSkin());
			table.add(chooseMapButton).pad(10);
			table.row();

			textButtonMap.put(chooseMapButton, map);

			chooseMapButtons.add(chooseMapButton);

			chooseMapButton.addListener(new InputListener() {
				public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
					gameSettings.setMap(map);

					setDone(true);

					return true;

				}
			});
		}

		float layoutWidth = BUTTON_WIDTH * 2;
		float layoutHeight = BUTTON_HEIGHT * mapList.size;

		// final float startX = (width - layoutWidth) / 2;
		final float startX = width - layoutWidth;
		float currentY = height - 50;

		table.setX(startX);
		table.setY(currentY -= layoutHeight);
		table.setWidth(layoutWidth);
		table.setHeight(layoutHeight);
		stage.addActor(table);

		stage.setSelectionListener(this);

		stage.selectActor(chooseMapButtons.first());
	}

	@Override
	protected void drawBackground(float delta) {
		if (previewTexture != null) {
			SpriteBatch spriteBatch = stage.getSpriteBatch();
			spriteBatch.begin();

			spriteBatch.setColor(Color.WHITE); // otherwise, the preview picture might be red, because of the selected button that was drawn last

			TextureRegion textureRegion = new TextureRegion(previewTexture);
			spriteBatch.draw(textureRegion, 20, (Gdx.graphics.getHeight() - PREVIEW_HEIGHT) - 20, PREVIEW_WIDTH, PREVIEW_HEIGHT);

			String mapMetaData = "";
			mapMetaData += map.getName() + "\n\n";
			mapMetaData += "created by " + map.getCreator() + " on " + map.getDate() + "\n";
			mapMetaData += map.getDescription() + "\n";
			getFont().drawMultiLine(spriteBatch, mapMetaData, 20, 100);

			spriteBatch.end();
		}
	}

	@Override
	public void selectionChanged(Actor oldSelection, Actor newSelection) {
		final JSONMap map = textButtonMap.get(newSelection);
		this.map = map;

		// load map preview asynchronously (creating the pixmap can take quite some time)
		new Thread(new Runnable() {
			@Override
			public void run() { // in background thread

				final Pixmap preview = map.getPreview();

				Gdx.app.postRunnable(new Runnable() { // back in the opengl thread
					@Override
					public void run() {
						// dispose old texture
						if (previewTexture != null) {
							previewTexture.dispose();
						}

						previewTexture = new Texture(preview);
					}
				});
			}
		}).start();

	}

	@Override
	public boolean backAllowed() {
		return true;
	}

	@Override
	public void setDone(boolean done) {
		this.done = done;
	}
}
