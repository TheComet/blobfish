package com.nerdinand.ouya.blobfish.screen.types;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine.ScreenType;
import com.nerdinand.ouya.blobfish.screen.UIScreen;

public class PauseScreen extends UIScreen {

	public static final String ACTION_CONTINUE = "continue";
	public static final String ACTION_EXIT = "exit";
	private boolean done;
	private TextButton continueButton;
	private Actor quitButton;

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		final float buttonX = (width - BUTTON_WIDTH) / 2;
		float currentY = 280f;

		Label welcomeLabel = new Label("Pause", getSkin());
		welcomeLabel.setX(((width - welcomeLabel.getWidth()) / 2));
		welcomeLabel.setY((currentY + 100));
		stage.addActor(welcomeLabel);

		continueButton = new TextButton("Continue", getSkin());
		continueButton.setX(buttonX);
		continueButton.setY(currentY);
		continueButton.setWidth(BUTTON_WIDTH);
		continueButton.setHeight(BUTTON_HEIGHT);
		stage.addActor(continueButton);

		stage.selectActor(continueButton);
		
		continueButton.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				setAction(ACTION_CONTINUE);
                setDone(true);
				return true;
			}
		});
		
		quitButton = new TextButton("Quit Game", getSkin());
		quitButton.setX(buttonX);
		quitButton.setY((currentY -= BUTTON_HEIGHT + BUTTON_SPACING));
		quitButton.setWidth(BUTTON_WIDTH);
		quitButton.setHeight(BUTTON_HEIGHT);
		stage.addActor(quitButton);
		
		quitButton.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				setAction(ACTION_EXIT);
				setDone(true);
				return true;
	        }
		});
	}
	
	@Override
	public ScreenType getScreenType() {
		return ScreenType.PAUSE;
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public boolean backAllowed() {
		return true;
	}

	@Override
	public void setDone(boolean done) {
		this.done = done;
	}

}
