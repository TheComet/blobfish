package com.nerdinand.ouya.blobfish.screen.types;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.nerdinand.ouya.blobfish.Blobfish;
import com.nerdinand.ouya.blobfish.input.ControllerConnectionListener;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine.ScreenType;
import com.nerdinand.ouya.blobfish.screen.UIScreen;
import com.nerdinand.ouya.blobfish.simulation.GameSettings;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.util.SplitScreenHelper;

public class PlayerSelectionScreen extends UIScreen implements ControllerConnectionListener {
	private static final String TAG = "PlayerSelectionScreen";

	Map<LocalPlayer, TextButton> playerColorButtons = new HashMap<LocalPlayer, TextButton>();

	private Actor nextButton;

	private boolean done;

	private GameSettings gameSettings;

	public PlayerSelectionScreen(GameSettings gameSettings) {
		this.gameSettings = gameSettings;
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		updatePlayerLists();

		final float buttonX = (width - BUTTON_WIDTH) / 2;
		float currentY = 500f;

		Table table = new Table(getSkin());
		Collection<LocalPlayer> localPlayerList = Blobfish.getInputManager().getLocalPlayerList();

		for (final LocalPlayer localPlayer : localPlayerList) {
			Label playerLabel = new Label(localPlayer.toString(), getSkin(), "default-font", localPlayer.getColor());
			table.add(playerLabel).pad(10);
			TextButton changeColorButton = new TextButton("Change color", getSkin());
			playerColorButtons.put(localPlayer, changeColorButton);
			table.add(changeColorButton).pad(10);
			table.row();

			changeColorButton.addListener(new InputListener() {
				public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
					localPlayer.setColor(Player.nextColor(localPlayer.getColor()));
					refresh();
					stage.selectActor(playerColorButtons.get(localPlayer));
					return true;
				}
			});
		}

		table.setX(buttonX);
		table.setY(currentY -= (BUTTON_HEIGHT * localPlayerList.size()) + BUTTON_SPACING);
		table.setWidth(BUTTON_WIDTH);
		table.setHeight(BUTTON_HEIGHT * localPlayerList.size());
		stage.addActor(table);

		nextButton = new TextButton("Next", getSkin());
		nextButton.setX(buttonX);
		nextButton.setY((currentY -= BUTTON_HEIGHT + BUTTON_SPACING));
		nextButton.setWidth(BUTTON_WIDTH);
		nextButton.setHeight(BUTTON_HEIGHT);
		nextButton.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				setDone(true);
				return true;
			}
		});
		stage.addActor(nextButton);

		stage.selectActor(nextButton);
	}

	private void updatePlayerLists() {
		gameSettings.clearPlayerLists();

		Collection<LocalPlayer> localPlayerList = Blobfish.getInputManager().getLocalPlayerList();
		for (Player localPlayer : localPlayerList) {
			gameSettings.addPlayer(localPlayer);
		}

		// gameSettings.addPlayer(new TeleportingAIPlayer(new Color(MathUtils.random(), MathUtils.random(), MathUtils.random(), 1), gameSettings));
		// gameSettings.addPlayer(new TeleportingAIPlayer(new Color(MathUtils.random(), MathUtils.random(), MathUtils.random(), 1), gameSettings));

		SplitScreenHelper splitScreenHelper = new SplitScreenHelper(localPlayerList.size());
		int i = 0;
		for (LocalPlayer player : localPlayerList) {
			player.initSplitScreenCamera(splitScreenHelper.getDimension(i++));
		}
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.PLAYER_SELECTION;
	}

	@Override
	public void controllerConnected(Controller controller, LocalPlayer player) {
		refresh();
	}

	private void refresh() {
		resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}

	@Override
	public void controllerDisconnected(Controller controller, LocalPlayer player) {
		refresh();
	}

	@Override
	public boolean backAllowed() {
		return true;
	}

	@Override
	public void setDone(boolean done) {
		this.done = done;
	}
}
