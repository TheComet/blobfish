package com.nerdinand.ouya.blobfish.screen.types;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.nerdinand.ouya.blobfish.screen.ScreenStateMachine.ScreenType;
import com.nerdinand.ouya.blobfish.screen.UIScreen;

public class TitleScreen extends UIScreen {

	protected static final String TAG = "TitleScreen";

	private static final float BUTTON_WIDTH = 200f;
	private static final float BUTTON_HEIGHT = 50f;
	private static final float BUTTON_SPACING = 40f;
	
	private boolean done = false;
	private TextButton startGameButton;
	private TextButton creditsButton;
	private TextButton quitButton;

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		final float buttonX = (width - BUTTON_WIDTH) / 2;
		float currentY = 280f;

		Label welcomeLabel = new Label("Welcome to Blobfish!", getSkin());
		welcomeLabel.setX(((width - welcomeLabel.getWidth()) / 2));
		welcomeLabel.setY((currentY + 100));
		stage.addActor(welcomeLabel);

		startGameButton = new TextButton("Start game", getSkin());
		startGameButton.setX(buttonX);
		startGameButton.setY(currentY);
		startGameButton.setWidth(BUTTON_WIDTH);
		startGameButton.setHeight(BUTTON_HEIGHT);
		stage.addActor(startGameButton);

		stage.selectActor(startGameButton);
		
		startGameButton.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				setAction(ACTION_START_GAME);
                setDone(true);
				return true;
			}
		});

		creditsButton = new TextButton("Credits", getSkin());
		creditsButton.setX(buttonX);
		creditsButton.setY((currentY -= BUTTON_HEIGHT + BUTTON_SPACING));
		creditsButton.setWidth(BUTTON_WIDTH);
		creditsButton.setHeight(BUTTON_HEIGHT);
		stage.addActor(creditsButton);

		creditsButton.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				setAction(ACTION_CREDITS);
				setDone(true);
				return true;
	        }
		});
		
		quitButton = new TextButton("Quit", getSkin());
		quitButton.setX(buttonX);
		quitButton.setY((currentY -= BUTTON_HEIGHT + BUTTON_SPACING));
		quitButton.setWidth(BUTTON_WIDTH);
		quitButton.setHeight(BUTTON_HEIGHT);
		stage.addActor(quitButton);
		
		quitButton.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				setAction(ACTION_EXIT);
				setDone(true);
				return true;
	        }
		});
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.TITLE;
	}

	@Override
	public boolean backAllowed() {
		return false;
	}

	@Override
	public void setDone(boolean done) {
		this.done = done;
	}
}
