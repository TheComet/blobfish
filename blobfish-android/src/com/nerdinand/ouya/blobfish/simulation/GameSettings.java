package com.nerdinand.ouya.blobfish.simulation;

import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.Map;
import com.nerdinand.ouya.blobfish.map.MapPack;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;
import com.nerdinand.ouya.blobfish.simulation.player.Player;

public class GameSettings {
	private static final float DEFAULT_LOCK_TIME = 5f;
	private static final float DEFAULT_DURATION = 3 * 60;

	private MapPack mapPack;
	private Map map;
	private float duration;
	
	private Array<Player> playerList = new Array<Player>();
	private Array<LocalPlayer> localPlayerList = new Array<LocalPlayer>();

	private boolean powerUpsEnabled;
	private float lockTime;
	
	public GameSettings() {
		setDuration(DEFAULT_DURATION);
		setLockTime(DEFAULT_LOCK_TIME);
		setPowerUpsEnabled(true);
	}
	
	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public MapPack getMapPack() {
		return mapPack;
	}
	
	public void setMapPack(MapPack mapPack) {
		this.mapPack = mapPack;
	}
	
	public float getDuration() {
		return duration;
	}

	public void setDuration(float duration) {
		this.duration = duration;
	}
	
	public void clearPlayerLists(){
		playerList.clear();
		localPlayerList.clear();
	}
	
	public Array<? extends Player> getPlayerList() {
		return playerList;
	}

	public Array<LocalPlayer> getLocalPlayerList() {
		return localPlayerList;
	}

	public void addPlayer(Player player) {
		playerList.add(player);

		player.setDead(true);

		if (player instanceof LocalPlayer) {
			localPlayerList.add((LocalPlayer) player);
		}
	}

	public boolean isPowerUpsEnabled() {
		return powerUpsEnabled;
	}

	public void setPowerUpsEnabled(boolean powerUpsEnabled) {
		this.powerUpsEnabled = powerUpsEnabled;
	}

	public float getLockTime() {
		return lockTime;
	}

	public void setLockTime(float lockTime) {
		this.lockTime = lockTime;
	}
}
