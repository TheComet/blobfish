package com.nerdinand.ouya.blobfish.simulation;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.Map;
import com.nerdinand.ouya.blobfish.map.tile.ColourTile;
import com.nerdinand.ouya.blobfish.map.tile.TeleporterTile;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.Tile.TileType;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.AIPlayer;
import com.nerdinand.ouya.blobfish.simulation.player.LocalPlayer;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.player.TeleportingAIPlayer;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUpManager;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.JumpPowerUp;
import com.nerdinand.ouya.blobfish.util.Physics;

public class Simulation {

	private static final float KILL_HEIGHT = -200f;

	private static final float PREPARATION_TIME = 1f;

	private static final String TAG = "Simulation";
	private SimulationListener listener;

	private float accTime = 0;

	private PowerUpManager powerUpManager;
	private Physics physics;

	enum SimulationPhase {
		PREPARATION, GAME
	}

	private SimulationPhase currentPhase = SimulationPhase.PREPARATION;

	private GameSettings gameSettings;

	public Simulation(GameSettings gameSettings) {
		this.gameSettings = gameSettings;

		this.powerUpManager = new PowerUpManager(this);
		this.physics = new Physics(getMap().getTileField());
	}

	public Map getMap() {
		return getGameSettings().getMap();
	}

	public GameSettings getGameSettings() {
		return gameSettings;
	}

	public void setListener(SimulationListener listener) {
		this.listener = listener;
	}

	public void update(float deltaTime) {
		accTime += deltaTime; // add to total game time count

		if (currentPhase == SimulationPhase.GAME) { // game is running
			updateGame(deltaTime); // update the simulation

			if (accTime >= getGameSettings().getDuration()) { // check if game over
				stopGame();
				return;
			}

		} else { // game is not running
			if (accTime > PREPARATION_TIME) { // we are transitioning from PREPARATION to GAME phase
				currentPhase = SimulationPhase.GAME; // set the phase to running
				accTime = 0; // and reset the accTime, so the countdown is accurate

				for (Player player : getPlayerList()) { // set the speed of all players to the default
					player.setSpeed(Player.DEFAULT_SPEED);
				}

			} else { // we are still in the PREPARATION phase
				updateGame(deltaTime); // update the simulation normally

				for (Player player : getPlayerList()) { // but set speed for all players to 0
					player.setSpeed(0);
				}
			}
		}
	}

	public void stopGame() {
		listener.onGameOver();		
	}

	private void updateGame(float deltaTime) {
		if (gameSettings.isPowerUpsEnabled()){
			getPowerUpManager().update(deltaTime);
		}

		for (Array<Tile> tiles : getMap().getTileField().getTiles()) {
			for (Tile tile : tiles) {
				tile.update(deltaTime);
			}
		}

		Array<? extends Player> players = getPlayerList();

		for (int i = 0; i < players.size; i++) { // loop over all players (for some bizarre reason foreach doesn't seem to work here)
			Player player = players.get(i);

			if (player.isDead()) { // if player is dead
				respawn(player);

			} else { // if player isn't dead

				player.update(deltaTime);
				player.setMoveForce(deltaTime);

				player.setForce(physics.computeForce(player.getForce(), player.getVelocity(), deltaTime));
				player.setForce(physics.computeCollision(player, players, deltaTime));

				Tile tile = getGameSettings().getMap().getTileField().getTileAt(player.getPosition()); // find current tile of player

				final float playerHeight = player.getPosition().y;
				if (tile != null) { // there is a tile
					if (player.getLastInteractedTile() != tile) { // player has not already interacted with this tile
						if (playerHeight >= 0 && playerHeight < 5) { // only interact with the tile, if the player is not too much above or under the tile
							interact(tile, player); // perform interaction
							player.setLastInteractedTile(tile); // set interacted
						}
					}

					boolean trigger = player.getTrigger();

					if (trigger) { // trigger has been pressed
						triggerAction(player, tile);
						player.setTrigger(false);
					}
				}

				player.move(deltaTime);

				if (playerHeight <= KILL_HEIGHT) {
					player.setDead(true);
				}
			}
		}
	}

	private void respawn(Player player) {
		player.onRespawn();

		// player.setForce(0f, 0f, 0f);
		player.setVelocity(0.0f, 0.0f, 0.0f);

		Tile spawnTile = getGameSettings().getMap().getTileField().getSpawnTile();
		player.setPosition(spawnTile); // respawn
		player.setDead(false); // revive player

		if (player instanceof AIPlayer) {
			((AIPlayer) player).calculateNewPath(spawnTile);
		}
	}

	private void triggerAction(Player player, Tile tile) {
		if (player.hasPowerUp()) {
			boolean trigger = player.triggerPowerUp();

			if (trigger) {
				return;
			}
		}

		if (tile.getTileType() == TileType.TELEPORTER_TILE) {
			Tile teleportingAiTile = (((TeleporterTile) tile).getDestinationTile());
			player.setPosition(teleportingAiTile);
			if (player instanceof TeleportingAIPlayer) {
				((AIPlayer) player).calculateNewPath(teleportingAiTile);
			}
			return;
		}
	}

	public void interact(Tile tile, Player player) {
		if (tile.getTileType() == TileType.COLOUR_TILE) {
			ColourTile colourTile = (ColourTile) tile;
			Player oldOwner = colourTile.getOwner();
			Player newOwner = player.getOwner();

			if (!colourTile.isLocked()) {
				if (oldOwner != newOwner) { // tile was actually captured (and not
											// owned by the player before)
					captureTile(colourTile, newOwner);
				}
			}

		} else if (tile.getTileType() == TileType.JUMP_TILE) {
			Vector3 velocity = player.getVelocity();
			velocity.add(JumpPowerUp.JUMP_FORCE.cpy());
		}

		if (tile instanceof VisibleTile) {
			VisibleTile visibleTile = (VisibleTile) tile;

			if (visibleTile.hasPowerUp()) {
				getPowerUpManager().pickUp(player, visibleTile.getPowerUp());

				visibleTile.setPowerUp(null);
				listener.powerUpGet(visibleTile, player);
			}
		}
	}

	private void captureTile(ColourTile colourTile, Player newOwner) {
		Player oldOwner = colourTile.getOwner();

		if (oldOwner != null) {
			oldOwner.decreaseScore(); // decrease old owner's score
		}

		colourTile.setLocked();
		colourTile.setOwner(newOwner);
		newOwner.increaseScore(); // increase new owner's score

		if (listener != null) {
			listener.tileOwned(colourTile, newOwner);
		}
	}

	public Array<? extends Player> getPlayerList() {
		return getGameSettings().getPlayerList();
	}

	public Array<LocalPlayer> getLocalPlayerList() {
		return getGameSettings().getLocalPlayerList();
	}

	public int getSecondsLeft() {
		return (int) (getGameSettings().getDuration() - accTime);
	}

	public Player getWinner() {
		Array<? extends Player> playerList = getPlayerList();
		Player winner = playerList.first();

		for (Player player : playerList) {
			if (player.getScore() > winner.getScore()) {
				winner = player;
			}
		}

		return winner;
	}

	@Override
	public String toString() {
		return getPowerUpManager().toString();
	}
	
	public PowerUpManager getPowerUpManager() {
		return powerUpManager;
	}
}
