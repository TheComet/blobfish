package com.nerdinand.ouya.blobfish.simulation;

import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.simulation.player.Player;

public interface SimulationListener {
	// TODO methods for stuff, the GameScreen will want to know about, events in
	// the Simulation, e.g. audio needs to be played back

	void onGameOver();
	
	void tileOwned(Tile tile, Player player);
	void powerUpGet(Tile tile, Player player);
}
