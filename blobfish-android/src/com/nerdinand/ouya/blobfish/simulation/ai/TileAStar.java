package com.nerdinand.ouya.blobfish.simulation.ai;

import java.util.List;

import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.ColourTile;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.Tile.TileType;
import com.nerdinand.ouya.blobfish.simulation.player.AIPlayer;

public class TileAStar extends AStar<Tile> {

	private static final String TAG = "TileAStar";
	private TileField tileField;
	private Tile to;
	private AIPlayer aiPlayer;

	private TileAStar(TileField tileField) {
		this.tileField = tileField;
	}

	public TileAStar(TileField tileField, Tile to) {
		this(tileField);

		this.to = to;
	}

	public TileAStar(TileField tileField, AIPlayer player) {
		this(tileField);
		this.aiPlayer = player;
	}

	@Override
	protected boolean isGoal(Tile node) {
		boolean isGoal;

		if (to == null) {
			if (node.getTileType() == TileType.COLOUR_TILE) {
				ColourTile colourTile = (ColourTile) node;
				isGoal = !(colourTile.getOwner() == aiPlayer || colourTile.isLocked());
			} else {
				isGoal = false;
			}
		} else {
			isGoal = node == to;
		}

		return isGoal;
	}

	@Override
	protected Double g(Tile from, Tile to) {
		return (double) from.getgCost(to);
	}

	@Override
	protected Double h(Tile from, Tile to) {
		return (double) tileField.estimateDistance(from, to);
	}

	@Override
	protected List<Tile> generateSuccessors(Tile node) {
		return tileField.getAdjacentTiles(node);
	}

}
