package com.nerdinand.ouya.blobfish.simulation.player;

import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;
import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.simulation.GameSettings;
import com.nerdinand.ouya.blobfish.simulation.ai.PowerUpAStar;
import com.nerdinand.ouya.blobfish.simulation.ai.TileAStar;

public abstract class AIPlayer extends Player {

	private GameSettings gameSettings;
	protected List<Tile> path;

	private static int aiPlayerCount = 0;
	
	public AIPlayer(Color color, GameSettings gameSettings) {
		super("AIPlayer "+(++aiPlayerCount), color);
		this.gameSettings = gameSettings;
	}

	public GameSettings getGameSettings() {
		return gameSettings;
	}

	protected void moveTowards(Tile nextTile) {
		if (nextTile == null){
			return;
		}
		
		Vector3 currPosition = getPosition().cpy();
		currPosition.y = 0;
		Vector3 nextPosition = nextTile.getCenter().cpy();
		nextPosition.sub(currPosition);
		
		setDirection(nextPosition);
	}

	public void setPath(List<Tile> path) {
		this.path = path;
	}

	public void calculateNewPath(Tile currentTile) {
			setPath(getNewPath(currentTile));
	}

	public List<Tile> getNewPath(Tile fromTile) {
		TileAStar tileAStar = new TileAStar(getTileField(), this);
		List<Tile> tileList = tileAStar.compute(fromTile) ;
		if(tileList != null){
			return tileList;	
		} else {
			PowerUpAStar powerUpAStar = new PowerUpAStar(getTileField(), this);
			return powerUpAStar.compute(fromTile);
		}
	}

	protected TileField getTileField() {
		return getGameSettings().getMap().getTileField();
	}

}
