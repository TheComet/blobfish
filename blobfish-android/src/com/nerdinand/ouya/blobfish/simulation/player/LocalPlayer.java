package com.nerdinand.ouya.blobfish.simulation.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.nerdinand.ouya.blobfish.input.AnalogInputListener;
import com.nerdinand.ouya.blobfish.input.FunctionButtonsListener;
import com.nerdinand.ouya.blobfish.map.tile.Tile;

public class LocalPlayer extends Player implements AnalogInputListener, FunctionButtonsListener {
	private static final float AXIS_THRESHOLD = 0.3f;

	private static final String TAG = "Player";
	public PerspectiveCamera camera;

	private boolean isReady = false;

	private Controller controller;

	private Rectangle screen;

	private int tilesCullingBottom;

	private int tilesCullingRight;

	private boolean cullingCalculated;

	private int tilesCullingTop;

	private int tilesCullingLeft;

	private static int localPlayerCount = 0;

	// camera settings
	private static float cameraAngle = 62;
	private static float cameraNear = 1;
	private static float cameraFar = 300;

	private Vector3 newCameraPosition = new Vector3(0, 0, 0);
	private static Vector3 relativeCameraPosition = new Vector3(-30, 60, 0);
	private static float cameraForwardOffset = 6.0f;
	private static float cameraSmoothFactor = 12.0f;

	// smoothing function for the camera
	private Vector3 curvevalue(Vector3 newPosition, Vector3 currentPosition, float smoothFactor) {
		newPosition.sub(currentPosition);
		newPosition.div(smoothFactor);
		newPosition.add(currentPosition);
		return newPosition;
	}

	public LocalPlayer() {
		super("Player " + (++localPlayerCount));
	}

	public void initSplitScreenCamera(Rectangle screen) {
		this.screen = screen;
		
		PerspectiveCamera camera = new PerspectiveCamera(cameraAngle, screen.width, screen.height);
		camera.near = cameraNear;
		camera.far = cameraFar;

		setCamera(camera);

		camera.position.set(relativeCameraPosition);
		camera.lookAt(0, 0, 0);
		
		camera.update();

		if (getPosition() != null) {
			initCulling();
		}
	}

	private void initCulling() {
		tilesCullingTop = 8;
		tilesCullingBottom = 5;
		tilesCullingLeft = 11;
		tilesCullingRight = 11;

		Gdx.app.log(TAG, "Player culling initialized: top: " + tilesCullingTop + " left: " + tilesCullingLeft + " bottom: " + tilesCullingBottom + " right: " + tilesCullingRight);

		cullingCalculated = true;
	}

	public void setPosition(Vector3 position) {
		super.setPosition(position);

		// get the player's camera
		PerspectiveCamera camera = getCamera();

		// calculate new position of camera from player's position and add offset
		newCameraPosition = getPosition().cpy();
		//newCameraPosition.add(relativeCameraPosition).add(moveDirection.cpy().mul(cameraForwardOffset));
		newCameraPosition.add(relativeCameraPosition).add(getVelocity().cpy().mul(cameraForwardOffset));
		
		/*moveForce*/
		
		// smooth camera positions
		newCameraPosition = curvevalue(newCameraPosition.cpy(), camera.position.cpy(), cameraSmoothFactor);
		camera.position.set(newCameraPosition);

	}

	@Override
	public void setPosition(Tile tile) {
		super.setPosition(tile);

		// initialise culling : calculates frustum view
		if (!cullingCalculated) {
			initCulling();
		}
	}

	public int getTilesCullingLeft() {
		return tilesCullingLeft;
	}

	public int getTilesCullingTop() {
		return tilesCullingTop;
	}

	public int getTilesCullingBottom() {
		return tilesCullingBottom;
	}

	public int getTilesCullingRight() {
		return tilesCullingRight;
	}

	public Rectangle getScreen() {
		return screen;
	}

	public boolean isReady() {
		return isReady;
	}

	public void setReady(boolean isReady) {
		this.isReady = isReady;
	}

	private void setCamera(PerspectiveCamera camera) {
		this.camera = camera;
	}

	@Override
	public PerspectiveCamera getCamera() {
		return camera;
	}

	public Controller getController() {
		return controller;
	}

	@Override
	public void functionButtonDown(LocalPlayer player, FunctionButtonType functionType) {
		switch (functionType) {
		case SELECT:
			setTrigger(true);
			break;

		default:
			break;
		}
	}

	@Override
	public void functionButtonUp(LocalPlayer player, FunctionButtonType functionType) {
	}

	@Override
	public void controllerAxisMoved(LocalPlayer player, Controller controller, ControllerAxis axis, Vector2 value) {
		if (player == this) {
			if (axis == ControllerAxis.AXIS_LEFT) {
				Vector3 direction = axisDirectionToWorld(value);

				// only actually update direction, if the length of the
				// direction
				// vector is bigger than our threshold
				if (direction.len() > AXIS_THRESHOLD) {
					setDirection(direction);
				}
			}
		}
	}

	public static Vector3 axisDirectionToWorld(Vector2 axisDirection) {
		return new Vector3(-axisDirection.y, 0, axisDirection.x);
	}

	@Override
	public void update(float timeDelta) {
		super.update(timeDelta);
	}
}
