package com.nerdinand.ouya.blobfish.simulation.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.model.still.StillModel;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.Assets;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUp;

public class Player {

	public static final int DEFAULT_SPEED = 1;
	public static final float DEFAULT_COLLISION_SPHERE_SIZE = 1.0f;
	public static final float DEFAULT_RENDER_SPHERE_SIZE = 1.0f;

	public static final Array<Color> COLORS = new Array<Color>(new Color[] { Color.RED, Color.GREEN, Color.BLUE, Color.CYAN, Color.MAGENTA, Color.YELLOW });

	private static final Vector3 INITIAL_DIRECTION = new Vector3(1.0f, 0.0f, 1.0f);
	private static final Vector3 RELATIVE_SPAWN_POSITION = new Vector3(0f, 10f, 0f);

	private float speed = DEFAULT_SPEED;
	private float collisionSphereSize = DEFAULT_COLLISION_SPHERE_SIZE;
	private float renderSphereSize = DEFAULT_RENDER_SPHERE_SIZE;

	private static final String TAG = "Player";

	protected Vector3 moveForce;
	private Color color;
	public Vector3 position;

	private int score = 0;

	private boolean dead = true;
	public boolean isAffectedByBlackHole = false;
	private int deathCount = 0;

	private Tile lastInteractedTile;
	private boolean trigger;

	private PowerUp powerUp;
	private PowerUp triggeredPowerUp;

	private Player owner;

	private Vector3 oldPosition;

	// physics!
	private Vector3 force;
	private Vector3 collisionEllipsoid = new Vector3(10.0f, 10.0f, 10.0f);

	final private String name;
	private float inverseMass = 1.0f / 1f;
	private Vector3 velocity = new Vector3(0.0f, 0.0f, 0.0f);
	private Vector3 environmentForce = new Vector3(0.0f, 0.0f, 0.0f);

	public void setMass(float mass) {
		if (mass != 0.0f) {
			inverseMass = 0.1f / mass;
		}
	}
	
	public boolean getIsAffectedByBlackHole() {
		return isAffectedByBlackHole;
	}
	
	public void setIsAffectedByBlackHole(boolean isAffected) {
		this.isAffectedByBlackHole = isAffected;
	}

	public float getMass() {
		return 1.0f / inverseMass;
	}

	public void setVelocity(Vector3 velocity) {
		this.velocity = velocity;
	}

	public void setVelocity(float x, float y, float z) {
		setVelocity(new Vector3(x, y, z));
	}

	public Vector3 getVelocity() {
		return velocity;
	}

	// constructor
	public Player() {
		this("Player");
	}

	public Player(String name) {
		this(name, COLORS.random());
	}

	public Player(String name, Color color) {
		setDirection(INITIAL_DIRECTION);

		setColor(color);

		this.name = name;
	}

	public static Color nextColor(Color color) {
		int index = Player.COLORS.indexOf(color, true);
		index++;

		if (index >= Player.COLORS.size) {
			index = 0;
		}

		return Player.COLORS.get(index);
	}

	public void setPosition(Tile tile) {
		Vector3 position = tile.getCenter().cpy();
		position.add(RELATIVE_SPAWN_POSITION);

		setPosition(position);
	}

	// The playing person's input y-value will be set to 0 here, 
	// because the person should not be able to fly.
	public void setDirection(Vector3 direction) {
		moveForce = direction;
		moveForce.y = 0;
	}

	public Vector3 getDirection() {
		return moveForce;
	}

	public void setDirection(float x, float y, float z) {
		setDirection(new Vector3(x, y, z));
	}

	// Compute the velocity based on the input of the playing person.
	// Hack: Here the input should be added to the Force of a player but
	// this would cause the player to accelerate and break like a car and
	// this fells a bit stupid (you don't want to drift in a UFO;-)
	// Also, the environment-forces were added to the player, because the 
	// the input of the playing person come as fix sated values.
	// I'm afraid, this is just a huge hack.
	public void setMoveForce(float deltaTime) {
		if (getForce() == null) {
			setForce(0.0f, 0.0f, 0.0f);
		}
		moveForce.nor();
		moveForce.mul(speed);
		setVelocity(moveForce.x, getVelocity().y, moveForce.z);
		applyEnvironmentForce(deltaTime);
	}

	public void update(float deltaTime) {
		oldPosition = getPosition().cpy();
	}

	public Vector3 getOldPosition() {
		return oldPosition;
	}
	
	// Compute new position for the player by adding the actual velocity to the actual position.
	// Compute the acceleration based on the forces and the mass of the player.
	// Adding the acceleration to the actual velocity to get the resulting velocity.
	public void move(float deltaTime) {
		setPosition(getPosition().add((getVelocity())));

		Vector3 acc = getForce().mul(inverseMass);

		setVelocity(getVelocity().add((acc)));

		clearForce();
	}

	public void setPosition(float x, float y, float z) {
		this.position = new Vector3(x, y, z);
	}

	public void setPosition(Vector3 position) {
		this.position = position;
	}

	public Vector3 getPosition() {
		return position;
	}

	public Vector3 getForce() {
		return force;
	}

	public void setForce(Vector3 force) {
		this.force = force;
	}

	public void setForce(float x, float y, float z) {
		this.force = new Vector3(x, y, z);
	}

	public void addForce(Vector3 addForce) {
		force = force.add(addForce);
	}

	public void addForce(float x, float y, float z) {
		force = force.add(new Vector3(x, y, z));
	}

	public void clearForce() {
		setForce(0.0f, 0.0f, 0.0f);
		setEnvironmentForce(new Vector3(0.0f, 0.0f, 0.0f));
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	public PerspectiveCamera getCamera() {
		return null;
	}

	public int getScore() {
		return score;
	}

	public void increaseScore() {
		score++;
	}

	public void decreaseScore() {
		score--;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;

		if (dead) {
			deathCount++;
		}
	}

	public int getDeathCount() {
		return deathCount;
	}

	public Vector3 getMoveDirection() {
		return moveForce;
	}

	public void setMoveDirection(Vector3 moveDirection) {
		this.moveForce = moveDirection;
	}

	public void setLastInteractedTile(Tile lastInteractedTile) {
		this.lastInteractedTile = lastInteractedTile;
	}

	public Tile getLastInteractedTile() {
		return lastInteractedTile;
	}

	public void setTrigger(boolean trigger) {
		this.trigger = trigger;
	}

	public boolean getTrigger() {
		return trigger;
	}

	public void setPowerUp(PowerUp powerUp) {
		this.powerUp = powerUp;
	}

	public PowerUp getPowerUp() {
		return powerUp;
	}

	public boolean hasPowerUp() {
		return getPowerUp() != null;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public float getSpeed() {
		return speed;
	}

	public boolean triggerPowerUp() {
		boolean triggered;
		if (hasPowerUp()) {
			PowerUp currentPowerUp = getPowerUp();
			triggered = currentPowerUp.trigger();
			triggeredPowerUp = currentPowerUp;

			setPowerUp(null);
		} else {
			triggered = false;
		}
		return triggered;
	}

	public void setTriggeredPowerUp(PowerUp triggeredPowerUp) {
		this.triggeredPowerUp = triggeredPowerUp;
	}

	public void onRespawn() {
		if (hasPowerUp()) {
			getPowerUp().setOwner(null);
		}
		setPowerUp(null);

		if (hasTriggeredPowerup()) {
			getTriggeredPowerUp().done();
		}
	}

	public PowerUp getTriggeredPowerUp() {
		return triggeredPowerUp;
	}

	public boolean hasTriggeredPowerup() {
		return getTriggeredPowerUp() != null;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public Player getOwner() {
		return (owner == null) ? this : owner;
	}

	// returns the ellipsoid of the player for collision purposes
	public Vector3 getCollisionEllipsoid() {
		return collisionEllipsoid;
	}

	@Override
	public String toString() {
		return name;
	}

	public Vector3 getEnvironmentForce() {
		return environmentForce;
	}

	public void setEnvironmentForce(float x, float y, float z) {
		setEnvironmentForce(new Vector3(x, y, z));
	}

	public void setEnvironmentForce(Vector3 environmentForce) {
		this.environmentForce = environmentForce;
	}

	public void applyEnvironmentForce(float deltaTime) {
		getVelocity().add(getEnvironmentForce().cpy().mul(deltaTime));
	}

	public boolean hasTriggeredPowerUp() {
		return getTriggeredPowerUp() != null;
	}

	public float getCollisionSphereSize() {
		return collisionSphereSize;
	}

	public void setCollisionSphereSize(float collisionSphereSize) {
		this.collisionSphereSize = collisionSphereSize;
	}

	public float getRenderSphereSize() {
		return renderSphereSize;
	}

	public void setRenderSphereSize(float renderSphereSize) {
		this.renderSphereSize = renderSphereSize;
	}
}