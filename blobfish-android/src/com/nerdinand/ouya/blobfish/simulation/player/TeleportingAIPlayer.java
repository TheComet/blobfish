package com.nerdinand.ouya.blobfish.simulation.player;

import com.badlogic.gdx.graphics.Color;
import com.nerdinand.ouya.blobfish.map.tile.TeleporterTile;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.Tile.TileType;
import com.nerdinand.ouya.blobfish.simulation.GameSettings;

public class TeleportingAIPlayer extends AIPlayer {
	private static final String TAG = "TeleportingAIPlayer";
	private Tile nextTile;

	public TeleportingAIPlayer(Color color, GameSettings gameSettings) {
		super(color, gameSettings);
	}

	@Override
	public void update(float deltaTime) {
		super.update(deltaTime);
		Tile currentTile = getTileField().getTileAt(getPosition());

//		Gdx.app.debug(TAG, "current: " + currentTile);
		
		if (path == null || path.size() == 0) {
			path = getNewPath(currentTile);
			if (path == null) {
				moveTowards(currentTile);
				return;
			} else {
				path.remove(0); // first tile is always the start tile, we're already there...
			}
		}
		
		if (nextTile == null){
			nextTile = path.remove(0);
			
		} else if (currentTile == nextTile) {
			if (path == null){
				path = getNewPath(currentTile);
			}
			
			nextTile = path.remove(0);
			
			if (currentTile.getTileType() == TileType.TELEPORTER_TILE) {
				if (((TeleporterTile) currentTile).getDestinationTile() == nextTile) {
					setTrigger(true);
					
					if (path.size() > 0){
						nextTile = path.remove(0);
					} else {
						moveTowards(currentTile);
					}
				}
			}
		}
		
		if(hasPowerUp()) {
			setTrigger(true);
		}
		
		moveTowards(nextTile);
		
//		Gdx.app.debug(TAG, "next: " + nextTile);
	}
}
