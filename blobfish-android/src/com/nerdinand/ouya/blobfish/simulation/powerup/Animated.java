package com.nerdinand.ouya.blobfish.simulation.powerup;

import com.badlogic.gdx.graphics.g3d.model.keyframe.KeyframedModel;

public interface Animated {
	public KeyframedModel getModel();
	
	public void updateAnimation(float deltaTime);
	public boolean doesAnimate();
}
