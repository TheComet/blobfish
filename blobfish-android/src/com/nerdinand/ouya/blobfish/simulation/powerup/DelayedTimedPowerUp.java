package com.nerdinand.ouya.blobfish.simulation.powerup;

import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUpState.State;

public abstract class DelayedTimedPowerUp extends TimedPowerUp {

	private float delay;
	private TileField tileField;
	private Tile triggerTile;
	private float timePast;
	private boolean delayedTriggered = false;

	public DelayedTimedPowerUp(VisibleTile tile, float duration, float delay, TileField tileField) {
		super(tile, duration);
		this.delay = delay;
		this.setTileField(tileField);
	}
	
	@Override
	public boolean trigger() {
		boolean trigger = super.trigger();
		if (trigger) {
			triggerTile = getTileField().getTileAt(getOwner().getPosition());
		}
		return trigger;
	}
	
	@Override
	public void update(float deltaTime) {
		super.update(deltaTime);
		if (getState().getState() == State.TRIGGERED) {
			timePast += deltaTime;
			if (timePast > delay && delayedTriggered == false) {
				delayedTrigger(triggerTile);
				delayedTriggered = true;
			}
		}
	}
	
	public boolean isDelayedTriggered() {
		return delayedTriggered;
	}
	
	public Tile getTriggerTile() {
		return triggerTile;
	}

	public abstract void delayedTrigger(Tile triggerTile);

	public TileField getTileField() {
		return tileField;
	}

	public void setTileField(TileField tileField) {
		this.tileField = tileField;
	}
}
