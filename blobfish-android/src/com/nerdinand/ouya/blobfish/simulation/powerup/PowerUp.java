package com.nerdinand.ouya.blobfish.simulation.powerup;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.math.Vector3;
import com.nerdinand.ouya.blobfish.Assets;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUpState.State;

public abstract class PowerUp {
	
	public enum PowerUpType {
		SPEED_BOOST_POWER_UP,
		SLOW_DOWN_POWER_UP,
		TILE_STEALING_POWER_UP,
		TILE_KILLER_POWER_UP,
		JUMP_POWER_UP,
		BLACK_HOLE_POWER_UP,
		PLAYER_SHOCK_WAVE_POWER_UP,
		FORCE_FIELD_POWER_UP,
		DRUNK_POWER_UP,
		BUMP_POWER_UP,
		SIZE_BOOST_POWER_UP,
		TILE_SHOCK_WAVE_POWER_UP
	}
	
	public static final float TIME_TO_LIVE = 5;

	private VisibleTile spawnTile;
	private float timeToLive;

	private Player owner;

	private PowerUpState state = new PowerUpState();

	private float animationTime = 0;

	private Vector3 triggerPosition;

	private final static HashMap<PowerUpType, String> typeIconMap;
	
	public PowerUp(VisibleTile tile) {
		spawnTile = tile;
		tile.setPowerUp(this);

		timeToLive = TIME_TO_LIVE;
	}
	
	static {
		typeIconMap = new HashMap<PowerUp.PowerUpType, String>();
		typeIconMap.put(PowerUpType.SPEED_BOOST_POWER_UP, Assets.TEXTURE_POWERUP_ICON_SPEED_BOOST);
		typeIconMap.put(PowerUpType.SLOW_DOWN_POWER_UP, Assets.TEXTURE_POWERUP_ICON_SLOW_DOWN);
		typeIconMap.put(PowerUpType.TILE_STEALING_POWER_UP, Assets.TEXTURE_POWERUP_ICON_TILE_STEALING);
		typeIconMap.put(PowerUpType.TILE_KILLER_POWER_UP, Assets.TEXTURE_POWERUP_ICON_TILE_KILLER);
		typeIconMap.put(PowerUpType.JUMP_POWER_UP, Assets.TEXTURE_POWERUP_ICON_JUMP);
		typeIconMap.put(PowerUpType.BLACK_HOLE_POWER_UP, Assets.TEXTURE_POWERUP_ICON_BLACK_HOLE);
		typeIconMap.put(PowerUpType.PLAYER_SHOCK_WAVE_POWER_UP, Assets.TEXTURE_POWERUP_ICON_PLAYER_SHOCK_WAVE);
		typeIconMap.put(PowerUpType.FORCE_FIELD_POWER_UP, Assets.TEXTURE_POWERUP_ICON_FORCE_FIELD);
		typeIconMap.put(PowerUpType.DRUNK_POWER_UP, Assets.TEXTURE_POWERUP_ICON_DRUNK);
		typeIconMap.put(PowerUpType.BUMP_POWER_UP, Assets.TEXTURE_POWERUP_ICON_BUMP);
		typeIconMap.put(PowerUpType.SIZE_BOOST_POWER_UP, Assets.TEXTURE_POWERUP_ICON_SIZE_BOOST);
		typeIconMap.put(PowerUpType.TILE_SHOCK_WAVE_POWER_UP, Assets.TEXTURE_POWERUP_ICON_TILE_SHOCK_WAVE);
	}
	
	private static HashMap<PowerUpType, String> getTypeIconMap() {
		return typeIconMap;
	}
	
	public abstract PowerUpType getPowerUpType(); 

	public TextureRegion getIconTextureRegion(){
		return Assets.getRegion(Assets.TEXTURE_ATLAS_GAME, PowerUp.getTypeIconMap().get(getPowerUpType()));
	}
	
	public PowerUpState getState() {
		return state;
	}

	public VisibleTile getSpawnTile() {
		return spawnTile;
	}

	public float getTimeToLive() {
		return timeToLive;
	}

	public void update(float deltaTime) {
		if (getState().getState() == State.SPAWNED) {
			timeToLive -= deltaTime;

			if (timeToLive <= 0) {
				getState().die();
			}

			animationTime += deltaTime;
			Animation powerUpAnimation = Assets.powerUpAnimation;
			if (powerUpAnimation != null) {
				if (animationTime >= powerUpAnimation.totalDuration) {
					animationTime = 0;
				}
			}
		}
		
		if (this instanceof Animated){
			((Animated) this).updateAnimation(deltaTime);
		}
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}

	public void setOwner(Player player) {
		this.owner = player;

		if (player == null) { // this means that this powerup is losing its
								// owner, being replaced or the player has died.
								// either way, the powerup should die too.

			getState().replaced();

		} else {
			player.setPowerUp(this);

			getState().pickUp();
		}
	}

	public Player getOwner() {
		return owner;
	}

	public boolean trigger() {
		setTriggerPosition(getOwner().getPosition().cpy());
		return getState().trigger();
	}

	public void done() {
		if (state.done()) {
			getOwner().setTriggeredPowerUp(null);
			this.setOwner(null);
		}
	}

	public float getAnimationTime() {
		return animationTime;
	}
	
	public Vector3 getTriggerPosition() {
		return triggerPosition;
	}

	public void setTriggerPosition(Vector3 triggerPosition) {
		this.triggerPosition = triggerPosition;
	}
}
