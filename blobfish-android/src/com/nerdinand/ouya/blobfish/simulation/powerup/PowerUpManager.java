package com.nerdinand.ouya.blobfish.simulation.powerup;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.Simulation;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUp.PowerUpType;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUpState.State;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.BlackHolePowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.BumpPowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.DrunkPowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.ForceFieldPowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.JumpPowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.PlayerShockWavePowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.SizeBoostPowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.SlowDownPowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.SpeedBoostPowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.TileKillerPowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.TileShockWavePowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.types.TileStealingPowerup;

public class PowerUpManager {

	private static final String TAG = "PowerUpManager";

	private Array<PowerUp> mapPowerUps = new Array<PowerUp>();
	private Array<PowerUp> playerPowerUps = new Array<PowerUp>();

	private Simulation simulation;
	
	public PowerUpManager(Simulation simulation) {
		this.simulation = simulation;
	}

	public boolean spawnsPowerUp(float deltaTime) {
		float random = MathUtils.random();

		//SPAWNRATE
		float powerUpsPerSecond = (simulation.getMap().getPowerUpsPerMinute() / 60f) * deltaTime;

		return random < powerUpsPerSecond;
	}

	public void update(float deltaTime) {
		if (spawnsPowerUp(deltaTime)) {
			PowerUp newPowerUp = spawnPowerUp();
			mapPowerUps.add(newPowerUp);

			Gdx.app.log(TAG, "Powerup spawns: " + newPowerUp + " on tile " + newPowerUp.getSpawnTile());
		}

		for (PowerUp powerUp : mapPowerUps) {
			powerUp.update(deltaTime);

			if (powerUp.getState().getState() == State.DEAD) {
				Gdx.app.log(TAG, "Powerup despawns: " + powerUp + " on tile " + powerUp.getSpawnTile());
				despawnPowerUp(powerUp);
			}
		}

		for (PowerUp powerUp : playerPowerUps) {
			powerUp.update(deltaTime);
			
			if (powerUp.getState().getState() == State.DEAD){
				playerPowerUps.removeValue(powerUp, true);
			}
		}
	}

	private void despawnPowerUp(PowerUp powerUp) {
		powerUp.getState().die();
		
		powerUp.getSpawnTile().setPowerUp(null);
		mapPowerUps.removeValue(powerUp, true);
	}

	private PowerUp spawnPowerUp() {
		PowerUpType[] values = PowerUpType.values();
		int random = MathUtils.random(values.length - 1);

		VisibleTile tile;
		do {
			tile = simulation.getMap().getTileField().getPowerUpTile();
		} while (tile.hasPowerUp());

		PowerUpType type = values[random];
		return getNewPowerUp(type, tile);
//		return getNewPowerUp(PowerUpType.SIZE_BOOST_POWER_UP, tile);
	}

	private PowerUp getNewPowerUp(PowerUpType type, VisibleTile tile) {
		switch (type) {
		case SPEED_BOOST_POWER_UP:
			return new SpeedBoostPowerUp(tile);

		case SLOW_DOWN_POWER_UP:
			return new SlowDownPowerUp(tile, simulation.getPlayerList());
			
		case TILE_STEALING_POWER_UP:
			return new TileStealingPowerup(tile, simulation.getPlayerList());
			
		case JUMP_POWER_UP:
			return new JumpPowerUp(tile);
			
		case PLAYER_SHOCK_WAVE_POWER_UP:
			return new PlayerShockWavePowerUp(tile, simulation.getPlayerList());
		
		case BLACK_HOLE_POWER_UP:
			return new BlackHolePowerUp(tile, simulation.getPlayerList(), simulation.getMap().getTileField());
			
		case FORCE_FIELD_POWER_UP:
			return new ForceFieldPowerUp(tile, simulation.getPlayerList(), simulation.getMap().getTileField());
		
		case DRUNK_POWER_UP:
			return new DrunkPowerUp(tile, simulation.getPlayerList(), simulation.getMap().getTileField());
			
		case TILE_KILLER_POWER_UP:
			return new TileKillerPowerUp(tile, simulation.getMap().getTileField());
			
		case BUMP_POWER_UP:
			return new BumpPowerUp(tile);
			
		case SIZE_BOOST_POWER_UP:
			return new SizeBoostPowerUp(tile);
		
		case TILE_SHOCK_WAVE_POWER_UP:
			return new TileShockWavePowerUp(tile, simulation.getMap().getTileField());
			
		default:
			break;
		}

		return null;
	}

	public void pickUp(Player player, PowerUp powerUp) {
		if (player.hasPowerUp()){
			PowerUp oldPowerUp = player.getPowerUp();
			oldPowerUp.getState().replaced();
			
			playerPowerUps.removeValue(oldPowerUp, true);
		}
		
		powerUp.getState().pickUp();
		
		player.setPowerUp(powerUp);
		powerUp.setOwner(player);
		
		playerPowerUps.add(powerUp);
		mapPowerUps.removeValue(powerUp, true);
	}
	
	public Array<PowerUp> getPlayerPowerUps() {
		return playerPowerUps;
	}

	@Override
	public String toString() {
		return "PowerUps on map: "+mapPowerUps.size+" PowerUps in player posession: "+playerPowerUps.size;
	}

	public Array<PowerUp> getMapPowerUps() {
		return mapPowerUps;
	}
}
