package com.nerdinand.ouya.blobfish.simulation.powerup;

public class PowerUpState {
	public enum State {
		SPAWNED, PICKED_UP, TRIGGERED, DEAD
	}

	private State state;

	public PowerUpState() {
		state = State.SPAWNED;
	}

	public State getState() {
		return state;
	}

	public boolean pickUp() {
		if (state == State.SPAWNED) {
			state = State.PICKED_UP;

			return true;
		}
		return false;
	}

	public boolean trigger() {
		if (state == State.PICKED_UP) {
			state = State.TRIGGERED;

			return true;
		}
		return false;
	}

	public boolean done() {
		if (state == State.TRIGGERED) {
			state = State.DEAD;

			return true;
		}
		return false;
	}

	public boolean die() {
		if (state == State.SPAWNED) {
			state = State.DEAD;

			return true;
		}
		return false;
	}

	public boolean replaced() {
		if (state == State.PICKED_UP) {
			state = State.DEAD;

			return true;
		}
		return false;
	}
}
