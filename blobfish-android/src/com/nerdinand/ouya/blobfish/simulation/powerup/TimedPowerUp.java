package com.nerdinand.ouya.blobfish.simulation.powerup;

import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUpState.State;

public abstract class TimedPowerUp extends PowerUp {

	private float duration;

	public TimedPowerUp(VisibleTile tile, float duration) {
		super(tile);
		this.duration = duration;
	}

	@Override
	public void update(float deltaTime) {
		if (getState().getState() == State.TRIGGERED) {
			duration -= deltaTime;
			if (duration <= 0) {
				done();
			}
		}
		super.update(deltaTime);
	}
}
