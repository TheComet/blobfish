package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.graphics.g3d.model.keyframe.KeyframedModel;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.Assets;
import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.Animated;
import com.nerdinand.ouya.blobfish.simulation.powerup.DelayedTimedPowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUpState.State;

public class BlackHolePowerUp extends DelayedTimedPowerUp implements Animated {

	private static final float RADIUS = 50f;
	private static final float FORCE_STRENGTH = 100f;
	public static final float DURATION = 5;
	public static final float DELAY = 2;
	
	private Array<? extends Player> players;
	
	private float triggeredAnimationTime;
	
	public BlackHolePowerUp(VisibleTile tile, Array<? extends Player> array, TileField tileField) {
		super(tile, DURATION, DELAY, tileField);
		this.players = array;
	}

	@Override
	public void update(float deltaTime) {
		if(getState().getState() == State.TRIGGERED && isDelayedTriggered()) {
			setForce(deltaTime);
		}
		super.update(deltaTime);
	}

	private void setForce(float deltaTime) {
		for (Player player : players) {
			Vector3 direction = getTriggerTile().getPosition().cpy().sub(player.getPosition());
			direction.y = 0;
			float distance = direction.len();
			if(distance < RADIUS) {
				float forceStrength = - distance + FORCE_STRENGTH;
				direction.nor();
				player.getEnvironmentForce().add((direction.x * forceStrength), 0, (direction.z * forceStrength));
				player.setIsAffectedByBlackHole(true);
			} else {
				player.setIsAffectedByBlackHole(false);
			}
		}
	}

	@Override
	public void delayedTrigger(Tile triggerTile) {		
	}

	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.BLACK_HOLE_POWER_UP;
	}

	@Override
	public KeyframedModel getModel() {
		return Assets.blackHoleModel;
	}
	
	@Override
	public void updateAnimation(float deltaTime) {
		triggeredAnimationTime += deltaTime;
		Animation powerUpAnimation = Assets.blackHoleAnimation;
		if (powerUpAnimation != null) {
			if (triggeredAnimationTime >= powerUpAnimation.totalDuration) {
				triggeredAnimationTime = 0;
			}
		}
		
		getModel().setAnimation(powerUpAnimation.name, triggeredAnimationTime, false);
	}

	@Override
	public boolean doesAnimate() {
		return getState().getState() == State.TRIGGERED && isDelayedTriggered();
	}	
}
