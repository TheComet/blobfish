package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.TimedPowerUp;

public class BumpPowerUp extends TimedPowerUp {

	public static final float DURATION = 2;
	public static final float SPEED_MULTIPLIER = 3;

	public BumpPowerUp(VisibleTile tile) {
		super(tile, DURATION);
	}

	@Override
	public boolean trigger() {
		boolean trigger = super.trigger();

		if (trigger) {
			getOwner().setSpeed(Player.DEFAULT_SPEED * SPEED_MULTIPLIER);
		}

		return trigger;
	}

	@Override
	public void done() {
		getOwner().setSpeed(Player.DEFAULT_SPEED);

		super.done();
	}
	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.BUMP_POWER_UP;
	}

}
