package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUpState.State;
import com.nerdinand.ouya.blobfish.simulation.powerup.TimedPowerUp;

public class DrunkPowerUp extends TimedPowerUp {

	private static final float RADIUS = 100f;
	private static final float FORCE_STRENGTH = 3;
	private static final float DURATION = 10;

	private static final float ROTATION_SPEED = 45;

	private Array<? extends Player> players;
	private final Vector3 drunkDirection;

	public DrunkPowerUp(VisibleTile tile, Array<? extends Player> array, TileField tileField) {
		super(tile, DURATION);
		this.players = array;

		drunkDirection = new Vector3();
		drunkDirection.x = MathUtils.random(-1f, 1f);
		drunkDirection.z = MathUtils.random(-1f, 1f);
		drunkDirection.nor();
	}

	@Override
	public void update(float deltaTime) {
		if (getState().getState() == State.TRIGGERED) {
			updateDrunkDirection(deltaTime);
			setForce(deltaTime);
		}
		super.update(deltaTime);
	}

	private void updateDrunkDirection(float deltaTime) {
		float rotationDegrees = ROTATION_SPEED * deltaTime;

		drunkDirection.rotate(Vector3.Y, rotationDegrees);
	}

	private void setForce(float deltaTime) {
		for (Player player : players) {
			if (player != getOwner()) {
				float vecDivLen = new Vector3(player.getPosition().cpy()).sub(getTriggerPosition()).len();

				if (vecDivLen < RADIUS) {
					player.getDirection().add(drunkDirection.cpy().mul(FORCE_STRENGTH * deltaTime));
				}
			}
		}
	}

	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.DRUNK_POWER_UP;
	}
}
