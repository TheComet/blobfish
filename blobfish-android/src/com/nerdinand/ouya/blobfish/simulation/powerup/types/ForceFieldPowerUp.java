package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUpState.State;
import com.nerdinand.ouya.blobfish.simulation.powerup.TimedPowerUp;

public class ForceFieldPowerUp extends TimedPowerUp {

	private static final float RADIUS = 70f;
	private static final float FORCE_STRENGTH = 100f;
	public static final float DURATION = 10;

	private Array<? extends Player> players;

	public ForceFieldPowerUp(VisibleTile tile, Array<? extends Player> array, TileField tileField) {
		super(tile, DURATION);
		this.players = array;
	}

	@Override
	public void update(float deltaTime) {
		if (getState().getState() == State.TRIGGERED) {  // if powerup is already triggered
			setForce(deltaTime); // apply forces to opponents
		}
		super.update(deltaTime);
	}

	private void setForce(float deltaTime) {
		for (Player player : players) {  // for each player
			if (player != getOwner()) { // if it is an opponent
				
				Vector3 direction = new Vector3(player.getPosition().cpy().sub(getTriggerPosition())); 
				direction.y = 0;
				
				float distance = direction.len();
				
				if (distance < RADIUS) { // only apply force if opponent inside radius
					float forceStrength = -distance + FORCE_STRENGTH;
					direction.nor();
					Vector3 additionalForce = new Vector3(direction.x * forceStrength, 0, direction.z * forceStrength);
					player.getEnvironmentForce().add(additionalForce);
				}
			}
		}
	}
	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.FORCE_FIELD_POWER_UP;
	}

}
