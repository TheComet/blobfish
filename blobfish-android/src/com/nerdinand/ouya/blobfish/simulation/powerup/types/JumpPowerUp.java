package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import com.badlogic.gdx.math.Vector3;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUp;

public class JumpPowerUp extends PowerUp {

	public static final Vector3 JUMP_FORCE = new Vector3(0, 3f, 0);

	public JumpPowerUp(VisibleTile tile) {
		super(tile);
	}

	@Override
	public boolean trigger() {
		boolean trigger = super.trigger();

		if (trigger){
			Vector3 velocity = getOwner().getVelocity();
			velocity.add(JUMP_FORCE.cpy());			
		}
		
		return trigger;
	}
	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.JUMP_POWER_UP;
	}
}
