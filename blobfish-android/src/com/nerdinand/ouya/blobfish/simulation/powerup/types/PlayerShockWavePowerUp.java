package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUp;

public class PlayerShockWavePowerUp extends PowerUp {

	private static final float RADIUS = 80f;
	private static final float FORCESTRENGHT = 1500f;

	private Array<? extends Player> players;

	public PlayerShockWavePowerUp(VisibleTile tile, Array<? extends Player> array) {
		super(tile);
		this.players = array;
	}

	@Override
	public boolean trigger() {
		setForce();
		return super.trigger();
	}

	private void setForce() {
		for (Player player : players) {
			if (player != getOwner()) {
				Vector3 vecDiv = new Vector3((player.getPosition().cpy()).sub(getOwner().getPosition()));
				vecDiv.y = 0;
				float vecDivLen = vecDiv.len();
				if (vecDivLen < RADIUS) {
					float forceLenght = -vecDivLen + FORCESTRENGHT;
					player.getEnvironmentForce().add((vecDiv.nor().x * forceLenght), 0, (vecDiv.nor().z * forceLenght));
				}
			}
		}
	}

	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.PLAYER_SHOCK_WAVE_POWER_UP;
	}
}
