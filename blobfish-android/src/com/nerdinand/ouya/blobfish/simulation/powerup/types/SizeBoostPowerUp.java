package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.TimedPowerUp;

public class SizeBoostPowerUp extends TimedPowerUp {

	public static final float DURATION = 5f;
	public static final float SIZE_MULTIPLIER = 2.0f;

	public SizeBoostPowerUp(VisibleTile tile) {
		super(tile, DURATION);
	}

	@Override
	public boolean trigger() {
		boolean trigger = super.trigger();

		if (trigger) {
			getOwner().setCollisionSphereSize(getOwner().getCollisionSphereSize()*(SIZE_MULTIPLIER));
			getOwner().setRenderSphereSize(getOwner().getRenderSphereSize()*(SIZE_MULTIPLIER));
		}
		return trigger;
	}

	@Override
	public void done() {
			getOwner().setCollisionSphereSize(Player.DEFAULT_COLLISION_SPHERE_SIZE);
			getOwner().setRenderSphereSize(Player.DEFAULT_RENDER_SPHERE_SIZE);
		super.done();
	}
	
	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.SIZE_BOOST_POWER_UP;
	}
}
