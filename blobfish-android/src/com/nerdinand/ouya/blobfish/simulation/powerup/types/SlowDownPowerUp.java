package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.TimedPowerUp;

public class SlowDownPowerUp extends TimedPowerUp {

	public static final float DURATION = 5;
	public static final float SPEED_MULTIPLIER = 0.5f;
	private Array<? extends Player> players;
	
	public SlowDownPowerUp(VisibleTile tile, Array<? extends Player> array) {
		super(tile, DURATION);
		
		this.players = array;
	}

	@Override
	public boolean trigger() {
		boolean trigger = super.trigger();

		if (trigger){
			setSpeed(Player.DEFAULT_SPEED * SPEED_MULTIPLIER);
		}
		
		return trigger;
	}

	private void setSpeed(float speed) {
		for (Player player : players) {
			if(player != getOwner()){
				player.setSpeed(speed);
			}
		}
	}

	@Override
	public void done() {
		setSpeed(Player.DEFAULT_SPEED);
		
		super.done();
	}
	
	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.SLOW_DOWN_POWER_UP;
	}
}
