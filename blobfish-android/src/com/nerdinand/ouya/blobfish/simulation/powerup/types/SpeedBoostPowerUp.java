package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.TimedPowerUp;

public class SpeedBoostPowerUp extends TimedPowerUp {

	public static final float DURATION = 5;
	public static final float SPEED_MULTIPLIER = 2;

	public SpeedBoostPowerUp(VisibleTile tile) {
		super(tile, DURATION);
	}

	@Override
	public boolean trigger() {
		boolean trigger = super.trigger();

		if (trigger) {
			getOwner().setSpeed(Player.DEFAULT_SPEED * SPEED_MULTIPLIER);
		}

		return trigger;
	}

	@Override
	public void done() {
		getOwner().setSpeed(Player.DEFAULT_SPEED);

		super.done();
	}
	
	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.SPEED_BOOST_POWER_UP;
	}
}
