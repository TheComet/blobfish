package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import java.util.List;

import com.nerdinand.ouya.blobfish.map.MapFormatException;
import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.powerup.DelayedTimedPowerUp;

public class TileKillerPowerUp extends DelayedTimedPowerUp {

	public static final float DELAY = 2;
	public static final float DURATION = 7;
	
	private List<Tile> killedTiles;

	public TileKillerPowerUp(VisibleTile tile, TileField tileField) {
		super(tile, DURATION, DELAY, tileField);
	}

	@Override
	public void done() {
		if (getKilledTiles() != null){ // the powerup may not even have been triggered
			getTileField().revive(getKilledTiles());
		}
		super.done();
	}

	@Override
	public void delayedTrigger(Tile triggerTile) {
		try {
			setKilledTiles(getTileField().killTiles(triggerTile));
		} catch (MapFormatException e) { // if there is an error reinitializing the map (should not happen)
			this.done(); // finish the powerup
		}
	}
	
	private void setKilledTiles(List<Tile> killTiles) {
		this.killedTiles = killTiles;
		
	}

	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.TILE_KILLER_POWER_UP;
	}

	public List<Tile> getKilledTiles() {
		return killedTiles;
	}
}
