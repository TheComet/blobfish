package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import java.util.List;

import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.ColourTile;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.Tile.TileType;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUp;

public class TileShockWavePowerUp extends PowerUp {

	private TileField tileField;

	public TileShockWavePowerUp(VisibleTile tile, TileField tileField) {
		super(tile);
		this.tileField = tileField;
	}

	@Override
	public boolean trigger() {
		boolean trigger = super.trigger();

		if (trigger) {
			List<Tile> tileList = tileField.getNeighbourTiles(tileField.getTileAt(getTriggerPosition()), true);
			if (tileList != null) {
				for (Tile tileToGet : tileList) {
					if (tileToGet.getTileType() == TileType.COLOUR_TILE) {
						((ColourTile) tileToGet).setOwner(getOwner().getOwner());
					}
				}
			}
		}
		
		return trigger;
	}
	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.TILE_SHOCK_WAVE_POWER_UP;
	}
}
