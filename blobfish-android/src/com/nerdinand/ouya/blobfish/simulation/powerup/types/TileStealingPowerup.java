package com.nerdinand.ouya.blobfish.simulation.powerup.types;

import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.TimedPowerUp;

public class TileStealingPowerup extends TimedPowerUp {
	public static final float DURATION = 5;
	
	private Array<? extends Player> players;
	
	public TileStealingPowerup(VisibleTile tile, Array<? extends Player> array) {
		super(tile, DURATION);
		
		this.players = array;
	}
	
	@Override
	public boolean trigger() {
		boolean trigger = super.trigger();

		if (trigger){
			setPlayerOwner(getOwner());
		}
		
		return trigger;
	}

	private void setPlayerOwner(Player owner) {
		for (Player player : players) {
			player.setOwner(owner);
		}
	}
	
	@Override
	public void done() {
		setPlayerOwner(null);
		
		super.done();
	}
	
	@Override
	public PowerUpType getPowerUpType() {
		return PowerUpType.TILE_STEALING_POWER_UP;
	}
}
