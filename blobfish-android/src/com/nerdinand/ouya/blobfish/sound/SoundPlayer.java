package com.nerdinand.ouya.blobfish.sound;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector3;

public class SoundPlayer {
	public static final float STANDARD_VOLUME = 5f;
	public static final float AUDIBLE_DISTANCE = 100f;

	private static final String TAG = "SoundPlayer";

	public static void playSound(Sound sound, Vector3 listenerPosition, Vector3 sourcePosition) {
		if (sound == null || listenerPosition == null || sourcePosition == null){
			return;
		}
		
		float distance = listenerPosition.dst(sourcePosition);

		if (distance < AUDIBLE_DISTANCE) {
			float volume = calcVolume(distance);
			float pan = calcPan(listenerPosition, sourcePosition);

			sound.play(volume, 1, pan);
		}
	}

	public static float calcPan(Vector3 listenerPosition, Vector3 sourcePosition) {
		float pan = (sourcePosition.z - listenerPosition.z) / AUDIBLE_DISTANCE;
		return pan;
	}

	public static float calcVolume(float distance) {
		float volume = (1f / distance) * AUDIBLE_DISTANCE - 1;
		return volume;
	}
}
