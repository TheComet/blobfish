package com.nerdinand.ouya.blobfish.util;

import java.util.List;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Sphere;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.TileField;
import com.nerdinand.ouya.blobfish.map.tile.Tile;
import com.nerdinand.ouya.blobfish.map.tile.Tile.TileType;
import com.nerdinand.ouya.blobfish.simulation.player.Player;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUp.PowerUpType;

public class Physics {

	private static final float CENTER_DISTANCE = 5f;

	private static final float WALL_DISTANCE = 9.0f;

	private static final int COLLISION_SPHERE_RADIUS = 4;

	private static final Vector3 FLOOR_REPULSIVE_FORCE = new Vector3(0.0f, 9.81f, 0.0f); //(0, 0.06f, 0);

	private static final String TAG = "Physics";

	// access to the world
	private TileField tileField;

	// world gravity
	private static float worldGravity = 9.81f;//0.06f;

	// air resistance
	private static float airResistance = 0.02f;

	public Physics(TileField tileField) {
		this.tileField = tileField;
	}
	
	
	// compute new forces for an object
	public Vector3 computeForce(Vector3 inputForce, Vector3 inputVelocity, float deltaTime) {
		/*double speedFactorX = (Math.pow(Math.abs(inputVelocity.x), 4));
		double speedFactorZ = (Math.pow(Math.abs(inputVelocity.z), 4));
		if (inputVelocity.x > 0) {
			inputForce.x -= (speedFactorX * airResistance * deltaTime);
		} else if (inputVelocity.x < 0) {
			inputForce.x += (speedFactorX * airResistance * deltaTime);
		}
		if (inputVelocity.z > 0) {
			inputForce.z -= (speedFactorZ * airResistance * deltaTime);
		} else if (inputVelocity.z < 0) {
			inputForce.z += (speedFactorZ * airResistance * deltaTime);
		}*/
		inputForce.y -= (worldGravity * deltaTime);
		return inputForce;
	}

	// All Collisions
	public Vector3 computeCollision(Player player, Array<? extends Player> array, float deltaTime) {
		Vector3 currentPosition = player.getPosition();
		Vector3 force = player.getForce();
		List<Tile> possibleTiles = tileField.getPossibleTilesforIntersection(player);
		
		Sphere collisionSphere = getCollisionSphere(currentPosition, player); // the sphere symbolising the player model
		
		if (possibleTiles != null) {
			for (Tile tile : possibleTiles) {		
				if (intersectsWith(tile.getBoundingBox(), collisionSphere)) {
					force.add(computeReaction(player, tile, deltaTime));
				}
			}
		}
		
		// For BumpPowerUp -> If the Player has a triggered BumpPowerUp, he shall not react to any collisions with other Players.
		boolean hasTriggeredBumopPowerUp = (player.hasTriggeredPowerup() && (player.getTriggeredPowerUp().getPowerUpType() == PowerUpType.BUMP_POWER_UP));
		
		if (array != null && !hasTriggeredBumopPowerUp) {
			for (Player otherPlayer : array) {
				if (otherPlayer != player) {
					Sphere otherPlayerSphere = getCollisionSphere(otherPlayer.getPosition(), otherPlayer);
					if (collisionSphere.overlaps(otherPlayerSphere)) {
						computeSphereReaction(player, otherPlayer);
					}
				}
			}
		}
		//if (hasTriggeredBumopPowerUp && player.getPowerUp().getState().getState() == State.TRIGGERED) {
		//	player.getPowerUp().done();
		//}
		return force;
	}
	
	public void computeSphereReaction(Player player, Player otherPlayer) {
		if (player.getIsAffectedByBlackHole() == false) {
		Vector3 dist = player.getPosition().cpy().sub(otherPlayer.getPosition());
		player.setPosition(player.getPosition().x + dist.cpy().nor().x * 8f, player.getPosition().y, player.getPosition().z + dist.cpy().nor().z * 8);
		player.setDirection(dist.cpy().nor().x, player.getVelocity().y, dist.cpy().nor().z);
		}
	}
	
	//CollisionReaction
	public Vector3 computeReaction(Player player, Tile tile, float deltaTime) {
		Tile currentTile = tileField.getTileAt(player.getPosition());
		Vector3 force = new Vector3(0.0f, 0.0f, 0.0f);
		Vector3 velo = player.getVelocity();
		Vector3 curPos = player.getPosition();
		Vector3 relPos = curPos.cpy().sub(tile.getPosition().cpy());
		Vector3 boxCent = tile.getBoundingBox().getCenter();
		Vector3 boxDim = tile.getBoundingBox().getDimensions();
		
		boolean hasTriggeredSizeBoostPowerUp = (player.hasTriggeredPowerup() && (player.getTriggeredPowerUp().getPowerUpType() == PowerUpType.SIZE_BOOST_POWER_UP));

		//GroundCollision
		if (Math.abs(relPos.x) <= CENTER_DISTANCE && Math.abs(relPos.z) <= CENTER_DISTANCE) {
			player.getPosition().y = (boxDim.cpy().y / 2) + 3f;
			if(player.getVelocity().y < 0) {
				//player.getVelocity().y = 0f;
				player.getVelocity().y = player.getVelocity().y * (-0.5f);
			}
			force.add(FLOOR_REPULSIVE_FORCE.mul(deltaTime));
			
		//SlidingCollision
		} else if ((relPos.x > CENTER_DISTANCE || relPos.x < -CENTER_DISTANCE) && !(currentTile.getTileType() == TileType.WALL_TILE) && !(currentTile.getTileType() == TileType.HIGH_WALL_TILE) && !(hasTriggeredSizeBoostPowerUp)) {
			if (relPos.x > 5f){
				player.getPosition().x = boxCent.x + WALL_DISTANCE;
			} else if (relPos.x < -5f) {
				player.getPosition().x = boxCent.x - WALL_DISTANCE;
			}
			if(velo.z == 0f){
				//player.setDirection((velo.cpy().nor().x * (-1)), velo.y, velo.cpy().nor().z);
				player.setDirection(0.0f, velo.y, 0.0f);
			} else {
				player.setVelocity(0.0f, velo.y, velo.cpy().z * 0.7f);
				//player.setDirection(0.0f, velo.y, velo.cpy().z);
			}
		} else if ((relPos.z > CENTER_DISTANCE || relPos.z < -CENTER_DISTANCE) && !(currentTile.getTileType() == TileType.WALL_TILE) && !(currentTile.getTileType() == TileType.HIGH_WALL_TILE) && !(hasTriggeredSizeBoostPowerUp)) {
			if (relPos.z > 5f){
				player.getPosition().z = boxCent.z + WALL_DISTANCE;
			} else if (relPos.z < -5f) {
				player.getPosition().z = boxCent.z - WALL_DISTANCE;
			}
			if(velo.x == 0f){
				//player.setDirection(velo.cpy().nor().x, velo.y, (velo.cpy().nor().z * (-1)));
				player.setDirection(0.0f, velo.y, 0.0f);
			} else {
				player.setVelocity(velo.cpy().x * 0.7f, velo.y, 0.0f);
				//player.setDirection(velo.cpy().x, velo.y, 0.0f);
			}
		}
		return force;
	}

	
	private Sphere getCollisionSphere(Vector3 position, Player player) {
		return new Sphere(position, COLLISION_SPHERE_RADIUS * player.getCollisionSphereSize());
	}

	public static boolean intersectsWith(BoundingBox boundingBox1, BoundingBox boundingBox2) {
		Vector3 otherMin = boundingBox1.getMin();
		Vector3 otherMax = boundingBox1.getMax();
		Vector3 min = boundingBox2.getMin();
		Vector3 max = boundingBox2.getMax();

		return (min.x < otherMax.x) && (max.x > otherMin.x) && (min.y < otherMax.y) && (max.y > otherMin.y) && (min.z < otherMax.z) && (max.z > otherMin.z);
	}

	public static boolean intersectsWith(BoundingBox boundingBox, Sphere sphere) {
		if (boundingBox == null || sphere == null) { // if an argument is null, there is no collision
			return false;
		}

		float dmin = 0;
		Vector3 center = sphere.center;
		Vector3 bmin = boundingBox.getMin();
		Vector3 bmax = boundingBox.getMax();
		
		if (center.x < bmin.x) {
			dmin += Math.pow(center.x - bmin.x, 2);
		} else if (center.x > bmax.x) {
			dmin += Math.pow(center.x - bmax.x, 2);
		}

		if (center.y < bmin.y) {
			dmin += Math.pow(center.y - bmin.y, 2);
		} else if (center.y > bmax.y) {
			dmin += Math.pow(center.y - bmax.y, 2);
		}

		if (center.z < bmin.z) {
			dmin += Math.pow(center.z - bmin.z, 2);
		} else if (center.z > bmax.z) {
			dmin += Math.pow(center.z - bmax.z, 2);
		}

		return dmin <= Math.pow(sphere.radius, 2);
	}

}
