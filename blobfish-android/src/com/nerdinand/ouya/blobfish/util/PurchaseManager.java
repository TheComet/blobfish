package com.nerdinand.ouya.blobfish.util;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.json.JSONException;
import org.json.JSONObject;

import tv.ouya.console.api.CancelIgnoringOuyaResponseListener;
import tv.ouya.console.api.OuyaEncryptionHelper;
import tv.ouya.console.api.OuyaErrorCodes;
import tv.ouya.console.api.OuyaFacade;
import tv.ouya.console.api.OuyaResponseListener;
import tv.ouya.console.api.Product;
import tv.ouya.console.api.Purchasable;
import tv.ouya.console.api.Receipt;
import android.app.Activity;
import android.os.Bundle;
import android.util.Base64;

import com.badlogic.gdx.Gdx;

public class PurchaseManager {
	private static final String TAG = "PurchaseManager";

	private static OuyaFacade ouyaFacade;

	private static final String DEVELOPER_ID = "24e495a8-4667-4915-b5c9-b3709f74765a";

	// TODO include an array of purchasable entitlements (products), as defined on the OUYA dev site
	private static final List<Purchasable> PRODUCT_ID_LIST = new ArrayList<Purchasable>();

	// TODO include application key from Games panel on OUYA dev site
	private static final byte[] APPLICATION_KEY = null;
	private static PublicKey mPublicKey;
	
	private static HashMap<String, Product> mOutstandingPurchaseRequests = new HashMap<String, Product>();

	static OuyaResponseListener<ArrayList<Product>> productListListener = new CancelIgnoringOuyaResponseListener<ArrayList<Product>>() {
		@Override
		public void onSuccess(ArrayList<Product> products) {
			for (Product p : products) {
				Gdx.app.debug("Product", p.getName() + " costs " + p.getPriceInCents());
			}
		}

		@Override
		public void onFailure(int errorCode, String errorMessage, Bundle optionalData) {
			Gdx.app.debug("Error", errorMessage);
		}
	};

	CancelIgnoringOuyaResponseListener<String> receiptListListener = new CancelIgnoringOuyaResponseListener<String>() {
		@Override
		public void onSuccess(String receiptResponse) {
			OuyaEncryptionHelper helper = new OuyaEncryptionHelper();
			List<Receipt> receipts = null;
			try {
				JSONObject response = new JSONObject(receiptResponse);
				if (response.has("key") && response.has("iv")) {
					receipts = helper.decryptReceiptResponse(response, mPublicKey);
				} else {
					receipts = helper.parseJSONReceiptResponse(receiptResponse);
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			for (Receipt r : receipts) {
				Gdx.app.debug("Receipt", "You have purchased: " + r.getIdentifier());
			}
		}

		@Override
		public void onFailure(int errorCode, String errorMessage, Bundle optionalData) {
			Gdx.app.debug("Error", errorMessage);
		}
	};

	CancelIgnoringOuyaResponseListener<String> purchaseListener = new CancelIgnoringOuyaResponseListener<String>() {
		private Product mProduct;

		@Override
		public void onSuccess(String result) {
			Product product;
			try {
				OuyaEncryptionHelper helper = new OuyaEncryptionHelper();

				JSONObject response = new JSONObject(result);
				if (response.has("key") && response.has("iv")) {
					String id = helper.decryptPurchaseResponse(response, mPublicKey);
					Product storedProduct;
					synchronized (mOutstandingPurchaseRequests) {
						storedProduct = mOutstandingPurchaseRequests.remove(id);
					}
					if (storedProduct == null) {
						onFailure(OuyaErrorCodes.THROW_DURING_ON_SUCCESS, "No purchase outstanding for the given purchase request", Bundle.EMPTY);
						return;
					}
					product = storedProduct;
				} else {
					product = new Product(new JSONObject(result));
					if (!mProduct.getIdentifier().equals(product.getIdentifier())) {
						onFailure(OuyaErrorCodes.THROW_DURING_ON_SUCCESS, "Purchased product is not the same as purchase request product", Bundle.EMPTY);
						return;
					}
				}

				Gdx.app.debug("Purchase", "Congrats you bought: " + product.getName());
			} catch (Exception e) {
				Gdx.app.error("Purchase", "Your purchase failed.", e);
			}
		}

		@Override
		public void onFailure(int errorCode, String errorMessage, Bundle info) {
			Gdx.app.debug("Error", errorMessage);
		}
	};

	public static void init(OuyaFacade ouyaFacade, Activity activity) {
		PurchaseManager.ouyaFacade = ouyaFacade;

		ouyaFacade.init(activity, DEVELOPER_ID);

		// TODO comment this out when releasing
		ouyaFacade.setTestMode();

		ouyaFacade.requestProductList(PRODUCT_ID_LIST, productListListener);

		createPublicKey();
	}

	private static void createPublicKey() {
		// Create a PublicKey object from the key data downloaded from the developer portal.
		try {
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(APPLICATION_KEY);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			mPublicKey = keyFactory.generatePublic(keySpec);
		} catch (Exception e) {
			Gdx.app.error(TAG, "Unable to create encryption key", e);
		}
	}

	public static boolean isPurchased(Purchasable purchasable) {
		return false;
	}

	public void requestPurchase(final Product product) throws GeneralSecurityException, UnsupportedEncodingException, JSONException {
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");

		// This is an ID that allows you to associate a successful purchase with
		// it's original request. The server does nothing with this string except
		// pass it back to you, so it only needs to be unique within this instance
		// of your app to allow you to pair responses with requests.
		String uniqueId = Long.toHexString(sr.nextLong());

		JSONObject purchaseRequest = new JSONObject();
		purchaseRequest.put("uuid", uniqueId);
		purchaseRequest.put("identifier", product.getIdentifier());
		// This value is only needed for testing, not setting it results in a live purchase
		purchaseRequest.put("testing", "true");
		String purchaseRequestJson = purchaseRequest.toString();

		byte[] keyBytes = new byte[16];
		sr.nextBytes(keyBytes);
		SecretKey key = new SecretKeySpec(keyBytes, "AES");

		byte[] ivBytes = new byte[16];
		sr.nextBytes(ivBytes);
		IvParameterSpec iv = new IvParameterSpec(ivBytes);

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC");
		cipher.init(Cipher.ENCRYPT_MODE, key, iv);
		byte[] payload = cipher.doFinal(purchaseRequestJson.getBytes("UTF-8"));

		cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
		cipher.init(Cipher.ENCRYPT_MODE, mPublicKey);
		byte[] encryptedKey = cipher.doFinal(keyBytes);

		Purchasable purchasable = new Purchasable(product.getIdentifier(), Base64.encodeToString(encryptedKey, Base64.NO_WRAP), Base64.encodeToString(ivBytes, Base64.NO_WRAP), Base64.encodeToString(payload, Base64.NO_WRAP));

		synchronized (mOutstandingPurchaseRequests) {
			mOutstandingPurchaseRequests.put(uniqueId, product);
		}
		ouyaFacade.requestPurchase(purchasable, purchaseListener);
	}

}
