package com.nerdinand.ouya.blobfish.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

public class SplitScreenHelper {

	private Array<Rectangle> screenDimensions;

	public SplitScreenHelper(int numPlayers) {
		screenDimensions = calculateDimensions(numPlayers);
	}

	private Array<Rectangle> calculateDimensions(int num) {
		Array<Rectangle> dimensions = new Array<Rectangle>(num);

		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		// OpenGL counts the pixels from the bottom left corner
		
		switch (num) {
		case 1: {
			dimensions.add(new Rectangle(0, 0, width, height));
			break;
		}
		case 2: {
			final int dimensionsWidth = width / 2;
			dimensions.add(new Rectangle(0, 0, dimensionsWidth, height));
			dimensions.add(new Rectangle(dimensionsWidth, 0, dimensionsWidth, height));
			break;
		}
		case 3: {
			final int dimensionsWidth = width / 2;
			final int dimensionsHeight = height / 2;
			dimensions.add(new Rectangle(0, dimensionsHeight, dimensionsWidth, dimensionsHeight));
			dimensions.add(new Rectangle(dimensionsWidth, dimensionsHeight, dimensionsWidth, dimensionsHeight));
			dimensions.add(new Rectangle(0, 0, dimensionsWidth, dimensionsHeight));
			break;
		}
		
		case 4: {
			final int dimensionsWidth = width / 2;
			final int dimensionsHeight = height / 2;
			dimensions.add(new Rectangle(0, dimensionsHeight, dimensionsWidth, dimensionsHeight));
			dimensions.add(new Rectangle(dimensionsWidth, dimensionsHeight, dimensionsWidth, dimensionsHeight));
			dimensions.add(new Rectangle(0, 0, dimensionsWidth, dimensionsHeight));
			dimensions.add(new Rectangle(dimensionsWidth, 0, dimensionsWidth, dimensionsHeight));
			break;
		}
		default:
			break;
		}

		return dimensions;
	}

	public Rectangle getDimension(int i) {
		return screenDimensions.get(i);
	}
}
