package com.nerdinand.ouya.blobfish;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2.Settings;

public class BlobfishDesktop {
	public static void main(String[] args) {
//		Settings settings = new Settings();
//		settings.maxWidth = 1024;
//		settings.maxHeight = 1024;
//		TexturePacker2.process(settings, "../images/game", "../blobfish-android/assets/textures", "game");
//		TexturePacker2.process(settings, "../images/ui", "../blobfish-android/assets/textures", "ui");

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Blobfish";
		config.vSyncEnabled = true;
		config.useGL20 = true;
		config.width = 1280;
		config.height = 720;
		config.fullscreen = false;

		new LwjglApplication(new Blobfish(), config);
	}
}
