package com.nerdinand.ouya.blobfish;

import java.io.IOException;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.utils.Array;
import com.nerdinand.ouya.blobfish.map.MapFormatException;
import com.nerdinand.ouya.blobfish.map.formats.JSONMap;
import com.nerdinand.ouya.blobfish.map.formats.OldMap;

public class MapConverter implements ApplicationListener {

	private Array<String> paths;

	public MapConverter(String path) {
		this(new String[]{path});
	}

	public MapConverter(String[] maps) {
		this.paths = new Array<String>(maps);
	}

	public static void main(String[] args) {
		String[] maps = new String[]{
				"maps/ponypack/apples.omg", 
				"maps/ponypack/dresses.omg", 
				"maps/ponypack/magic.omg", 
				"maps/ponypack/party.omg", 
				"maps/ponypack/stayoutofmyshed.omg", 
				"maps/ponypack/swag.omg"
		};
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Map converter";
		config.fullscreen = false;
		
		new LwjglApplication(new MapConverter(maps), config);
	}

	@Override
	public void create() {
		for (String path : paths) {			
			OldMap oldMap = new OldMap(path);
			
			try {
				oldMap.loadMap();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MapFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			JSONMap jsonMap = new JSONMap(oldMap);
			jsonMap.saveMap(path+".r34");		
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}
