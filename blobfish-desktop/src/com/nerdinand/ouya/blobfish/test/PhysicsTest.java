package com.nerdinand.ouya.blobfish.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Sphere;
import com.nerdinand.ouya.blobfish.test.mock.MockPlayer;
import com.nerdinand.ouya.blobfish.test.mock.MockTile;
import com.nerdinand.ouya.blobfish.util.Physics;

public class PhysicsTest {

	@Test
	public void testBoundingBoxCollision() {
		BoundingBox box1 = new BoundingBox(new Vector3(0, 0, 0), new Vector3(1, 1, 1));
		BoundingBox box2 = new BoundingBox(new Vector3(5, 5, 5), new Vector3(6, 6, 6));
		BoundingBox box3 = new BoundingBox(new Vector3(0, 0, 0), new Vector3(1, 1, 1));

		assertTrue(Physics.intersectsWith(box1, box3));
		assertFalse(Physics.intersectsWith(box1, box2));
	}

	@Test
	public void testSphereBoxCollision() {
		BoundingBox box1 = new BoundingBox(new Vector3(0, 0, 0), new Vector3(1, 1, 1));
		Sphere sphere1 = new Sphere(new Vector3(0, 0, 0), 1);

		Sphere sphere2 = new Sphere(new Vector3(3, 3, 3), 1);

		assertTrue(Physics.intersectsWith(box1, sphere1));
		assertFalse(Physics.intersectsWith(box1, sphere2));
	}
	
	@Test
	public void testComputeSphereReaction() {
		Physics physics = new Physics(null);
		MockPlayer player1 = new MockPlayer();
		MockPlayer player2 = new MockPlayer();
		player1.setPosition(new Vector3(0,0,0));
		player2.setPosition(new Vector3(0.5f,0f,0f));

		physics.computeSphereReaction(player1, player2);
		
		//assertEquals(new Vector3(-8f, 0.0f,0.0f), player1.getPosition());
		assertEquals(new Vector3(-1f, 0, 0), player1.getDirection());
	}
}
