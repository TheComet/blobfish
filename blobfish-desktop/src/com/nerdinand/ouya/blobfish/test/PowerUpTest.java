package com.nerdinand.ouya.blobfish.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUp;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUpState.State;
import com.nerdinand.ouya.blobfish.test.mock.MockPlayer;
import com.nerdinand.ouya.blobfish.test.mock.MockPowerUp;
import com.nerdinand.ouya.blobfish.test.mock.MockVisibleTile;

public class PowerUpTest {

	private MockVisibleTile mockTile;
	private MockPowerUp mockPowerUp;

	@Before
	public void before() {
		mockTile = new MockVisibleTile(new Vector2(1f, 1f));
		mockPowerUp = new MockPowerUp(mockTile);
	}

	@Test
	public void testConstructor() {
		assertEquals(mockTile, mockPowerUp.getSpawnTile());
		assertTrue(mockTile.hasPowerUp());
		assertEquals(mockPowerUp, mockTile.getPowerUp());
		assertEquals(PowerUp.TIME_TO_LIVE, mockPowerUp.getTimeToLive(), 0);
		assertFalse(mockPowerUp.getState().getState() == State.DEAD);

		assertFalse(mockPowerUp.getState().getState() == State.PICKED_UP);
		assertNull(mockPowerUp.getOwner());
	}

	@Test
	public void testIsDead() {
		mockPowerUp.update(PowerUp.TIME_TO_LIVE);
		assertEquals(0, mockPowerUp.getTimeToLive(), 0);
		assertTrue(mockPowerUp.getState().getState() == State.DEAD);

		mockPowerUp.update(1);
		assertTrue(mockPowerUp.getState().getState() == State.DEAD);
	}

	@Test
	public void testToString() {
		assertEquals("MockPowerUp", mockPowerUp.toString());
	}

	@Test
	public void testOwner(){
		MockPlayer player = new MockPlayer();
		mockPowerUp.setOwner(player);

		assertTrue(mockPowerUp.getState().getState() == State.PICKED_UP);
		assertEquals(player, mockPowerUp.getOwner());
	}
}
