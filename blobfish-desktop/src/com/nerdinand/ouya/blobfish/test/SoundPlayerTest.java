package com.nerdinand.ouya.blobfish.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.math.Vector3;
import com.nerdinand.ouya.blobfish.sound.SoundPlayer;

public class SoundPlayerTest {

	@Test
	public void testVolumeCalculation() {
		assertEquals("volume calculation", Double.POSITIVE_INFINITY, (double) SoundPlayer.calcVolume(0), 0);
		assertEquals("volume calculation", 9, (double) SoundPlayer.calcVolume(10), 0);
		assertEquals("volume calculation", 49, (double) SoundPlayer.calcVolume(2), 0);
	}

	@Test
	public void testPanCalculation() {
		Vector3 listenerPosition = Vector3.Zero;
		Vector3 sourcePosition = listenerPosition;
		
		assertEquals("pan calculation", 0.0, (double) SoundPlayer.calcPan(listenerPosition, sourcePosition), 0);
		
		sourcePosition = new Vector3(0, 0, 10);
		assertEquals("pan calculation", 0.1, (double) SoundPlayer.calcPan(listenerPosition, sourcePosition), 0.0000001);

		sourcePosition = new Vector3(0, 0, -10);
		assertEquals("pan calculation", -0.1, (double) SoundPlayer.calcPan(listenerPosition, sourcePosition), 0.0000001);
	}

}
