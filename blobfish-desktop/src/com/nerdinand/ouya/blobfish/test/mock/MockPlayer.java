package com.nerdinand.ouya.blobfish.test.mock;

import com.nerdinand.ouya.blobfish.simulation.player.Player;

public class MockPlayer extends Player {

	public MockPlayer() {
		super("MockPlayer");
	}

}
