package com.nerdinand.ouya.blobfish.test.mock;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;
import com.nerdinand.ouya.blobfish.simulation.powerup.PowerUp;

public class MockPowerUp extends PowerUp {

	private static final PowerUpType MOCK_POWER_UP = null;

	public MockPowerUp(VisibleTile tile) {
		super(tile);
	}

	@Override
	public boolean trigger() {
		return false;
	}

	@Override
	public PowerUpType getPowerUpType() {
		return MOCK_POWER_UP;
	}

}
