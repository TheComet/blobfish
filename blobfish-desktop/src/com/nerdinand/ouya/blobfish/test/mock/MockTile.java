package com.nerdinand.ouya.blobfish.test.mock;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;

public class MockTile extends VisibleTile {
	
	public MockTile(Vector2 tileCoordinates) {
		super(tileCoordinates);
	}

	@Override
	
	public boolean isSpawnPoint() {
		return false;
	}

	@Override
	public boolean canSpawnPowerUp() {
		return false;
	}

	@Override
	public boolean doesRender() {
		return false;
	}

	@Override
	public Pixmap getMiniMapIcon() {
		return null;
	}
	
	@Override
	public boolean isWalkable() {
		return false;
	}

	@Override
	public void update(float deltaTime) {
	}

	@Override
	public Vector3 getDimension() {
		return super.getDimension();
	}
	
	@Override
	public TileType getTileType() {
		return TileType.COLOUR_TILE;
	}
}
