package com.nerdinand.ouya.blobfish.test.mock;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.nerdinand.ouya.blobfish.map.tile.VisibleTile;

public class MockVisibleTile extends VisibleTile {

	public MockVisibleTile(Vector2 tileCoordinates) {
		super(tileCoordinates);
	}

	@Override
	public boolean isSpawnPoint() {
		return false;
	}

	@Override
	public boolean canSpawnPowerUp() {
		return true;
	}

	@Override
	public boolean isWalkable() {
		return true;
	}
	
	@Override
	public Pixmap getMiniMapIcon() {
		return null;
	}
	
	@Override
	public TileType getTileType() {
		return TileType.COLOUR_TILE;
	}
}
