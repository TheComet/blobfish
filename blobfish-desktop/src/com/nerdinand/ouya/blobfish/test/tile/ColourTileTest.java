package com.nerdinand.ouya.blobfish.test.tile;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.nerdinand.ouya.blobfish.map.tile.ColourTile;

public class ColourTileTest {

	private ColourTile colourTile1;
	private ColourTile colourTile2;
	private ColourTile colourTile3;

	@Before
	public void before() {
		colourTile1 = new ColourTile(new Vector2(0, 0));
		colourTile2 = new ColourTile(new Vector2(0, 0), true);
		colourTile3 = new ColourTile(new Vector2(0, 0), true, false);
	}

	@Test
	public void testConstructor() {
		assertTrue("doesRender", colourTile1.doesRender());
		assertFalse("isSpawnPoint", colourTile1.isSpawnPoint());
		assertNull("owner", colourTile1.getOwner());
		assertTrue("canSpawnPowerUp", colourTile1.canSpawnPowerUp());

		assertTrue("isSpawnPoint", colourTile2.isSpawnPoint());
		
		assertFalse("canSpawnPowerUp", colourTile3.canSpawnPowerUp());
	}
}
