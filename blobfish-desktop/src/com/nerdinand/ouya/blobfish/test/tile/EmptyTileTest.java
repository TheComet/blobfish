package com.nerdinand.ouya.blobfish.test.tile;

import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.nerdinand.ouya.blobfish.map.tile.EmptyTile;

public class EmptyTileTest {

	private EmptyTile emptyTile;

	@Before
	public void before() {
		emptyTile = new EmptyTile(new Vector2(0, 0));
	}

	@Test
	public void testConstructor() {
		assertFalse("doesRender", emptyTile.doesRender());
		assertFalse("isSpawnPoint", emptyTile.isSpawnPoint());
		assertFalse("canSpawnPowerUp", emptyTile.canSpawnPowerUp());
	}

}
