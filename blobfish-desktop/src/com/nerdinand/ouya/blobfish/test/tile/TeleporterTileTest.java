package com.nerdinand.ouya.blobfish.test.tile;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.nerdinand.ouya.blobfish.map.tile.TeleporterTile;

public class TeleporterTileTest {

	private TeleporterTile teleporterTile;

	@Before
	public void before() {
		teleporterTile = new TeleporterTile(new Vector2(0, 0));
	}

	@Test
	public void testConstructor() {
		assertTrue("doesRender", teleporterTile.doesRender());
		assertFalse("isSpawnPoint", teleporterTile.isSpawnPoint());
		assertFalse("canSpawnPowerUp", teleporterTile.canSpawnPowerUp());
	}
}
