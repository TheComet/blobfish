package com.nerdinand.ouya.blobfish.test.tile;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.nerdinand.ouya.blobfish.map.TileField;

public class TileFieldTest {
	@Test
	public void testCoordinateConversion() {
		Vector2 mapCoord = new Vector2(10, 20);
		Vector3 worldCoord = TileField.mapCoordinatesToWorld(mapCoord);
		
		worldCoord = new Vector3(-195, 10, 120);
		mapCoord = TileField.worldCoordinatesToMap(worldCoord);
		assertEquals("worldCoordinatesToMap", new Vector2(12.5f, 20f), mapCoord);
		
		mapCoord = new Vector2(5, 4);
		worldCoord = TileField.mapCoordinatesToWorld(mapCoord);
		assertEquals(mapCoord, TileField.worldCoordinatesToMap(worldCoord));
	}
}
