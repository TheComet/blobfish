package com.nerdinand.ouya.blobfish.test.tile;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.nerdinand.ouya.blobfish.test.mock.MockVisibleTile;

public class TileTest {

	private MockVisibleTile mockTile;

	@Before
	public void before() {
		mockTile = new MockVisibleTile(new Vector2(0, 0));
	}

	@Test
	public void testConstructor() {
		assertEquals("tile coordinates", new Vector2(0, 0), mockTile.getTileCoordinates());
		assertEquals("3D tile position", new Vector3(5, 0, -5), mockTile.getPosition());

		BoundingBox boundingBox = mockTile.getBoundingBox();
		assertEquals("Bounding box minimum vector", new Vector3(0, 0, -10), boundingBox.min);
		assertEquals("Bounding box maximum vector", new Vector3(10, 0, 0), boundingBox.max);
		assertEquals(mockTile.getCenter(), boundingBox.getCenter());
	}

	@Test
	public void testSetTileCoordinates() {
		mockTile.setTileCoordinates(new Vector2(4, 3));
		assertEquals("tile coordinates", new Vector2(4, 3), mockTile.getTileCoordinates());
		assertEquals("3D tile position", new Vector3(-25, 0, 35), mockTile.getPosition());
		
		assertEquals("tile center", new Vector3(-25, 0, 35), mockTile.getCenter());
	}
}
