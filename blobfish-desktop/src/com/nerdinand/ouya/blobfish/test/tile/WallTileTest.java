package com.nerdinand.ouya.blobfish.test.tile;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.nerdinand.ouya.blobfish.map.tile.WallTile;

public class WallTileTest {

	private WallTile wallTile;

	@Before
	public void before() {
		wallTile = new WallTile(new Vector2(0, 0));
	}

	@Test
	public void testConstructor() {
		assertTrue("doesRender", wallTile.doesRender());
		assertFalse("isSpawnPoint", wallTile.isSpawnPoint());
		assertFalse("canSpawnPowerUp", wallTile.canSpawnPowerUp());
	}
}
